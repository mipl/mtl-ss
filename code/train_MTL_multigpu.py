import argparse
import datetime
import importlib
import os
import numpy as np
import re
import tensorflow as tf
import yaml
from dataset.helper import *
from models.AdapNet_pp_MTL_multigpu import AdapNet_pp_MTL
from models.SegNet_MTL_multigpu import SegNet_MTL
from models.UNet_MTL_multigpu import UNet_MTL
from models.FCN8_MTL_multigpu import FCN8_MTL
from models.ENet_MTL_multigpu import ENet_MTL
from models.DeconvNet_MTL_multigpu import DeconvNet_MTL
from models.FCDenseNet_MTL_multigpu import FCDenseNet_MTL
from models.FastNet_MTL_multigpu import FastNet_MTL
from models.ParseNet_MTL_multigpu import ParseNet_MTL
from summary.summary import CTensorBoard
from dataset.CamVid_read import CAMVID
from dataset.Cityscape_read_final import CITYSCAPES
from dataset.FreiburgForest_read import FREIBURG_FOREST

PARSER = argparse.ArgumentParser()
PARSER.add_argument('-c', '--config', default='config/cityscapes_train.config')

tf.reset_default_graph()

def _tower_loss(model, images, label_S, label_E, label_C, label_D, batch_size, scope, reuse_variables=None):
  """Calculate the total loss on a single tower running the Network model.
  We perform 'batch splitting'. This means that we cut up a batch across
  multiple GPUs. For instance, if the batch size = 32 and num_gpus = 2,
  then each tower will operate on an batch of 16 images.
  Args:
    images: Images. 4D tensor of size [batch_size, FLAGS.image_size, FLAGS.image_size, 3].
    labels: 1-D integer Tensor of [batch_size].
    num_classes: number of classes
    scope: unique prefix string identifying the ImageNet tower, e.g.
      'tower_0'.
  Returns:
     Tensor of shape [] containing the total loss for a batch of data
  """

  # Build inference Graph.
  with tf.variable_scope(tf.get_variable_scope(), reuse=reuse_variables):
    S, aux1_S, aux2_S, E, C, aux1_C, aux2_C, D, aux1_D, aux2_D = model.forward(images, batch_size)

  # Build the portion of the Graph calculating the losses. Note that we will
  # assemble the total_loss using a custom function below.
  loss, loss_S, loss_S_iou, loss_E, loss_E_iou, loss_C, loss_C_iou, loss_D, loss_D_iou = \
      model.loss(logits_ss=S, label_ss=label_S, aux1_ss=aux1_S, aux2_ss=aux2_S, softmax_bd=E, \
            softmax_cc=C, aux1_cc=aux1_C, aux2_cc=aux2_C, \
            softmax_el=D, aux1_el=aux1_D, aux2_el=aux2_D, \
            label_bd=label_E, label_cc=label_C, label_el=label_D)

  # Assemble all of the losses for the current tower only.
  losses = tf.get_collection('losses', scope)

  # Calculate the total loss for the current tower.
  total_loss = tf.add_n(losses, name='total_loss')

  # Compute the moving average of all individual losses and the total loss.
  loss_averages = tf.train.ExponentialMovingAverage(0.9, name='avg')
  loss_averages_op = loss_averages.apply(losses + [total_loss])

  # Attach a scalar summmary to all individual losses and the total loss; do the
  # same for the averaged version of the losses.
  with tf.control_dependencies([loss_averages_op]):
    total_loss = tf.identity(total_loss)
  return total_loss, S, E, C, D, loss_S, loss_S_iou, loss_E, \
          loss_E_iou, loss_C, loss_C_iou, loss_D, loss_D_iou

def _average_gradients(tower_grads):
  """Calculate the average gradient for each shared variable across all towers.
  Note that this function provides a synchronization point across all towers.
  Args:
    tower_grads: List of lists of (gradient, variable) tuples. The outer list
      is over individual gradients. The inner list is over the gradient
      calculation for each tower.
  Returns:
     List of pairs of (gradient, variable) where the gradient has been averaged
     across all towers.
  """
  average_grads = []
  for grad_and_vars in zip(*tower_grads):
    # Note that each grad_and_vars looks like the following:
    #   ((grad0_gpu0, var0_gpu0), ... , (grad0_gpuN, var0_gpuN))
    grads = []
    for g, v in grad_and_vars:
      #print("------------> ", v.name, v.get_shape().as_list())
      # Add 0 dimension to the gradients to represent the tower.
      expanded_g = tf.expand_dims(g, 0)

      # Append on a 'tower' dimension which we will average over below.
      grads.append(expanded_g)

    # Average over the 'tower' dimension.
    grad = tf.concat(axis=0, values=grads)
    grad = tf.reduce_mean(grad, 0)

    # Keep in mind that the Variables are redundant because they are shared
    # across towers. So .. we will just return the first tower's pointer to
    # the Variable.
    v = grad_and_vars[0][1]
    grad_and_var = (grad, v)
    average_grads.append(grad_and_var)
  return average_grads

def get_variables_to_restore(variables):
  variables_to_restore = []
  for v in variables:
    #print("--> v.name: ", v.name.split(':')[0])
    # one can do include or exclude operations here.
    if 'ExponentialMovingAverage' not in v.name.split(':')[0] and \
       '/avg' not in v.name.split(':')[0]  and '_power' not in v.name.split(':')[0] \
        and '/Adam' not in v.name.split(':')[0]:
      #print("Variables restored: %s" % v.name)
      variables_to_restore.append(v)
  return variables_to_restore

def get_variables_to_train(self, variables, frozen=False):
  """Returns a list of variables to train.
  Returns:
    A list of variables to train by the optimizer.
  """
  variables_to_train = []

  no_train_var=[]
  for var_train in variables:
    var_name_complete = str(var_train.name).split(':')[0]
    name_var = str(var_train.name).split(':')[0].split('/')
    name = name_var[0]
    if frozen:
      if name in no_train_var:
        continue
    variables_to_train.append(var_train)
  return variables_to_train

def train_func(config):
  """Train on dataset for a number of steps."""

  tf.reset_default_graph()
  with tf.Graph().as_default(), tf.device('/cpu:0'):
    #data_bt, label_bt, bound_bt, contourclass_bt, energy_bt, iterator = get_train_data(config)

    os.environ['CUDA_VISIBLE_DEVICES'] = config['gpu_id']
    #module = importlib.import_module('models.'+config['model'])
    #model_func = getattr(module, config['model'])
    # data_bt, label_bt, bound_bt, contourclass_bt, energy_bt, iterator = get_train_data(config)
    name_model = config['model'] #'resnet_v2_50'

    # Create a variable to count the number of train() calls. This equals the
    # number of batches processed * FLAGS.num_gpus.
    #global_step = tf.get_variable('Global_Step',[],initializer=tf.constant_initializer(0),trainable=False)
    global_step = tf.Variable(0, trainable=False, name='Global_Step')

    # placeholders
    batch_size_pl = tf.placeholder(tf.int64, shape=[], name="batch_size")
    images_pl = tf.placeholder(tf.float32, [None, config['height'], config['width'], 3])
    labels_S_pl1 = tf.placeholder(tf.float32, [None, config['height'], config['width'], 1])
    labels_E_pl = tf.placeholder(tf.float32, [None, config['height'], config['width'], 1])
    labels_C_pl1 = tf.placeholder(tf.float32, [None, config['height'], config['width'], 1])
    labels_D_pl1 = tf.placeholder(tf.float32, [None, config['height'], config['width'], 1])
    labels_S_pl = tf.one_hot(tf.cast(tf.squeeze(labels_S_pl1,axis=-1),tf.int64), config['num_classes'])
    labels_C_pl = tf.one_hot(tf.cast(tf.squeeze(labels_C_pl1,axis=-1),tf.int64), config['num_classes'])
    if config['regression']:
      labels_D_pl = labels_D_pl1
    else:
      labels_D_pl = tf.one_hot(tf.cast(tf.squeeze(labels_D_pl1,axis=-1),tf.int64), config['energy_level'])

    if name_model == 'SegNet':
      model_build = SegNet_MTL
    elif name_model == 'UNet':
      model_build = UNet_MTL
    elif name_model == 'DeconvNet':
      model_build = DeconvNet_MTL
    elif name_model == 'ENet':
      model_build = ENet_MTL
    elif name_model == 'FCN8':
      model_build = FCN8_MTL
    elif name_model == 'FCDenseNet67':
      model_build = FCDenseNet_MTL
    elif name_model == 'AdapNet_pp':
      model_build = AdapNet_pp_MTL
    elif name_model == 'FastNet':
      model_build = FastNet_MTL
    elif name_model == 'ParseNet':
      model_build = ParseNet_MTL
    else:
      sys.exit('name model undefined, [SegNet, AdapNet_pp, UNet, DeconvNet, FCN8, FCDenseNet67, FastNet, ParseNet]')

    TT_S = 'S' in config['tasks']
    TT_E = 'E' in config['tasks']
    TT_C = 'C' in config['tasks']
    TT_D = 'D' in config['tasks']

    with tf.variable_scope(name_model):
      model = model_build(num_classes=config['num_classes'], energy_level=config['energy_level'], \
               learning_rate=config['learning_rate'], float_type=tf.float32, weight_decay=0.0005, \
               decay_steps=30000, power=config['power'], training=True, ignore_label=True, \
               global_step=global_step, has_aux_loss=True, S=TT_S, E=TT_E, C=TT_C, D=TT_D, regression=config['regression'])
    
    # Create an optimizer that performs gradient descent.
    # decayed_learning_rate = learning_rate * decay_rate ^ (global_step / decay_steps)
    # decay from 0.1 to 0.01 in 10000 steps using sqrt (i.e. power=0.5)
    lr = tf.train.polynomial_decay(config['learning_rate'], \
          global_step, config['decay_steps'], config['end_learning_rate'], power=config['power'])

    opt = tf.train.AdamOptimizer(learning_rate=lr, epsilon=0.0001)

    # devices (gpus using)
    devices = []
    for i in range(len(config['gpu_id'].split(','))):
      print("/gpu:"+str(i))
      devices.append("/gpu:"+str(i))

    # Split the batch of images and labels for towers.
    images_splits = tf.split(axis=0, num_or_size_splits=len(devices), value=images_pl)
    labels_S_splits = tf.split(axis=0, num_or_size_splits=len(devices), value=labels_S_pl)
    labels_E_splits = tf.split(axis=0, num_or_size_splits=len(devices), value=labels_E_pl)
    labels_C_splits = tf.split(axis=0, num_or_size_splits=len(devices), value=labels_C_pl)
    labels_D_splits = tf.split(axis=0, num_or_size_splits=len(devices), value=labels_D_pl)

    # Calculate the gradients for each model tower.
    tower_grads = []
    tower_losses = []
    tower_loss_S_cross = []
    tower_loss_S_iou = []
    if model.T_E: 
      tower_loss_E_cross = []
      tower_loss_E_iou = []
    if model.T_C: 
      tower_loss_C_cross = []
      tower_loss_C_iou = []
    if model.T_D: 
      tower_loss_D_cross = []
      tower_loss_D_iou = []
    
    reuse_variables = None
    logits_S = None
    if model.T_E: logits_E = None
    if model.T_C: logits_C = None
    if model.T_D: logits_D = None

    batch_size = tf.cast(tf.divide(batch_size_pl,len(devices)), tf.int64)
    
    with tf.variable_scope(name_model):
      for gpu_index, gpu_device in enumerate(devices):
        with tf.device(gpu_device):
          with tf.name_scope('%s_%d' % ('tower', gpu_index)) as scope:

            # Calculate the loss for one tower of the Network model. This
            # function constructs the entire Network model but shares the
            # variables across all towers.
            loss, S, E, C, D, loss_S, loss_S_iou, loss_E, loss_E_iou, loss_C, loss_C_iou, loss_D, loss_D_iou = \
                _tower_loss(model, images_splits[gpu_index], labels_S_splits[gpu_index], \
                labels_E_splits[gpu_index], labels_C_splits[gpu_index], labels_D_splits[gpu_index], batch_size, scope, reuse_variables)

            #logist tensors output
            if logits_S is None:
              logits_S = S
              if model.T_E: logits_E = E
              if model.T_C: logits_C = C
              if model.T_D: logits_D = D
            else:
              logits_S = tf.concat([logits_S, S], axis=0)
              if model.T_E: logits_E = tf.concat([logits_E, E], axis=0)
              if model.T_C: logits_C = tf.concat([logits_C, C], axis=0)
              if model.T_D: logits_D = tf.concat([logits_D, D], axis=0)

            # Reuse variables for the next tower.
            reuse_variables = True

            # Retain the Batch Normalization updates operations only from the
            # final tower. Ideally, we should grab the updates from all towers
            # but these stats accumulate extremely fast so we can ignore the
            # other stats from the other towers without significant detriment.
            batchnorm_updates = tf.get_collection('_update_ops_', scope)

            # Calculate the gradients for the batch of data on this Network tower
            grads = opt.compute_gradients(loss)

            # Keep track of the gradients across all towers.
            tower_grads.append(grads)
            tower_losses.append(loss)

            tower_loss_S_cross.append(loss_S)
            tower_loss_S_iou.append(loss_S_iou)
            if model.T_E: 
              tower_loss_E_cross.append(loss_E)
              tower_loss_E_iou.append(loss_E_iou)
            if model.T_C: 
              tower_loss_C_cross.append(loss_C)
              tower_loss_C_iou.append(loss_C_iou)
            if model.T_D: 
              tower_loss_D_cross.append(loss_D)
              tower_loss_D_iou.append(loss_D_iou)

    # We must calculate the mean of each gradient. Note that this is the
    # synchronization point across all towers.
    grads_ave = _average_gradients(tower_grads)
    #grads_ave_clip = [(tf.clip_by_value(grad_i, -1., 1.),var_i) for grad_i, var_i in grads_ave]

    #average of loss
    tower_losses = tf.reduce_mean(tower_losses)

    # Apply the gradients to adjust the shared variables.
    apply_gradient_op = opt.apply_gradients(grads_ave, global_step=global_step)

    # Track the moving averages of all trainable variables.
    # Note that we maintain a "double-average" of the BatchNormalization
    # global statistics. This is more complicated then need be but we employ
    # this for backward-compatibility with our previous models.
    variable_averages = tf.train.ExponentialMovingAverage(0.9999, global_step)

    variables_to_average = (tf.trainable_variables() + tf.moving_average_variables())
    variables_averages_op = variable_averages.apply(variables_to_average)

    # Group all updates to into a single train op.
    batchnorm_updates_op = tf.group(*batchnorm_updates)
    train_op = tf.group(apply_gradient_op, variables_averages_op, batchnorm_updates_op)

    # Build an initialization operation to run below.
    init_global = tf.global_variables_initializer()
    init_local = tf.local_variables_initializer()

    config1 = tf.ConfigProto()
    config1.gpu_options.allow_growth = True
    sess = tf.Session(config=config1)
    sess.run(init_global)
    step = 0

    #summary values
    tasks = 'S'
    if model.T_E: tasks += 'E'
    if model.T_C: tasks += 'C'
    if model.T_D: tasks += 'D'
    if model.regression: tasks += 'R'
    if config['is_board']:
      TB = CTensorBoard()
      summary_writer = tf.summary.FileWriter(config['tb_logs']+name_model+'_'+tasks, graph=sess.graph)
      step_summary = 0

    #metrics for each task
    output_matrix_S = np.zeros([config['num_classes'], 3])
    if model.T_E: output_matrix_E = np.zeros([2, 3])
    if model.T_C: output_matrix_C = np.zeros([config['num_classes'], 3])
    if model.T_D: output_matrix_D = np.zeros([config['energy_level'], 3])
    
    if config['load_param']:
      ckpt = tf.train.get_checkpoint_state(os.path.dirname(os.path.join(config['path_param'],'checkpoint')))
      if ckpt: 
        import_variables = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES)
        variable_to_restore = get_variables_to_restore(import_variables)
        print('total_variables_loaded:', len(import_variables))
        saver = tf.train.Saver(variable_to_restore)
        saver.restore(sess, ckpt.model_checkpoint_path)
        step = sess.run(tf.assign(global_step, step))
        #step = 11000
        step = int( str(ckpt.model_checkpoint_path).split('model.ckpt-')[-1] )+1
        print('------ Model Loaded from ' + ckpt.model_checkpoint_path + ' -------')
      else:
        if 'intialize' in config:
          reader = tf.train.NewCheckpointReader(config['intialize'])
          var_str = reader.debug_string().decode('utf-8')
          name_var = re.findall('[A-Za-z0-9/:_]+ ', var_str)
          import_variables = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES)
          initialize_variables = {} 
          for var in import_variables: 
            if var.name+' ' in  name_var:
              initialize_variables[var.name] = var

          saver = tf.train.Saver(initialize_variables)
          saver.restore(save_path=config['intialize'], sess=sess)
          print('-------- Pretrained Intialization ---------')
        saver = tf.train.Saver()

    if config['dataset'] == 'camvid':
      num_classes = config['num_classes']  # 11+1
      energy_level = config['energy_level'] # 5+1
      #input_paste="/datasets/CamVid/", 360
      #input_paste="/datasets/CamVid_aug/", None 5616
      dataset = CAMVID(input_paste="/datasets/CamVid_aug/", num_classes=num_classes, \
              energy_level=energy_level-1, use_threads=12*4, regression=config['regression'], num_data_load=\
              5616 - (5616 % (config['batch_size']*len(devices)) ) )
              #150 - (150 % (config['batch_size']*len(devices)) ) )
      dataset.load_data(size=(360, 480)) 
    elif config['dataset'] == 'cityscape_11':
      num_classes = config['num_classes']  # 11+1
      energy_level = config['energy_level'] # 5+1 
      # /work/Cityscapes_aug3/train   17850 # 17850 - (17850 % config['batch_size']*len(devices))
      # /work/Cityscapes_aug3/val     500
      # /work/Cityscapes_aug3/test    1525
      dataset = CITYSCAPES(input_paste="/drw/Cityscapes_aug3", \
                    num_classes=num_classes, energy_level=energy_level-1, use_threads=12*4, num_data_load=\
                    #17850 - (17850 % (config['batch_size']*len(devices)) ) )#17850)#14000)
                    150 - (150 % (config['batch_size']*len(devices)) ) )#17850)#14000)
      dataset.load_data(size=(384,768))
      #seg_freq, bc_freq, energy_freq = dataset.get_calculated_frequency()
      #mean_channel = dataset.get_mean_channel()
    elif  config['dataset'] == 'freiburgforest':
      # /datasets/FreiburgForest/Aug/train   1840
      num_classes = config['num_classes'] # 5+1
      energy_level = config['energy_level'] # 5+1 
      dataset = FREIBURG_FOREST(input_paste="/datasets/FreiburgForest/Aug", num_classes=num_classes, \
          use_threads=12*4, energy_level=5, regression=config['regression'], num_data_load=\
          1840 - (1840 % (config['batch_size']*len(devices)) ) )
      dataset.load_data(size=(384,768))
    elif config['dataset'] == 'synthia':
      num_classes = config['num_classes']  # 11+1
      energy_level = config['energy_level'] # 5+1
      #input_paste="/datasets/synthia/", 9400 (7k train) (2.4k test)
      #input_paste="/datasets/synthia/Aug/", None 56000
      dataset = SYNTHIA(input_paste="/datasets/SYNTHIA/Aug", num_classes=num_classes, \
              energy_level=energy_level-1, use_threads=12*4, regression=config['regression'], num_data_load=\
              25000 - (25000 % (config['batch_size']*len(devices)) ) )
              #150 - (150 % (config['batch_size']*len(devices)) ) )
      dataset.load_data(size=(384,768)) 
    else:
      sys.exit('dataset undefined, [camvid, cityscape_11, freiburgforest, synthia]')

    list_l = [];
    list_l_S_cross = []
    list_l_S_iou = []
    if model.T_E: 
      list_l_E_cross = []
      list_l_E_iou = []
    if model.T_C: 
      list_l_C_cross = []
      list_l_C_iou = []
    if model.T_D: 
      list_l_D_cross = []
      list_l_D_iou = []

    epochs = config['epochs']
    bt_sz = config['batch_size']*len(devices)
    for ep in range(epochs):
      print("\n########## epoch " + str(ep+1) + " --- " + config['dataset'] + " --- " + name_model + " --- " + tasks + " ##########")
      total_batch = int(dataset.get_size_dataset('train')/bt_sz)
      for i in range(total_batch):
        batch_img, batch_gt_S, batch_gt_E, batch_gt_C, batch_gt_D, \
            batch_nameimg = dataset.next_batch(state='train', batch_size=bt_sz)
        if len(batch_img) == 0:
          continue
        batch_gt_S_ = np.expand_dims(batch_gt_S, axis=-1)
        batch_gt_E_ = np.expand_dims(batch_gt_E, axis=-1)
        batch_gt_C_ = np.expand_dims(batch_gt_C, axis=-1)
        batch_gt_D_ = np.expand_dims(batch_gt_D, axis=-1)
        feed_dict = {images_pl:batch_img, labels_S_pl1:batch_gt_S_, labels_E_pl:batch_gt_E_, \
            labels_C_pl1:batch_gt_C_, labels_D_pl1:batch_gt_D_, batch_size_pl:bt_sz}

        fetches = [ train_op, tower_losses, tower_loss_S_cross, tower_loss_S_iou, logits_S ] 
        if model.T_E: fetches += [ tower_loss_E_cross, tower_loss_E_iou, logits_E]
        if model.T_C: fetches += [ tower_loss_C_cross, tower_loss_C_iou, logits_C]
        if model.T_D: fetches += [ tower_loss_D_cross, tower_loss_D_iou, logits_D]
        step += 1

        res = sess.run(fetches, feed_dict)
        _, loss_batch, l_S_cross, l_S_iou, prob_S = res[0], res[1], res[2], res[3], res[4]
        cnt_res = 5
        if model.T_E: 
          l_E_cross, l_E_iou, prob_E = res[cnt_res], res[cnt_res+1], res[cnt_res+2]
          cnt_res += 3
        if model.T_C: 
          l_C_cross, l_C_iou, prob_C = res[cnt_res], res[cnt_res+1], res[cnt_res+2]
          cnt_res += 3
        if model.T_D: 
          l_D_cross, l_D_iou, prob_D = res[cnt_res], res[cnt_res+1], res[cnt_res+2]
          cnt_res += 3

        list_l.append(loss_batch)
        list_l_S_cross.append(l_S_cross)
        list_l_S_iou.append(l_S_iou)
        prediction_S = np.argmax(prob_S, 3)
        gt_S = batch_gt_S
        prediction_S[gt_S == config['unlabel']] = config['unlabel']
        output_matrix_S = compute_output_matrix(gt_S, prediction_S, output_matrix_S)

        if model.T_E: 
          list_l_E_cross.append(l_E_cross)
          list_l_E_iou.append(l_E_iou)
          prediction_E = (np.squeeze(prob_E,axis=-1) > 0.5)
          gt_E = batch_gt_E
          output_matrix_E = compute_output_matrix(gt_E, prediction_E, output_matrix_E)
        if model.T_C: 
          list_l_C_cross.append(l_C_cross)
          list_l_C_iou.append(l_C_iou)
          prediction_C = np.argmax(prob_C, 3)
          gt_C = batch_gt_C
          prediction_C[gt_C == config['unlabel']] = config['unlabel']
          output_matrix_C = compute_output_matrix(gt_C, prediction_C, output_matrix_C)
        if model.T_D: 
          list_l_D_cross.append(l_D_cross)
          list_l_D_iou.append(l_D_iou)
          prediction_D = np.argmax(prob_D, 3)
          gt_D = batch_gt_D
          prediction_D[gt_D == 0] = config['unlabel']
          output_matrix_D = compute_output_matrix(gt_D, prediction_D, output_matrix_D)
        
        if (step + 1) % config['save_step'] == 0:
          import_variables = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES)
          saver = tf.train.Saver(import_variables)
          saver.save(sess, os.path.join(config['checkpoint']+'/'+name_model+'/'+tasks, 'model.ckpt'), step+1)

        if (step + 1) % config['skip_step'] == 0:
          total_loss = np.mean(list_l)
          total_loss_S_cross = np.mean(list_l_S_cross)
          total_loss_S_iou = np.mean(list_l_S_iou)
          total_S_iou = compute_iou(output_matrix_S, -1)
          if model.T_E:
            total_loss_E_cross = np.mean(list_l_E_cross)
            total_loss_E_iou = np.mean(list_l_E_iou)
            total_E_iou = compute_iou(output_matrix_E, 0)
          if model.T_C:
            total_loss_C_cross = np.mean(list_l_C_cross)
            total_loss_C_iou = np.mean(list_l_C_iou)
            total_C_iou = compute_iou(output_matrix_C, config['unlabel'])
          if model.T_D:
            total_loss_D_cross = np.mean(list_l_D_cross)
            total_loss_D_iou = np.mean(list_l_D_iou)
            total_D_iou = compute_iou(output_matrix_D, 0)

          print("Step {} lr={:6.6} loss={:6.4f} iou={:6.4f} l_S={:6.4f} l_S_iou={:6.4f}".format(step, lr.eval(session=sess), total_loss, \
                      total_S_iou, total_loss_S_cross, total_loss_S_iou), end = '')
          if model.T_E:
            print(" l_E={:6.4} l_E_iou={:6.4f}".format(total_loss_E_cross, total_loss_E_iou), end = '')
          if model.T_C:
            print(" l_C={:6.4} l_C_iou={:6.4f}".format(total_loss_C_cross, total_loss_C_iou), end = '')
          if model.T_D:
            print(" l_D={:6.4} l_D_iou={:6.4f}".format(total_loss_D_cross, total_loss_D_iou), end = '')
          print()
          # ------------------- summary  ------------------- 
          if config['is_board']:
            req = [ TB.loss_sc, TB.loss_S_cross_sc, TB.loss_S_iou_sc, TB.iou_S ] 
            if model.T_E:
              req += [  TB.loss_E_cross_sc, TB.loss_E_iou_sc, TB.iou_E ]
            if model.T_C:
              req += [  TB.loss_C_cross_sc, TB.loss_C_iou_sc, TB.iou_C]
            if model.T_D:
              req += [  TB.loss_D_cross_sc, TB.loss_D_iou_sc, TB.iou_D ]
            
            feed_summary = {TB.loss_ph:total_loss, TB.loss_S_cross_ph:total_loss_S_cross, \
              TB.loss_S_iou_ph:total_loss_S_iou, TB.iou_S_ph:total_S_iou}
            if model.T_E:
              feed_summary.update({TB.loss_E_cross_ph:total_loss_E_cross, TB.loss_E_iou_ph:total_loss_E_iou, TB.iou_E_ph:total_E_iou})
            if model.T_C:
              feed_summary.update({TB.loss_C_cross_ph:total_loss_C_cross, TB.loss_C_iou_ph:total_loss_C_iou, TB.iou_C_ph:total_C_iou})
            if model.T_D:
              feed_summary.update({TB.loss_D_cross_ph:total_loss_D_cross, TB.loss_D_iou_ph:total_loss_D_iou, TB.iou_D_ph:total_D_iou})

            res_summ = sess.run(req, feed_dict=feed_summary)
            _l, _l_S_cross, _l_S_iou, _iou_S = res_summ[0], res_summ[1], res_summ[2], res_summ[3]
            cnt_summ = 4
            if model.T_E:
              _l_E_cross, _l_E_iou, _iou_E = res_summ[cnt_summ], res_summ[cnt_summ+1], res_summ[cnt_summ+2]         
              cnt_summ += 3
            if model.T_C:
              _l_C_cross, _l_C_iou, _iou_C = res_summ[cnt_summ], res_summ[cnt_summ+1], res_summ[cnt_summ+2]
              cnt_summ += 3
            if model.T_D:
              _l_D_cross, _l_D_iou, _iou_D = res_summ[cnt_summ], res_summ[cnt_summ+1], res_summ[cnt_summ+2]
              cnt_summ += 3

            summary_writer.add_summary(_l, step_summary)
            summary_writer.add_summary(_l_S_cross, step_summary)
            summary_writer.add_summary(_l_S_iou, step_summary)
            summary_writer.add_summary(_iou_S, step_summary)
            if model.T_E:
              summary_writer.add_summary(_l_E_cross, step_summary)
              summary_writer.add_summary(_l_E_iou, step_summary)   
              summary_writer.add_summary(_iou_E, step_summary)
            if model.T_C:
              summary_writer.add_summary(_l_C_cross, step_summary)
              summary_writer.add_summary(_l_C_iou, step_summary)           
              summary_writer.add_summary(_iou_C, step_summary)
            if model.T_D:
              summary_writer.add_summary(_l_D_cross, step_summary)
              summary_writer.add_summary(_l_D_iou, step_summary)
              summary_writer.add_summary(_iou_D, step_summary)

            step_summary += 1
          
      list_l.clear()
      output_matrix_ss.fill(0)
      if model.T_E: output_matrix_bd.fill(0)
      if model.T_C: output_matrix_cc.fill(0)
      if model.T_D: output_matrix_el.fill(0)
    
    saver.save(sess, os.path.join(config['checkpoint']+'/'+name_model+'/'+tasks, 'model.ckpt'), step-1)
    print('training_completed')
    sess.close()

def load_dataset(config):
    #os.environ['CUDA_VISIBLE_DEVICES'] = config['gpu_id']
    data_bt, label_bt, bound_bt, contourclass_bt, energy_bt, iterator = get_train_data(config)
    with tf.Session() as sess:
      image_, label_, bound_, contourclass_, energy_ = sess.run([data_bt, label_bt, bound_bt, contourclass_bt, energy_bt])
      print(image_.shape, label_.shape, bound_.shape, contourclass_.shape, energy_.shape)

def main():
    args = PARSER.parse_args()
    if args.config:
        file_address = open(args.config)
        config = yaml.load(file_address)
    else:
        print('--config config_file_address missing')
    #load_dataset(config)
    train_func(config)

if __name__ == '__main__':
    main()  
