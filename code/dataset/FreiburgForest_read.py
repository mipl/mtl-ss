from skimage.segmentation import find_boundaries
from skimage.morphology import erosion, dilation, disk
from skimage import measure
from scipy import ndimage
from collections import namedtuple
from matplotlib import animation
import matplotlib.pyplot as plt
plt.rcParams.update({'figure.max_open_warning': 0})
plt.switch_backend('agg')
from pathlib import Path
from PIL import Image, ImageEnhance
import pandas as pd
import numpy as np
#import random
import time
import os
from tqdm import tqdm
import multiprocessing

#random.seed(time.time())
np.random.seed(int(time.time()))
#np.random.seed(7)

import tensorflow as tf

def normalize(x):
  #normalized_x = x / np.linalg.norm(x) #slow operation
  #normalized_x = x / np.sqrt( np.sum( x**2 ) )
  if np.max(x) != np.min(x):
    normalized_x = ( x - np.min(x) ) / ( np.max(x) - np.min(x) ) #[0,1]
  else:
    normalized_x = x
  return normalized_x

def standardize(x):
  standardize_x = (x - np.mean(x)) / np.std(x)
  return standardize_x

def _int64_feature(data):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[data]))

def _bytes_feature(data):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[data]))


class FREIBURG_FOREST:

  def __init__( self, input_paste="/datasets/FreiburgForest/freiburg_forest_annotated", num_classes=6, use_threads=10, energy_level=6,  regression=False, num_data_load=None):
    self.input_paste = input_paste
    self.num_classes = num_classes
    self.num_data_load = num_data_load
    self.struct_elem = disk(2)
    self.K = energy_level
    self._index_in_epoch = 0
    self.use_threads = use_threads
    self.regression = regression

    #https://github.com/mcordts/cityscapesScripts/blob/master/cityscapesscripts/helpers/labels.py
    Label = namedtuple( 'Label' , [
      'name'      , # The identifier of this label, e.g. 'road', 'grass', ... .
      'trainId'     , 
      'color'       , # The rgb color of this label
      ] )

    self.labels = [
      #       name                     trainId    color
      Label(  'road',                0,            (170,	170, 	170) ),
      Label(  'grass',               1,            (0, 	255, 	0) ),
      Label(  'tree',                 2,            (0, 	60, 	0) ),
      Label(  'vegetation',     2,             (102,	102, 	51) ),
      Label(  'sky',                  3,             (0,	120, 	255) ),
      Label(  'obstacle',         4,             (255,	 255, 	0) ),
      Label(  'void',                 5 ,            (0,  0,  0) ), #unlabel
    ]

    #self.label_map_id = {label.color: label.trainId for label in self.labels}
    #self.color_to_trainId_map_func = np.vectorize(self.label_map_id.get)
    #self.label_map_class = [int(label.trainId) for label in self.labels] 
    #self.label_map_inst = [int(label.hasInstances) for label in self.labels]

  def set_im_size(self, h, w):
    self.img_w = w
    self.img_h = h

  def restart_initbatch(self):
      self._index_in_epoch = 0

  def random_crop(self, im, seg, depth, crop_dims, resize_dim=(768, 384), resample=Image.NEAREST):
    """
    Args:
        im:         PIL image
        crop_dims:  Dimensions of the crop region [width, height]
        offset:     Position of the crop box from Top Left corner [x, y]
        preserve_size: (bool) Should it resize back to original dims?
        resample:       resampling method during rescale.
    Returns:
        PIL image of size crop_size, randomly cropped from `im`.
    """
    crop_width, crop_height = crop_dims
    crop_height, crop_width = crop_dims
    width, height = im.size
    x_offset = np.random.randint(0, width - crop_width + 1)
    y_offset = np.random.randint(0, height - crop_height + 1)
    im2 = im.crop((x_offset, y_offset, x_offset + crop_width, y_offset + crop_height))
    im2seg = seg.crop((x_offset, y_offset, x_offset + crop_width, y_offset + crop_height))
    im2depth = depth.crop((x_offset, y_offset, x_offset + crop_width, y_offset + crop_height))
    im2 = self.random_brightness(im2)
    im2 = self.random_contrast(im2)
    im2 = im2.resize(resize_dim, resample=resample)
    im2seg = im2seg.resize(resize_dim, resample=resample)
    im2depth = im2depth.resize(resize_dim, resample=resample)
    return im2, im2seg, im2depth


  def random_brightness(self, im, sd=0.5, min=0.5, max=15):
    """Creates a new image which randomly adjusts the brightness of `im` by
       randomly sampling a brightness value centered at 1, with a standard
       deviation of `sd` from a normal distribution. Clips values to a
       desired min and max range.
    Args:
        im:   PIL image
        sd:   (float) Standard deviation used for sampling brightness value.
        min:  (int or float) Clip contrast value to be no lower than this.
        max:  (int or float) Clip contrast value to be no higher than this.
    Returns:
        PIL image with brightness randomly adjusted.
    """
    #brightness = np.clip(np.random.normal(loc=1, scale=sd), min, max)
    factor = np.random.uniform(0.8,1.2)
    enhancer = ImageEnhance.Brightness(im)
    #return enhancer.enhance(brightness)
    return enhancer.enhance(factor)


  def random_contrast(self, im, sd=0.5, min=0.5, max=10):
    """Creates a new image which randomly adjusts the contrast of `im` by
       randomly sampling a contrast value centered at 1, with a standard
       deviation of `sd` from a normal distribution. Clips values to a
       desired min and max range.
    Args:
        im:   PIL image
        sd:   (float) Standard deviation used for sampling contrast value.
        min:  (int or float) Clip contrast value to be no lower than this.
        max:  (int or float) Clip contrast value to be no higher than this.
    Returns:
        PIL image with contrast randomly adjusted.
    """
    #contrast = np.clip(np.random.normal(loc=1, scale=sd), min, max)
    factor = np.random.uniform(0.8,1.2)
    enhancer = ImageEnhance.Contrast(im)
    #return enhancer.enhance(contrast)
    return enhancer.enhance(factor)

  def data_augmentation(self, paste_out="/datasets/FreiburgForest/Aug", size=(384, 768), num_data_load=None):

    self.img_h = size[0] #img_h, axis=x
    self.img_w = size[1] #img_w, axis=y

    # print files into input paste
    print(os.listdir(self.input_paste))

    self.input_dir = Path(self.input_paste)
    self.images_dir_train = str(self.input_dir) + "/train/rgb/"
    self.labels_dir_train = str(self.input_dir) + "/train/GT_color/"
    self.depth_dir_train = str(self.input_dir) + "/train/depth_gray/"

    print("Read images from: ")
    print(self.input_dir)
    print(self.images_dir_train)
    print(self.labels_dir_train)
    print(self.depth_dir_train)

    self.image_write = paste_out + "/train/rgb/"
    self.depth_write = paste_out + "/train/depth_gray/"
    self.semantic_rgb_write = paste_out + "/train/GT_color/"

    print("Write images to: ")
    print(paste_out)
    print(self.image_write)
    print(self.depth_write)
    print(self.semantic_rgb_write)

    list_images = sorted(os.listdir(self.images_dir_train))
    list_labels = sorted(os.listdir(self.labels_dir_train))
    list_depth = sorted(os.listdir(self.depth_dir_train))

    if num_data_load != None:
      list_images = list_images[0:num_data_load]
      list_labels = list_labels[0:num_data_load]
      list_depth = list_depth[0:num_data_load]

    for idx in range(len(list_images)):
      image = self.read_image(path_img=self.images_dir_train+list_images[idx],color="RGB")
      depth_gt = self.read_image(path_img=self.depth_dir_train+list_depth[idx],color="BIL")#/255.0
      gt_seg = self.read_image(path_img=self.labels_dir_train+list_labels[idx],color="RGB", reshape=False)

      im_name = list_images[idx].replace('.jpg','.png')

      semantic_gt = np.ones((gt_seg.shape[0], gt_seg.shape[1]))*5
      for item in self.labels:
        #if item.trainId == 5: continue
        semantic_gt[ np.all(gt_seg == item.color, axis=2) ] = item.trainId

      semantic_gt = semantic_gt.astype(np.uint8)
      #print("semantic_gt original: ", np.unique(semantic_gt))
      semantic_gt = Image.fromarray(semantic_gt).resize(size=(self.img_w,self.img_h), resample=Image.NEAREST)
      #semantic_gt = Image.fromarray(semantic_gt)
      #print("semantic_gt resize: ", np.unique(np.array(semantic_gt)))
      #print("np.unique(semantic_gt) == 100: ", np.unique(np.array(semantic_gt)) == 100)
      #print("-------------------------")
      assert( np.sum(np.unique(np.array(semantic_gt)) >= 1) )

      #image = image.astype(np.uint8) #[0-255]
      #semantic_gt = semantic_gt.astype(np.uint8) #[0-29]
      
      Image.fromarray(image).save(self.image_write+im_name)
      Image.fromarray(depth_gt).save(self.depth_write+im_name)
      #semantic_gt.save(self.semantic_rgb_write+im_name)
      self.writeImage(np.array(semantic_gt).astype(np.uint8), self.semantic_rgb_write+im_name)
      
      #data_augmentation horizontal flip
      image_flip = Image.fromarray(image).transpose(Image.FLIP_LEFT_RIGHT)
      image_flip = image_flip.resize(size=(self.img_w,self.img_h), resample=Image.NEAREST)
      depth_gt_flip = Image.fromarray(depth_gt).transpose(Image.FLIP_LEFT_RIGHT)
      depth_gt_flip = depth_gt_flip.resize(size=(self.img_w,self.img_h), resample=Image.NEAREST)
      semantic_gt_flip = semantic_gt.transpose(Image.FLIP_LEFT_RIGHT)
      #semantic_gt_flip = semantic_gt_flip.resize(size=(self.img_w,self.img_h), resample=Image.NEAREST)

      im_name_ = im_name.split(".")[0]+"_flip.png"
      image_flip.save(self.image_write+im_name_)
      depth_gt_flip.save(self.depth_write+im_name_)
      #semantic_gt_flip.save(self.semantic_rgb_write+im_name_)
      self.writeImage(np.array(semantic_gt_flip).astype(np.uint8), self.semantic_rgb_write+im_name_)

      # crop
      for i in range(3):
        crop_image, crop_semantic_gt, crop_depth_gt = self.random_crop(im=Image.fromarray(image), \
          seg=semantic_gt, depth=Image.fromarray(depth_gt), crop_dims=(200, 450), resize_dim=(768, 384))
        #crop_image = self.random_brightness(crop_image)
        #crop_image = self.random_contrast(crop_image)

        im_name_ = im_name.split(".")[0]+"_crop"+str(i+1)+".png"
        print("--->im_name: ", im_name_)
        #save crop images
        crop_image.save(self.image_write+im_name_)
        crop_depth_gt.save(self.depth_write+im_name_)
        #crop_semantic_gt.save(self.semantic_rgb_write+im_name_)
        self.writeImage(np.array(crop_semantic_gt).astype(np.uint8), self.semantic_rgb_write+im_name_)

        crop_image_flip, crop_semantic_gt_flip, crop_depth_gt_flip = self.random_crop(im=image_flip, \
          seg=semantic_gt_flip, depth=depth_gt_flip, crop_dims=(250, 500), resize_dim=(768, 384))
        #crop_image_flip = self.random_brightness(crop_image_flip)
        #crop_image_flip = self.random_contrast(crop_image_flip)

        #crop_depth_gt_flip = (crop_depth_gt_flip*255).as_type(dtype=np.uint8)

        im_name_ = im_name.split(".")[0]+"_crop_flip"+str(i+1)+".png"
        print("--->im_name: ", im_name_)
        #save crop images
        crop_image_flip.save(self.image_write+im_name_)
        crop_depth_gt_flip.save(self.depth_write+im_name_)
        #crop_semantic_gt_flip.save(self.semantic_rgb_write+im_name_)
        self.writeImage(np.array(crop_semantic_gt_flip).astype(np.uint8), self.semantic_rgb_write+im_name_)

      #https://github.com/vyomshm/semantic_segmentation/tree/master/autonomous_driving
      #https://github.com/ronrest/kitti_semantic_segmentation/blob/master/image_processing.py

  def read_image(self, path_img, color="RGB", reshape=True):
    img = Image.open(path_img)
    width, height = img.size

    if reshape:
      if width != self.img_w or height != self.img_h:
        img = img.resize(size=(self.img_w,self.img_h), resample=Image.NEAREST)

    if color == 'RGB':
      #img = img.resize(size=(self.img_w,self.img_h), resample=Image.NEAREST)
      #img_rgb = scipy.misc.imread(path_img, flatten=False, mode='RGB')
      img_rgb = img.convert('RGB')
      return np.array(img_rgb).astype(np.uint8)
    elif color == "GRAY":
      #img = img.resize(size=(self.img_w,self.img_h), resample=Image.BILINEAR).convert('L')
      img = img.convert('L')
      return np.array(img).astype(np.uint8)
    else: #color == "BIL"
      return np.array(img)

  def writeImage(self, image, filename, is_contour=False):
    """ store label data to colored image, image=(h,w,1) """
    r = image.copy()
    g = image.copy()
    b = image.copy()
    for item in self.labels:
        r[image==item.trainId] = item.color[0]
        g[image==item.trainId] = item.color[1]
        b[image==item.trainId] = item.color[2]
    rgb = np.zeros((image.shape[0], image.shape[1], 3))
    rgb[:,:,0] = r/1.0
    rgb[:,:,1] = g/1.0
    rgb[:,:,2] = b/1.0
    im = Image.fromarray(rgb.astype(np.uint8))
    im.save(filename)

  def writeFloatImage(self, image, filename, dpi_=300, cmap_='plasma'):
    if len(image.shape) == 3:
      h, w, c = image.shape
    else:
      h, w = image.shape
    image = image.reshape((h,w))
    figsize = w/float(dpi_), h/float(dpi_)
    fig = plt.figure(figsize=figsize)
    ax = fig.add_axes([0, 0, 1, 1])
    ax.axis('off')
    #cmap styles https://matplotlib.org/examples/color/colormaps_reference.html
    ax.imshow(image, cmap=cmap_) #'viridis', 'plasma', 'gray', 'afmhot',
    fig.savefig(filename, pad_inches=0, dpi=dpi_)

  def QuantizeDistance(self, value, K, img_dist):
    "Quantize the values in the pixel-wise map into K uniform bins"
    range_bin = value / (K *  1.0)
    current_bin = 0.#-1e-12
    img_quantize = np.zeros_like(img_dist)
    label_bin = 1
    #while( current_bin < value ):
    while( label_bin < K+1 ):
      lower = current_bin
      upper = current_bin + range_bin
      img_quantize += label_bin * ((img_dist > lower)&(img_dist <= upper)).astype(np.uint8)
      current_bin = upper#+= range_bin
      label_bin += 1
    img_quantize += (label_bin - 1) * (img_dist > upper)
    #img_quantize += 1
    return img_quantize

  def compute_class_weights(self, size):
    '''
    references: https://github.com/GeorgeSeif/Semantic-Segmentation-Suite/blob/master/utils/utils.py
    Arguments:
        labels_dir(list): Directory where the image segmentation labels are
        num_classes(int): the number of classes of pixels in all images
    Returns:
        class_weights(list): a list of class weights where each index represents each class label and the element is the class weight for that label.
    '''
    label_values_ss = np.arange(self.num_classes)
    num_classes = len(label_values_ss)
    class_pixels_ss = np.zeros((num_classes))
    class_pixels_bc = np.zeros((num_classes))
    total_pixels_ss = 0.0
    total_pixels_bc = 0.0
    
    label_values_el = np.arange(self.K+1)
    energy_level = len(label_values_el)
    class_pixels_el = np.zeros((self.K+1))
    total_pixels_el = 0.0

    self.img_h = size[0]
    self.img_w = size[1]

    # print files into input paste
    print(os.listdir(self.input_paste))

    # Define some paths first
    self.input_dir = Path(self.input_paste)
    self.images_dir = str(self.input_dir) + "/leftImg8bit/train/"
    self.labels_dir = str(self.input_dir) + "/gtFine/train/gtFine_labelIds/"
    self.instances_dir = str(self.input_dir) + "/gtFine/train/gtFine_instanceIds/"

    list_images = sorted(os.listdir(self.images_dir))
    list_labels = sorted(os.listdir(self.labels_dir))
    list_instances = sorted(os.listdir(self.instances_dir))
    
    #label_files = [ labels_dir + "/" + p for p in list_labels ]
    #image_files = [ list_images + "/" + p for p in train_images ]

    means = np.zeros((3))
    for n in range(len(list_images)):
      image = self.read_image(path_img=self.images_dir+list_images[n],color="RGB")
      for c in range(3):
        means[c] += np.sum(image[:,:,c])
    means /= (image.shape[0] * image.shape[1] * len(list_images))

    for n in range(len(list_labels)):
      #image = self.read_image(path_img=label_files[n],color="GRAY")
      #instance_semantic_gt = self.read_image(path_list_imagesimg=labels_dir+dir_label[n],color="BIL")
      image, semantic_gt_relabel, boundary_gt, contour_class_gt, dist_transf_quantize, \
      im_name = self.preprocessing_data('train', list_images[n], list_labels[n], list_instances[n])

      for index, colour in enumerate(label_values_ss):
        #------- semantic segmentation -------------
        class_map_ss = (semantic_gt_relabel == colour)
        class_map_ss = class_map_ss.astype(np.float32)
        class_pixels_ss[index] += np.sum(class_map_ss)
        # ------------ boundary classifier -------------
        class_map_bc = (contour_class_gt == colour)
        class_map_bc = class_map_bc.astype(np.float32)
        class_pixels_bc[index] += np.sum(class_map_bc)

      for index, level in enumerate(label_values_el):
        # ------------ energy level -----------------
        class_map_el = (dist_transf_quantize == level)
        class_map_el = class_map_el.astype(np.float32)
        class_pixels_el[index] += np.sum(class_map_el)

    # ------------ semantic segmentation -------------
    total_pixels_ss = float(np.sum(class_pixels_ss))
    index_to_delete = np.argwhere(class_pixels_ss==0.0)
    class_pixels_ss = np.delete(class_pixels_ss, index_to_delete)
    class_weights_ss = total_pixels_ss / class_pixels_ss
    #class_weights_ss = class_weights_ss / np.sum(class_weights_ss)
    class_weights_ss = class_weights_ss / np.max(class_weights_ss)
    
    # ------------ boundary classifier -------------
    total_pixels_bc = float(np.sum(class_pixels_bc))
    index_to_delete = np.argwhere(class_pixels_bc==0.0)
    class_pixels_bc = np.delete(class_pixels_bc, index_to_delete)
    class_weights_bc = total_pixels_bc / class_pixels_bc
    #class_weights_bc = class_weights_bc / np.sum(class_weights_bc)
    class_weights_bc = class_weights_bc / np.max(class_weights_bc)

    # ------------ energy level -----------------
    total_pixels_el = float(np.sum(class_pixels_el))
    index_to_delete = np.argwhere(class_pixels_el==0.0)
    class_pixels_el = np.delete(class_pixels_el, index_to_delete)
    class_weights_el = total_pixels_el / class_pixels_el
    #class_weights_el = class_weights_el / np.sum(class_weights_el)
    class_weights_el = class_weights_el / np.max(class_weights_el)

    return means, class_weights_ss, class_weights_bc, class_weights_el

  def get_calculated_frequency(self):
    """get proportion of classes by task"""
    seg_freq = np.array([0.003264, 0.023681, 0.007411, 0.186275, 0.146969, 0.100323, 0.746295, 0.253497, 0.009771, \
            0.109778, 0.054524, 0.073533, 0.571312, 0.015181, 0.399885, 0.411589, 0.444637, 1.000000, 0.237857, 0.021363])

    bound_classifier_freq = np.array([0.019204, 0.024653, 0.012224, 0.182891, 0.137484, 0.024257, 0.258715, 0.116341, \
            0.015090, 0.087139, 0.088944, 0.042900, 0.316273, 0.019212, 0.719615, 0.758998, 1.000000, 0.744537, 0.141998, 0.000227])

    energy_freq = np.array([0.009071, 0.210188, 0.240810, 0.329022, 0.485742, 1.000000])
    
    return seg_freq, bound_classifier_freq, energy_freq

  def get_mean_channel(self):
    """get mean of each channel for subtracted in the dataset"""
    return [74.176388, 82.927987, 72.300755]

  def load_data(self, size=(384,768)):
    """size: (img_h, img_w) """
    #self.img_w = size[0] #img_w, axis=x
    #self.img_h = size[1] #img_h, axis=y
    self.img_h = size[0]
    self.img_w = size[1]

    # print files into input paste
    print(os.listdir(self.input_paste))

    # Define some paths first
    self.input_dir = Path(self.input_paste)
    self.images_dir_train = str(self.input_dir) + "/train/rgb/"
    self.labels_dir_train = str(self.input_dir) + "/train/GT_color/"
    self.depth_dir_train = str(self.input_dir) + "/train/depth_gray/"

    self.images_dir_test = str(self.input_dir) + "/test/rgb/"
    self.labels_dir_test = str(self.input_dir) + "/test/GT_color/"
    self.depth_dir_test = str(self.input_dir) + "/test/depth_gray/"

    print(self.input_dir)
    print(self.images_dir_train)
    print(self.labels_dir_train)
    print(self.depth_dir_train)

    list_images_train = sorted(os.listdir(self.images_dir_train))
    list_labels_train = sorted(os.listdir(self.labels_dir_train))
    list_depth_train = sorted(os.listdir(self.depth_dir_train))

    list_images_test = sorted(os.listdir(self.images_dir_test))
    list_labels_test = sorted(os.listdir(self.labels_dir_test))
    list_depth_test = sorted(os.listdir(self.depth_dir_test))

    #self.test_images = sorted(os.listdir(self.images_dir_test))

    #shuffle list samples
    merge_img_and_lb = list(zip(list_images_train, list_labels_train, list_depth_train))
    np.random.shuffle(merge_img_and_lb)
    list_images_train, list_labels_train, list_depth_train = map(list, zip(*merge_img_and_lb))

    if self.num_data_load != None:
        list_images_train = list_images_train[0:self.num_data_load]
        list_labels_train = list_labels_train[0:self.num_data_load]
        list_depth_train = list_depth_train[0:self.num_data_load]

    #num_val = 0.0
    #num_test = 0.1
    #num_train = 0.9
    #len_data = len(list_images_train)

    self.train_images = list_images_train #[0:int(len_data*num_train)]
    self.train_labels = list_labels_train #[0:int(len_data*num_train)]
    self.train_depth = list_depth_train #[0:int(len_data*num_train)]
    self.test_images = list_images_test #[int(len_data*num_train):int(len_data*num_train)+int(len_data*num_test)]
    self.test_labels = list_labels_test #[int(len_data*num_train):int(len_data*num_train)+int(len_data*num_test)]
    self.test_depth = list_depth_test #[int(len_data*num_train):int(len_data*num_train)+int(len_data*num_test)]
    #self.test_images = list_images[int(len_data*num_train)+int(len_data*num_test):]
    #self.test_labels = list_labels[int(len_data*num_train)+int(len_data*num_test):]
    #self.test_depth = list_depth[int(len_data*num_train)+int(len_data*num_test):]

    print("len train: ", len(self.train_images))
    print("len test: ", len(self.test_images))

    self.num_train_set = len(self.train_images)
    self.num_test_set =len(self.test_images)
    self.num_data_set = self.num_train_set + self.num_test_set

    print("num_data_set: ", self.num_data_set, "\nnum_train_set: ", \
        self.num_train_set, "\nnum_test_set: ", self.num_test_set)

    # ---------------------------------------------------------------------
    #preprocessing for each batch
    #self.train_generator = self.batch_generator('train', self.train_images, self.train_labels, self.train_depth)
    #self.test_generator = self.batch_generator('test', self.test_images, self.test_labels, self.test_depth)
    #'''
    # ---------------------------------------------------------------------
    #preprocessing all the dataset
    #self.data_train = self.load_bull('train', self.num_train_set, self.train_images, self.train_labels, self.train_depth, True)
    #self.data_test = self.load_bull('test', self.num_test_set, self.test_images, self.test_labels, self.test_depth, True)

    #preprocessing all the dataset by threads
    self.data_train = self.load_bull_thread('train', self.num_train_set, self.train_images, self.train_labels, self.train_depth)
    self.data_test = self.load_bull_thread('test', self.num_test_set, self.test_images, self.test_labels, self.test_depth)

    self.train_generator = self.batch_generator_bull(self.data_train[0], self.data_train[1], \
      self.data_train[2], self.data_train[3], self.data_train[4], self.data_train[5])
    #self.valid_generator = self.batch_generator_bull(self.data_val[0], self.data_val[1], \
    #  self.data_val[2], self.data_val[3], self.data_val[4], self.data_val[5])
    self.test_generator = self.batch_generator_bull(self.data_test[0], self.data_test[1], \
      self.data_test[2], self.data_test[3], self.data_test[4], self.data_test[5])
    #'''
    # ---------------------------------------------------------------------

  def get_size_dataset(self, state):
    '''return dataset size of the state'''
    if state == 'train':
      num_data = self.num_train_set
    elif state == 'val':
      num_data = self.num_val_set
    elif state == 'test':
      num_data = self.num_test_set
    else:
      assert(False), 'error of state'
    return num_data

  def get_dataset(self, state):
    '''return data of the state'''
    if state == 'train':
      dataset = self.data_train
    elif state == 'val':
      dataset = self.data_val
    elif state == 'test':
      dataset = self.data_test
    else:
      assert(False), 'error of state'
    return dataset

  def convert2tfrecod(self, record_name, state='train'):
    #https://www.tensorflow.org/tutorials/load_data/tfrecord#reading_a_tfrecord_file
    num_data = self.get_size_dataset(state)
    dataset = self.get_dataset(state)

    data_img = dataset[0]
    data_label_seg = dataset[1]
    data_bound = dataset[2]
    data_contour_class = dataset[3]
    data_energy = dataset[4]
    data_name = dataset[5]

    count = 0.0
    writer = tf.python_io.TFRecordWriter(record_name+'.tfrecords')

    #for name in f:
    for idx in range(num_data):
      modality1 = data_img[idx] #cv2.imread(name[0])
      label = data_label_seg[idx] #cv2.imread(name[1], cv2.IMREAD_ANYCOLOR)
      bound = data_bound[idx]
      contour_class = data_contour_class[idx]
      energy = data_energy[idx]
      try:
        assert len(label.shape)==2
      except AssertionError as e:
        raise( AssertionError( "Label should be one channel!" ) )
          
      height = modality1.shape[0]
      width = modality1.shape[1]
      modality1 = modality1.tostring()
      label = label.tostring()
      bound = bound.tostring()
      contourclass = contour_class.tostring()
      energy = energy.tostring()
      features = {'height':_int64_feature(height),
                  'width':_int64_feature(width),
                  'modality1':_bytes_feature(modality1),
                  'label':_bytes_feature(label),
                  'bound':_bytes_feature(bound),
                  'contourclass':_bytes_feature(contourclass),
                  'energy':_bytes_feature(energy),
                 }
      example = tf.train.Example(features=tf.train.Features(feature=features))
      writer.write(example.SerializeToString())

      if (count+1)%1 == 0:
        print('Processed data: {}'.format(count))

      count = count+1

  def load_bull_thread(self, state, num_data, dir_img, dir_label, dir_depth):

    from multiprocessing.managers import SyncManager
    import signal
    def mgr_init():
        '''initializer for SyncManager'''
        signal.signal(signal.SIGINT, signal.SIG_IGN)

    threads = self.use_threads
    num_data_thread = num_data / threads
    #http://jtushman.github.io/blog/2014/01/14/python-%7C-multiprocessing-and-interrupts/
    manager = SyncManager()
    manager.start(mgr_init) # explicitly starting the manager, and telling it to ignore the interrupt signal
    list_images = [[]] * threads
    list_labels_seg = [[]] * threads
    list_labels_bound = [[]] * threads
    list_labels_contour_class = [[]] * threads
    list_labels_dist = [[]] * threads
    list_img_name = [[]] * threads

    for i in range(0, threads):
      list_images[i] = manager.list()
      list_labels_seg[i] = manager.list()
      list_labels_bound[i] = manager.list()
      list_labels_contour_class[i] = manager.list()
      list_labels_dist[i] = manager.list()
      list_img_name[i] = manager.list()

    jobs = []
    for i in range(0, threads):
      #out_list = list()
      thread = multiprocessing.Process( target=self.load_bull2, args=(state, i, \
        dir_img[int(i*num_data_thread):min(int((i+1)*num_data_thread), num_data)], \
        dir_label[int(i*num_data_thread):min(int((i+1)*num_data_thread), num_data)], \
        dir_depth[int(i*num_data_thread):min(int((i+1)*num_data_thread), num_data)], \
        list_images[i], list_labels_seg[i], list_labels_bound[i], list_labels_contour_class[i], \
        list_labels_dist[i], list_img_name[i]))
      jobs.append(thread)
      #out_list2.append(out_list)
    # Start the threads (i.e. calculate the random number lists)
    for j in jobs:
      j.start()

    # Ensure all of the threads have finished
    for j in jobs:
      j.join()

    #https://stackoverflow.com/questions/1720421/how-to-concatenate-two-lists-in-python
    l_img=[]; l_seg=[]; l_bound=[]; l_bound_class=[]; l_energy=[]; l_name=[]
    for i in range(0, threads):
      l_img += list_images[i]
      l_seg += list_labels_seg[i]
      l_bound += list_labels_bound[i]
      l_bound_class += list_labels_contour_class[i]
      l_energy += list_labels_dist[i]
      l_name += list_img_name[i]

    return [l_img, l_seg, l_bound, l_bound_class, l_energy, l_name]

  def preprocessing_data(self, state, dir_img, dir_label, dir_depth):
    if state == 'train':
      images_dir = self.images_dir_train
      depths_dir = self.depth_dir_train
      labels_dir = self.labels_dir_train
    elif state == 'val':
      images_dir = self.images_dir_val
      depths_dir = self.depth_dir_val
      labels_dir = self.labels_dir_val
    elif state == 'test':
      images_dir = self.images_dir_test
      depths_dir = self.depth_dir_test
      labels_dir = self.labels_dir_test
    else:
      assert(False), 'no implemented test yet'
    image = self.read_image(path_img=images_dir+dir_img,color="RGB")
    depth_gt = self.read_image(path_img=depths_dir+dir_depth,color="BIL")/255.0
    gt_seg = self.read_image(path_img=labels_dir+dir_label,color="RGB")
    semantic_gt = gt_seg
    #'''
    semantic_gt = np.ones((image.shape[0], image.shape[1]))*len(self.labels)
    for item in self.labels:
      if item.trainId == 5: continue
      if item.trainId == 4:
        semantic_gt[ np.all(gt_seg == np.array([0,0,0]), axis=2) ] = item.trainId
      else:
        semantic_gt[ np.all(gt_seg == np.array(item.color), axis=2) ] = item.trainId
    #'''
    #instance_semantic_gt = semantic_gt * 100 + instance_gt
    boundary_gt = np.array(find_boundaries(semantic_gt, mode = 'thick', connectivity=1)).astype(np.uint8)
    contour_mask = dilation(boundary_gt, self.struct_elem)
    #semantic_gt_relabel = np.zeros_like(semantic_gt)
    #--------------- set to 19+1 classes ---------------
    #for label_real, relabel in self.label_map_id.items(): #python3 .items(), python2 .iteritems()
    #  semantic_gt_relabel[semantic_gt == label_real] = relabel
    #--------------------------------------------------------
    im_name = dir_img.split('/')[-1]
    #contour_class_gt = semantic_gt * contour_mask
    contour_class_gt = (semantic_gt+1) * contour_mask
    contour_class_gt -= 1
    contour_class_gt[contour_class_gt == -1] = 5 #blackground
    '''
    #-------------------------- energy level general ----------------------------
    #dist_mask_neg = (-1.)*(semantic_gt == 0).astype(np.float32) #unlabel
    dist_mask_pos = (semantic_gt != 5).astype(np.float32) #labels
    #dist_mask = dist_mask_neg + dist_mask_pos
    dist_transf = ndimage.distance_transform_edt(np.abs(1.0-boundary_gt)) * dist_mask_pos
    R_thr = 50. #D(p) = [+1,-1]min( min d(p, q), R)
    dist_transf[dist_transf > R_thr] = R_thr
    dist_transf_quantize = self.QuantizeDistance(value=np.max(dist_transf), K=self.K, img_dist=dist_transf)
    '''
    #'''
    #------------------------ energy level by instances -------------------------
    dist_transf_quantize = np.zeros((self.img_h, self.img_w))
    dist_mask_pos = (semantic_gt != 5).astype(np.float32) #labels
    for class_mask in [0,2]:
      obj_class = np.squeeze(np.array([semantic_gt == class_mask]).astype(np.int32))
      dist_transf = ndimage.distance_transform_edt(obj_class)
      R_thr = 40 #D(p) = [+1,-1]min( min d(p, q), R)
      dist_transf[dist_transf > R_thr] = R_thr
      dist_transf_quantize += self.QuantizeDistance(value=np.max(dist_transf), K=self.K, img_dist=dist_transf)
      dist_transf_quantize *= dist_mask_pos
      '''
      blobs_labels = measure.label(obj_class, background=0)
      blobs_labels_uni = np.unique(blobs_labels)
      for label_comp in blobs_labels_uni:
        obj_comp_uni = blobs_labels == label_comp
        dist_transf = ndimage.distance_transform_edt(obj_comp_uni*blobs_labels)
        #dist_transf = normalize(dist_transf)
        if self.regression:
          dist_transf_quantize += normalize(dist_transf) #[0,1] #standardize(dist_transf) #normalize(dist_transf)
        else:
          #------------------------ quantize the distance transform -------------------------
          R_thr = 40 #D(p) = [+1,-1]min( min d(p, q), R)
          dist_transf[dist_transf > R_thr] = R_thr
          dist_transf_quantize += self.QuantizeDistance(value=np.max(dist_transf), K=self.K, img_dist=dist_transf)
      dist_transf_quantize *= dist_mask_pos
      '''
    #'''
    '''
    self.writeImage(contour_class_gt.astype(np.uint8), "contour_"+im_name)
    self.writeImage(semantic_gt.astype(np.uint8), "seg_"+im_name)
    b_bound = Image.fromarray(boundary_gt.astype(np.uint8)*255)
    b_bound.save("bound_"+im_name)
    self.writeFloatImage(dist_transf_quantize, "dist_"+im_name)
    assert(False)
    '''

    return image.astype(np.uint8), semantic_gt.astype(np.uint8), boundary_gt.astype(np.uint8),\
      contour_class_gt.astype(np.uint8), dist_transf_quantize, im_name #dist_transf_quantize.astype(np.uint8), im_name

  def load_bull2(self, state, procnum, dir_img, dir_label, dir_depth, l_img, l_seg, l_bound, l_bound_class, l_energy, l_name):
    '''load all the dataset on memory using threads'''
    #import time
    for idx in range(len(dir_img)):
      image, semantic_gt_relabel, boundary_gt, contour_class_gt, dist_transf_quantize, \
        im_name = self.preprocessing_data(state, dir_img[idx], dir_label[idx], dir_depth[idx])
      l_img.append(image) #[0,255]
      l_seg.append(semantic_gt_relabel) #[0,19]
      l_bound.append(boundary_gt) #[0,1]
      l_bound_class.append(contour_class_gt) #[0,19]
      l_energy.append(dist_transf_quantize) #[0,K]
      l_name.append(im_name)
    #end = time.time()
    #print("time: ", end - start)

  def load_bull(self, state, num_data, dir_img, dir_label, dir_depth, show_processing=False):
    '''load all the dataset on memory'''
    images = np.zeros((num_data, self.img_h, self.img_w, 3))
    labels_seg = np.zeros((num_data, self.img_h, self.img_w))
    labels_bound = np.zeros((num_data, self.img_h, self.img_w))
    labels_contour_class = np.zeros((num_data, self.img_h, self.img_w))
    labels_dist = np.zeros((num_data, self.img_h, self.img_w))
    img_name = []
    if show_processing:
      pbar = tqdm(total=num_data)
      pbar.set_description('load')
    for idx in range(len(dir_img)):
      if show_processing:
        pbar.update(1)
      image, semantic_gt_relabel, boundary_gt, contour_class_gt, dist_transf_quantize, \
        im_name = self.preprocessing_data(state, dir_img[idx], dir_label[idx], dir_depth[idx])
      images[idx] = image #[0,255]
      labels_seg[idx] = semantic_gt_relabel #[0,5]
      labels_bound[idx] = boundary_gt #[0,1]
      labels_contour_class[idx] = contour_class_gt #[0,5]
      labels_dist[idx] = dist_transf_quantize #[0,K]
      img_name.append(im_name)
    if show_processing:
      pbar.close()
    return [np.array(images), np.array(labels_seg), np.array(labels_bound), np.array(labels_contour_class), \
            np.array(labels_dist), img_name]

  def batch_generator_test(self, db_img, dir_img):
    def gen_batch(batch_size):
      for offset in range(0, len(db_img), batch_size):
        files_img = db_img[offset:offset+batch_size]
        images = []
        for idx in range(len(files_img)):
          image = self.read_image(path_img=dir_img+"/"+files_img[idx],color="RGB")
          images.append(image.astype(np.uint8))
        yield np.array(images)
    return gen_batch

  def batch_generator_bull(self, data_img, data_label_seg, data_bound, data_contour_class, data_energy, data_name):
    def gen_batch(batch_size):
      #print("batch_size batch_size: ", batch_size)
      for offset in range(0, len(data_img), batch_size):
        images = data_img[offset:offset+batch_size]
        labels_seg = data_label_seg[offset:offset+batch_size]
        labels_bound = data_bound[offset:offset+batch_size]
        labels_contour_class = data_contour_class[offset:offset+batch_size]
        labels_dist = data_energy[offset:offset+batch_size]
        img_name = data_name[offset:offset+batch_size]
        yield np.array(images), np.array(labels_seg), np.array(labels_bound), np.array(labels_contour_class), \
              np.array(labels_dist), img_name
    return gen_batch

  def next_batch(self, state, batch_size):
    '''Return the next batch_size examples from this data set'''
    num_data = self.get_size_dataset(state)
    dataset = self.get_dataset(state)

    data_img = dataset[0]
    data_label_seg = dataset[1]
    data_bound = dataset[2]
    data_contour_class = dataset[3]
    data_energy = dataset[4]
    data_name = dataset[5]

    start_bt = self._index_in_epoch
    self._index_in_epoch += batch_size
    end_bt = self._index_in_epoch
    images = data_img[start_bt:end_bt]
    labels_seg = data_label_seg[start_bt:end_bt]
    labels_bound = data_bound[start_bt:end_bt]
    labels_contour_class = data_contour_class[start_bt:end_bt]
    labels_dist = data_energy[start_bt:end_bt]
    img_name = data_name[start_bt:end_bt]
    if self._index_in_epoch >= num_data:
      self._index_in_epoch = 0
    return np.array(images), np.array(labels_seg), np.array(labels_bound), np.array(labels_contour_class), \
          np.array(labels_dist), img_name

  def batch_generator(self, state, dir_img, dir_label, dir_depth):
    def gen_batch(batch_size):
      #print("batch_size batch_size: ", batch_size)
      for offset in range(0, len(dir_img), batch_size):
        files_img = dir_img[offset:offset+batch_size]
        files_label = dir_label[offset:offset+batch_size]
        files_depth = dir_depth[offset:offset+batch_size]
        data_proc = self.load_bull(state, len(files_img), files_img, files_label, files_depth)
        #num_train_set
        yield np.array(data_proc[0]), np.array(data_proc[1]), np.array(data_proc[2]), np.array(data_proc[3]), \
              np.array(data_proc[4]), data_proc[5]
    return gen_batch


def prove_read_dataset_training():
    #"/datasets/KITTI/training/"
    #"/datasets/Cityscapes_aug2", 6000
    #/work/Cityscapes_aug3/
    #"/datasets/FreiburgForest/Aug"
    FreiburgForest = FREIBURG_FOREST(input_paste="/datasets/FreiburgForest/Aug", num_classes=5+1, use_threads=12*4, \
                                        energy_level=5, regression=False, num_data_load=None)#6000)
    FreiburgForest.load_data(size=(384,768))
    epochs = 1
    batch_size = 50
    for ep in range(epochs):
      counter = 0
      #gen_training = FreiburgForest.train_generator(batch_size=10)#.train_generator(batch_size=10)
      #for batch_img, batch_gt_seg, batch_gt_bound, batch_gt_contourclass, batch_gt_dist, batch_nameimg  in gen_training:
      total_batch = int(FreiburgForest.num_train_set/batch_size)
      for i in range(total_batch):
        batch_img, batch_gt_seg, batch_gt_bound, batch_gt_contourclass, batch_gt_dist, \
          batch_nameimg = FreiburgForest.next_batch(state='train', batch_size=batch_size)
        print("batch_img: ", batch_img.shape )
        print("batch_gt_seg: ", batch_gt_seg.shape )
        print("batch_gt_bound: ", batch_gt_bound.shape )
        print("batch_gt_contourclass: ", batch_gt_contourclass.shape )
        #print(batch_nameimg)
        print("unique seg: ", np.unique(batch_gt_seg))
        print("unique bound class: ", np.unique(batch_gt_contourclass))
        print("unique ener: ", np.unique(batch_gt_dist))
        #assert(False)

        for idx in range( len(batch_img) ):
          batch_nameimg[idx] = batch_nameimg[idx].replace('.jpg', '.png')
          print(batch_nameimg[idx])
          bt_img = Image.fromarray(batch_img[idx].astype(np.uint8))
          bt_img_name = "tmp/img_"+batch_nameimg[idx]
          bt_img.save(bt_img_name)
          lb_seg_name = "tmp/lb_seg_"+batch_nameimg[idx]
          FreiburgForest.writeImage(batch_gt_seg[idx].astype(np.uint8), lb_seg_name)
          batch_gt_bound[idx] *= 255
          lb_bound = Image.fromarray(batch_gt_bound[idx].astype(np.uint8))
          lb_bound_name = "tmp/lb_bound_"+batch_nameimg[idx]
          lb_bound.save(lb_bound_name)
          lb_cntourclass_name = "tmp/lb_cntourclass_"+batch_nameimg[idx]
          FreiburgForest.writeImage(batch_gt_contourclass[idx].astype(np.uint8), lb_cntourclass_name, True)
          #figsize = w/float(dpi_), h/float(dpi_)
          #fig = plt.figure(figsize=figsize)
          #ax = fig.add_axes([0, 0, 1, 1])
          #ax.axis('off')
          #ax.imshow(batch_gt_dist[idx], cmap='plasma') #'viridis', 'plasma', 'gray', 'afmhot',
          #fig.savefig('tmp/lb_dist_'+batch_nameimg[idx], pad_inches=0, dpi=dpi_)
          lb_dist_name = 'tmp/lb_dist_'+batch_nameimg[idx]
          FreiburgForest.writeFloatImage(batch_gt_dist[idx], lb_dist_name)

          paste_merge = 'tmp/merge/merge_'+batch_nameimg[idx]
          os.system("montage "+bt_img_name+" "+lb_bound_name+" "+lb_cntourclass_name+" "\
                  +lb_seg_name+" "+lb_dist_name+" -geometry +3+2 "+paste_merge)
          os.system("rm "+bt_img_name)
          os.system("rm "+lb_bound_name)
          os.system("rm "+lb_seg_name)
          os.system("rm "+lb_dist_name)
          os.system("rm "+lb_cntourclass_name)

          #for i in range(10):
          #  im1, imseg, imb, imbc,imenerg = kitti.random_crop(im=bt_img, seg=batch_gt_seg[idx].astype(np.uint8), bound=lb_bound,\
          #    bound_class=batch_gt_contourclass[idx].astype(np.uint8), energy=batch_gt_dist[idx], crop_dims=(300, 500))
          #  im1.save("tmp/img"+str(i)+"_"+batch_nameimg[idx])
        print("------------------------------------------------------")
        break

        print("###########################################################")

def run_data_augmentation():
  #input_paste = "/datasets/FreiburgForest/freiburg_forest_annotated"
  #paste_out = "/datasets/FreiburgForest/Aug"
  FreiburgForest = FREIBURG_FOREST(input_paste="/datasets/FreiburgForest/freiburg_forest_annotated", num_classes=5+1, num_data_load=None)
  FreiburgForest.data_augmentation(paste_out="/datasets/FreiburgForest/Aug", size=(384, 768), num_data_load=None)

def run_weight_classes():
  num_classes = 20
  energy_level = 5
  kitti = CITYSCAPES(input_paste="/datasets/Cityscapes_aug2", num_classes=num_classes, energy_level=energy_level, use_threads=11, num_data_load=None)
  mean, wss, wbc, wel = kitti.compute_class_weights(size=(300, 500))
  np.set_printoptions(formatter={'float': '{: 0.6f}'.format})
  print("mean: ", mean)
  print("wss: ", wss, np.sum(wss))
  print("wbc: ", wbc, np.sum(wbc))
  print("wel: ", wel, np.sum(wel))

def convert_tfrecord(state='train'):
  cityspaces = CITYSCAPES(input_paste="/work/Cityscapes_aug3", num_classes=11+1, use_threads=12*4, energy_level=5, regression=True, num_data_load=600)#None)#6000)
  cityspaces.load_data(size=(384,768))
  cityspaces.convert2tfrecod(record_name='cityscape_aug_tfrecord_11_384x768_reg_'+state, state=state)
  #cityspaces.convert2tfrecod(record_name='/datasets/Cityscapes/cityscape_aug_tfrecord_11_384x768_'+state, state=state)

if __name__ == '__main__':
    print("run some function")
    prove_read_dataset_training()
    #run_data_augmentation()
    #run_weight_classes()
    #convert_tfrecord(state='train')
