from skimage.segmentation import find_boundaries
from skimage.morphology import erosion, dilation, disk, square
from skimage import measure
from matplotlib import animation
import matplotlib.pyplot as plt
plt.rcParams.update({'figure.max_open_warning': 0})
plt.switch_backend('agg')
from scipy import ndimage
from pathlib import Path
from PIL import Image, ImageEnhance
from tqdm import tqdm
import numpy as np
import random
import time
import cv2
import os
import multiprocessing

random.seed(time.time())

import tensorflow as tf

def normalize(x):
  #normalized_x = x / np.linalg.norm(x) #slow operation
  #normalized_x = x / np.sqrt( np.sum( x**2 ) )
  if np.max(x) != np.min(x):
    normalized_x = ( x - np.min(x) ) / ( np.max(x) - np.min(x) ) #[0,1]
  else:
    normalized_x = x
  return normalized_x

def standardize(x):
  standardize_x = (x - np.mean(x)) / np.std(x)
  return standardize_x

def _int64_feature(data):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[data]))

def _bytes_feature(data):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[data]))

class CAMVID:

    def __init__( self, input_paste="../input", num_classes=11, use_threads=10, energy_level=5, regression = False, num_data_load=None):
      self.input_paste = input_paste
      self.num_classes = num_classes
      self.num_data_load = num_data_load
      self.struct_elem = disk(2)
      self.K = energy_level
      self._index_in_epoch = 0
      self.use_threads = use_threads
      self.regression = regression

      self.labelmap = [ #all -1
                      (1,'Sky'),
                      (2,'Building'),
                      (3,'Pole'),
                      (4,'Road'),
                      (5,'Pavement'),
                      (6,'Tree'),
                      (7,'SignSymbol'),
                      (8,'Fence'),
                      (9,'Car'),
                      (10,'Pedestrian'),
                      (11,'Bicyclist')
                      #(12,'Unlabelled')
                      ]
      #Sky, Building, Pole, Road, Pavement, Tree, SignSymbol, Fence, Car, Pedestrian, Bicyclist, Unlabelled
      self.labeldict = {
                      0 : 0,
                      1 : 1,
                      2 : 2,
                      3 : 3,
                      4 : 4,
                      5 : 5,
                      6 : 6,
                      7 : 7,
                      8 : 8,
                      9 : 9,
                      10 : 10,
                      11 : 11 } #unlabel

    def read_image(self, path_img, color="RGB"):
      if color == "BIN":
          img = np.array(cv2.imread(path_img,cv2.IMREAD_UNCHANGED))
          img = img//1000
          #img = img.astype(np.uint8)
      elif color == "GRAY":
          img = np.array(cv2.imread(path_img,cv2.IMREAD_GRAYSCALE))
      else:
          img = np.array(cv2.imread(path_img, cv2.IMREAD_COLOR))
          #img = cv2.resize(img,(self.img_w, self.img_h), interpolation = cv2.INTER_CUBIC)
          #img = np.array(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))

      sz = img.shape
      w = sz[0]; h = sz[1]
      if w != self.img_w or h != self.img_h:
        img = cv2.resize(img,(self.img_w, self.img_h), interpolation = cv2.INTER_NEAREST)
        if color == "RGB":
          img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

      return np.array(img).astype(np.float32)

    def get_mean_channel(self):
      """get mean of each channel for subtracted in the dataset"""
      return [99.6253963, 103.3297624, 105.65772592] #11 + 1 classes

    def restart_initbatch(self):
      self._index_in_epoch = 0

    def get_calculated_frequency(self):
      ################ 11 + 1 classes ################
      seg_freq = np.array([0.042242, 0.026913, 0.718872, 0.020649, 0.118831, 0.057543, 0.576841,\
          0.414539, 0.128936, 1.000000, 0.907232, 0.185243])
      #seg_freq = seg_freq +  weight * self.eval

      bound_classifier_freq = np.array([0.110495, 0.098856, 0.326339, 0.191673, 0.226480, 0.142671, \
          0.519899, 0.688737, 0.329424, 0.685475, 1.000000, 0.003190])
      #bound_classifier_freq = bound_classifier_freq +  weight * self.eval

      energy_freq = np.array([0.005919, 0.338735, 0.374905, 0.493336, 0.703038, 1.000000])

      return seg_freq, bound_classifier_freq, energy_freq

    def writeImage(self, image, filename):
        """ store label data to colored image """
        Sky = [128,128,128]
        Building = [128,0,0]
        Pole = [192,192,128]
        Road_marking = [255,69,0]
        Road = [128,64,128]
        Pavement = [60,40,222]
        Tree = [128,128,0]
        SignSymbol = [192,128,128]
        Fence = [64,64,128]
        Car = [64,0,128]
        Pedestrian = [64,64,0]
        Bicyclist = [0,128,192]
        Unlabelled = [0,0,0]
        r = image.copy()
        g = image.copy()
        b = image.copy()
        label_colours = np.array([Sky, Building, Pole, Road, Pavement, Tree, SignSymbol, Fence, Car, Pedestrian, Bicyclist, Unlabelled])
        #label_colours = np.array([Unlabelled, Sky, Building, Pole, Road, Pavement, Tree, SignSymbol, Fence, Car, Pedestrian, Bicyclist])
        for l in range(0,12):
            r[image==l] = label_colours[l,0]
            g[image==l] = label_colours[l,1]
            b[image==l] = label_colours[l,2]
        rgb = np.zeros((image.shape[0], image.shape[1], 3))
        rgb[:,:,0] = r/1.0
        rgb[:,:,1] = g/1.0
        rgb[:,:,2] = b/1.0
        im = Image.fromarray(np.uint8(rgb))
        im.save(filename)

    def writeImage_color(self, image, filename):
        #BGR
        img = image[:,:,np.argsort([2,1,0])]
        cv2.imwrite(filename,img.astype(np.uint8))

    def convert2tfrecod(self, record_name, state='train'):
        #https://www.tensorflow.org/tutorials/load_data/tfrecord#reading_a_tfrecord_file
        num_data = self.get_size_dataset(state)
        dataset = self.get_dataset(state)

        data_img = dataset[0]
        data_label_seg = dataset[1]
        data_bound = dataset[2]
        data_contour_class = dataset[3]
        data_energy = dataset[4]
        data_name = dataset[5]

        count = 0.0
        writer = tf.python_io.TFRecordWriter(record_name+'.tfrecords')

        #for name in f:
        for idx in range(num_data):
            modality1 = data_img[idx] #cv2.imread(name[0])
            label = data_label_seg[idx] #cv2.imread(name[1], cv2.IMREAD_ANYCOLOR)
            bound = data_bound[idx]
            contour_class = data_contour_class[idx]
            energy = data_energy[idx]
            try:
              assert len(label.shape)==2
            except AssertionError as e:
              raise( AssertionError( "Label should be one channel!" ) )
                
            height = modality1.shape[0]
            width = modality1.shape[1]
            modality1 = modality1.tostring()
            label = label.tostring()
            bound = bound.tostring()
            contourclass = contour_class.tostring()
            energy = energy.tostring()
            features = {'height':_int64_feature(height),
                        'width':_int64_feature(width),
                        'modality1':_bytes_feature(modality1),
                        'label':_bytes_feature(label),
                        'bound':_bytes_feature(bound),
                        'contourclass':_bytes_feature(contourclass),
                        'energy':_bytes_feature(energy),
                       }
            example = tf.train.Example(features=tf.train.Features(feature=features))
            writer.write(example.SerializeToString())

            if (count+1)%1 == 0:
              print('Processed data: {}'.format(count))

            count = count+1

    def writeFloatImage(self, image, filename, dpi_=300, cmap_='plasma'):
      if len(image.shape) == 3:
        h, w, c = image.shape
      else:
        h, w = image.shape
      image = image.reshape((h,w))
      figsize = w/float(dpi_), h/float(dpi_)
      fig = plt.figure(figsize=figsize)
      ax = fig.add_axes([0, 0, 1, 1])
      ax.axis('off')
      #cmap styles https://matplotlib.org/examples/color/colormaps_reference.html
      ax.imshow(image, cmap=cmap_) #'viridis', 'plasma', 'gray', 'afmhot',
      fig.savefig(filename, pad_inches=0, dpi=dpi_)

    def random_crop(self, im, seg, crop_dims, preserve_size=False, resample=Image.NEAREST):
      """
      Args:
          im:         PIL image
          crop_dims:  Dimensions of the crop region [width, height]
          offset:     Position of the crop box from Top Left corner [x, y]
          preserve_size: (bool) Should it resize back to original dims?
          resample:       resampling method during rescale.
      Returns:
          PIL image of size crop_size, randomly cropped from `im`.
      """
      crop_width, crop_height = crop_dims
      crop_height, crop_width = crop_dims
      width, height = im.size
      x_offset = np.random.randint(0, width - crop_width + 1)
      y_offset = np.random.randint(0, height - crop_height + 1)
      im2 = im.crop((x_offset, y_offset, x_offset + crop_width, y_offset + crop_height))
      im2seg = seg.crop((x_offset, y_offset, x_offset + crop_width, y_offset + crop_height))
      im2 = self.random_brightness(im2)
      #im2 = self.random_contrast(im2)
      if preserve_size:
          im2 = im2.resize(im.size, resample=resample)
          im2seg = im2seg.resize(im.size, resample=resample)
      return im2, im2seg


    def random_brightness(self, im, sd=0.5, min=1.0, max=15):
      """Creates a new image which randomly adjusts the brightness of `im` by
         randomly sampling a brightness value centered at 1, with a standard
         deviation of `sd` from a normal distribution. Clips values to a
         desired min and max range.
      Args:
          im:   PIL image
          sd:   (float) Standard deviation used for sampling brightness value.
          min:  (int or float) Clip contrast value to be no lower than this.
          max:  (int or float) Clip contrast value to be no higher than this.
      Returns:
          PIL image with brightness randomly adjusted.
      """
      brightness = np.clip(np.random.normal(loc=1, scale=sd), min, max)
      enhancer = ImageEnhance.Brightness(im)
      return enhancer.enhance(brightness)


    def random_contrast(self, im, sd=0.5, min=0.9, max=2):
      """Creates a new image which randomly adjusts the contrast of `im` by
         randomly sampling a contrast value centered at 1, with a standard
         deviation of `sd` from a normal distribution. Clips values to a
         desired min and max range.
      Args:
          im:   PIL image
          sd:   (float) Standard deviation used for sampling contrast value.
          min:  (int or float) Clip contrast value to be no lower than this.
          max:  (int or float) Clip contrast value to be no higher than this.
      Returns:
          PIL image with contrast randomly adjusted.
      """
      contrast = np.clip(np.random.normal(loc=1, scale=sd), min, max)
      enhancer = ImageEnhance.Contrast(im)
      return enhancer.enhance(contrast)

    #"""
    def data_augmentation(self, paste_out="/datasets/CamVid_aug", size=(360, 480)):
      self.img_h = size[0] #img_h
      self.img_w = size[1] #img_w

      # print files into input paste
      print(os.listdir(self.input_paste))

      # Define some paths first
      self.input_dir = Path(self.input_paste)
      self.images_dir_train = str(self.input_dir / 'train')
      self.labels_dir_train = str(self.input_dir / 'trainannot')
      self.images_dir_val = str(self.input_dir / 'val')
      self.labels_dir_val = str(self.input_dir / 'valannot')

      print(self.input_dir)
      print(self.images_dir_train)
      print(self.labels_dir_train)
      print(self.images_dir_val)
      print(self.labels_dir_val)

      train_images = sorted(os.listdir(self.images_dir_train))
      train_labels = sorted(os.listdir(self.labels_dir_train))
      val_images = sorted(os.listdir(self.images_dir_val))
      val_labels = sorted(os.listdir(self.labels_dir_val))

      #train_images = train_images[0:2]
      #train_labels = train_labels[0:2]
      #val_images = val_images[0:2]
      #val_labels = val_labels[0:2]

      print("len train: ", len(train_images))
      print("len labels: ", len(val_images))

      self.num_train_set  = len(train_images)
      self.num_dev_set = len(val_images)
      self.num_data_set = self.num_train_set + self.num_dev_set

      print("num_data_set: ", self.num_data_set, "\nnum_train_set: ", \
          self.num_train_set, "\nnum_dev_set: ", self.num_dev_set)


      #image, semantic_gt_relabel, boundary_gt, contour_class_gt, dist_transf_quantize, \
      #            im_name = self.preprocessing_data(train_images+"/"+files_img[idx], dir_label+"/"+files_label[idx])
      for i in range(2):
        if i == 1:
          self.images_dir_train = self.images_dir_val
          self.labels_dir_train = self.labels_dir_val
          train_images = val_images
          train_labels = val_labels
          self.num_train_set = self.num_dev_set

        for idx in range(self.num_train_set):
          os.system("cp "+self.images_dir_train+'/'+train_images[idx]+' '+paste_out+'/train/'+train_images[idx])
          os.system("cp "+self.labels_dir_train+'/'+train_labels[idx]+' '+paste_out+'/trainannot/'+train_labels[idx])

          image = self.read_image(path_img=self.images_dir_train+'/'+train_images[idx],color="RGB").astype(np.uint8)
          label = self.read_image(path_img=self.labels_dir_train+'/'+train_labels[idx],color="GRAY").astype(np.uint8)

          #data_augmentation horizontal flip
          image_flip = Image.fromarray(image).transpose(Image.FLIP_LEFT_RIGHT)
          #image_flip = image_flip.resize(size=(self.img_w,self.img_h), resample=Image.NEAREST)
          gt_flip = Image.fromarray(label).transpose(Image.FLIP_LEFT_RIGHT)
          #gt_flip = gt_flip.resize(size=(self.img_w,self.img_h), resample=Image.NEAREST)

          im_name_ = train_images[idx].split(".")[0]+"_flip.png"
          image_flip.save(paste_out+'/train/'+im_name_)
          gt_flip.save(paste_out+'/trainannot/'+im_name_)

          # crop
          for i in range(5):
            crop_image, crop_semantic_gt = self.random_crop(im=Image.fromarray(image),
              seg=Image.fromarray(label), crop_dims=(260, 346), preserve_size=True)
            #crop_image = self.random_brightness(crop_image)
            #crop_image = self.random_contrast(crop_image)

            im_name_ = train_images[idx].split(".")[0]+"_crop"+str(i+1)+".png"
            print("--->im_name: ", im_name_)
            #save crop images
            crop_image.save(paste_out+'/train/'+im_name_)
            crop_semantic_gt.save(paste_out+'/trainannot/'+im_name_)
            #self.writeImage(np.array(crop_semantic_gt), paste_out+'/trainannot/'+im_name_)

            crop_image_flip, crop_semantic_gt_flip = self.random_crop(im=image_flip, \
              seg=gt_flip, crop_dims=(260, 346), preserve_size=True)

            im_name_ = train_images[idx].split(".")[0]+"_crop_flip"+str(i+1)+".png"
            print("--->im_name: ", im_name_)
            #save crop images
            crop_image_flip.save(paste_out+'/train/'+im_name_)
            crop_semantic_gt_flip.save(paste_out+'/trainannot/'+im_name_)

    #"""

    def load_bull_thread(self, list_img, list_label, num_data, dir_img, dir_label):
      from multiprocessing.managers import SyncManager
      import signal
      def mgr_init():
          '''initializer for SyncManager'''
          signal.signal(signal.SIGINT, signal.SIG_IGN)

      threads = self.use_threads
      num_data_thread = num_data / threads
      #http://jtushman.github.io/blog/2014/01/14/python-%7C-multiprocessing-and-interrupts/
      manager = SyncManager()
      manager.start(mgr_init) # explicitly starting the manager, and telling it to ignore the interrupt signal
      list_images = [[]] * threads
      list_labels_seg = [[]] * threads
      list_labels_bound = [[]] * threads
      list_labels_contour_class = [[]] * threads
      list_labels_dist = [[]] * threads
      list_img_name = [[]] * threads

      for i in range(0, threads):
        list_images[i] = manager.list()
        list_labels_seg[i] = manager.list()
        list_labels_bound[i] = manager.list()
        list_labels_contour_class[i] = manager.list()
        list_labels_dist[i] = manager.list()
        list_img_name[i] = manager.list()

      jobs = []
      for i in range(0, threads):
        #out_list = list()
        thread = multiprocessing.Process( target=self.load_bull2, args=(i, \
          list_img, list_label, \
          dir_img[int(i*num_data_thread):min(int((i+1)*num_data_thread), num_data)], \
          dir_label[int(i*num_data_thread):min(int((i+1)*num_data_thread), num_data)], \
          list_images[i], list_labels_seg[i], list_labels_bound[i], list_labels_contour_class[i], \
          list_labels_dist[i], list_img_name[i]))
        jobs.append(thread)
        #out_list2.append(out_list)
      # Start the threads (i.e. calculate the random number lists)
      for j in jobs:
        j.start()

      # Ensure all of the threads have finished
      for j in jobs:
        j.join()

      #https://stackoverflow.com/questions/1720421/how-to-concatenate-two-lists-in-python
      l_img=[]; l_seg=[]; l_bound=[]; l_bound_class=[]; l_energy=[]; l_name=[]
      for i in range(0, threads):
        l_img += list_images[i]
        l_seg += list_labels_seg[i]
        l_bound += list_labels_bound[i]
        l_bound_class += list_labels_contour_class[i]
        l_energy += list_labels_dist[i]
        l_name += list_img_name[i]

      return [l_img, l_seg, l_bound, l_bound_class, l_energy, l_name]

    def preprocessing_data(self, dir_img, dir_label):
      image = self.read_image(path_img=dir_img,color="RGB")
      #image = cv2.resize(image.astype(np.uint8),(self.img_w, self.img_h), interpolation = cv2.INTER_CUBIC)
      label = self.read_image(path_img=dir_label,color="GRAY")
      label_pool = label
      '''
      label_pool = np.zeros([self.img_h, self.img_w], dtype=np.uint8)

      for class_pixel in self.labelmap:
        label_curr = (label == class_pixel[0]).astype(np.bool_)
        #label_curr = cv2.resize(label_curr.astype(np.uint8),(self.img_w, self.img_h), interpolation = cv2.INTER_NEAREST)
        label_pool += (label_curr.astype(np.uint8) * self.labeldict[class_pixel[0]]) #lalbedict
      '''

      boundary_gt = np.array(find_boundaries(label_pool, mode = 'thick', connectivity=1)).astype(np.uint8)
      contour_mask = dilation(boundary_gt, self.struct_elem)
      contour_class_gt = (label_pool+1) * contour_mask
      contour_class_gt[contour_class_gt == 0] = 12
      contour_class_gt -= 1

      component_car = measure.label(np.array([label_pool==8]), background=0)[0]
      component_car = dilation(erosion(component_car, self.struct_elem), self.struct_elem)
      component_person = measure.label(np.array([label_pool==10]), background=0)[0]
      component_person = dilation(erosion(component_person, self.struct_elem), self.struct_elem)
      component_gt = component_car + (np.max(component_car)+component_person)*[component_person>0]
      
      dist_transf_quantize = np.zeros((self.img_h, self.img_w))
      components_masks = np.unique(component_gt)
      for component_mask in components_masks:
        if component_mask == 0:
          continue
        obj_class = np.squeeze(np.array([component_gt == component_mask]).astype(np.int32))
        dist_transf = ndimage.distance_transform_edt(obj_class)
        if self.regression:
          dist_transf_quantize += normalize(dist_transf) #[0,1] #standardize(dist_transf) #normalize(dist_transf)
        else:
          R_thr = 9*np.max(dist_transf)/10 #150 #D(p) = [+1,-1]min( min d(p, q), R)
          dist_transf[dist_transf >= R_thr] = R_thr
          dist_transf /= R_thr
          dist_transf_quantize += self.QuantizeDistance(value=np.max(dist_transf), K=self.K, img_dist=dist_transf) #[1,K+1]
      #boundary_gt = boundary_gt - erosion(boundary_gt, self.struct_elem)
      #boundary_gt = erosion(boundary_gt, self.struct_elem)
      return image.astype(np.uint8), label_pool.astype(np.uint8), boundary_gt.astype(np.uint8),\
      contour_class_gt.astype(np.uint8), dist_transf_quantize, dir_img.split("/")[-1]


    def compute_class_weights(self, size):
      '''
      references: https://github.com/GeorgeSeif/Semantic-Segmentation-Suite/blob/master/utils/utils.py
      Arguments:
          labels_dir(list): Directory where the image segmentation labels are
          num_classes(int): the number of classes of pixels in all images
      Returns:
          class_weights(list): a list of class weights where each index represents each class label and the element is the class weight for that label.
      '''
      label_values_ss = np.arange(self.num_classes)
      num_classes = len(label_values_ss)
      class_pixels_ss = np.zeros((num_classes))
      class_pixels_bc = np.zeros((num_classes))
      total_pixels_ss = 0.0
      total_pixels_bc = 0.0

      label_values_el = np.arange(self.K+1)
      energy_level = len(label_values_el)
      class_pixels_el = np.zeros((self.K+1))
      total_pixels_el = 0.0

      self.img_h = size[0]
      self.img_w = size[1]

      # print files into input paste
      print(os.listdir(self.input_paste))

      # Define some paths first
      self.input_dir = Path(self.input_paste)
      self.images_dir_train = str(self.input_dir / 'train')
      self.labels_dir_train = str(self.input_dir / 'trainannot')
      self.images_dir_val = str(self.input_dir / 'val')
      self.labels_dir_val = str(self.input_dir / 'valannot')

      train_images = sorted(os.listdir(self.images_dir_train))
      train_labels = sorted(os.listdir(self.labels_dir_train))
      val_images = sorted(os.listdir(self.images_dir_val))
      val_labels = sorted(os.listdir(self.labels_dir_val))

      train_images_allpath = [ self.images_dir_train+"/"+p for p in train_images ]
      train_labels_allpath = [ self.labels_dir_train+"/"+p for p in train_labels ]
      val_images_allpath = [ self.images_dir_val+"/"+p for p in val_images ]
      val_labels_allpath = [ self.labels_dir_val+"/"+p for p in val_labels ]
      
      label_files = train_labels_allpath + val_labels_allpath
      image_files = train_images_allpath + val_images_allpath
  
      means = np.zeros((3))
      for n in range(len(image_files)):
        image = self.read_image(path_img=image_files[n],color="RGB")
        for c in range(3):
          means[c] += np.sum(image[:,:,c])
      means /= (image.shape[0] * image.shape[1] * len(image_files))
      print("means: ", means)

      for n in range(len(label_files)):
        #image = self.read_image(path_img=label_files[n],color="GRAY")
        #instance_semantic_gt = self.read_image(path_list_imagesimg=labels_dir+dir_label[n],color="BIL")
        image, semantic_gt_relabel, boundary_gt, contour_class_gt, dist_transf_quantize, \
        im_name = self.preprocessing_data(image_files[n], label_files[n])

        for index, colour in enumerate(label_values_ss):
          #------- semantic segmentation -------------
          class_map_ss = (semantic_gt_relabel == colour)
          class_map_ss = class_map_ss.astype(np.float32)
          class_pixels_ss[index] += np.sum(class_map_ss)
          # ------------ boundary classifier -------------
          class_map_bc = (contour_class_gt == colour)
          class_map_bc = class_map_bc.astype(np.float32)
          class_pixels_bc[index] += np.sum(class_map_bc)

        for index, level in enumerate(label_values_el):
          # ------------ energy level -----------------
          class_map_el = (dist_transf_quantize == level)
          class_map_el = class_map_el.astype(np.float32)
          class_pixels_el[index] += np.sum(class_map_el)

      # ------------ semantic segmentation -------------
      total_pixels_ss = float(np.sum(class_pixels_ss))
      index_to_delete = np.argwhere(class_pixels_ss==0.0)
      class_pixels_ss = np.delete(class_pixels_ss, index_to_delete)
      class_weights_ss = total_pixels_ss / class_pixels_ss
      #class_weights_ss = class_weights_ss / np.sum(class_weights_ss)
      class_weights_ss = class_weights_ss / np.max(class_weights_ss)

      # ------------ boundary classifier -------------
      total_pixels_bc = float(np.sum(class_pixels_bc))
      index_to_delete = np.argwhere(class_pixels_bc==0.0)
      class_pixels_bc = np.delete(class_pixels_bc, index_to_delete)
      class_weights_bc = total_pixels_bc / class_pixels_bc
      #class_weights_bc = class_weights_bc / np.sum(class_weights_bc)
      class_weights_bc = class_weights_bc / np.max(class_weights_bc)

      # ------------ energy level -----------------
      total_pixels_el = float(np.sum(class_pixels_el))
      index_to_delete = np.argwhere(class_pixels_el==0.0)
      class_pixels_el = np.delete(class_pixels_el, index_to_delete)
      class_weights_el = total_pixels_el / class_pixels_el
      #class_weights_el = class_weights_el / np.sum(class_weights_el)
      class_weights_el = class_weights_el / np.max(class_weights_el)

      return means, class_weights_ss, class_weights_bc, class_weights_el

    def QuantizeDistance(self, value, K, img_dist):
      "Quantize the values in the pixel-wise map into K uniform bins"
      range_bin = value / (K *  1.0)
      current_bin = 1e-12 #0
      img_quantize = np.zeros_like(img_dist)
      label_bin = 1
      #while( current_bin < value ):
      while( label_bin < K+1 ):
        lower = current_bin
        upper = current_bin + range_bin
        img_quantize += label_bin * ((img_dist > lower)&(img_dist <= upper)).astype(np.uint8)
        current_bin = upper#+= range_bin
        label_bin += 1
      #img_quantize += (label_bin - 1) * (img_dist > upper)
      #img_quantize += 1
      return img_quantize

    def get_size_dataset(self, state):
      '''return dataset size of the state'''
      if state == 'train':
        num_data = self.num_train_set
      elif state == 'val':
        num_data = self.num_dev_set
      elif state == 'test':
        num_data = self.num_test_set
      else:
        assert(False), 'error of state'
      return num_data

    def get_dataset(self, state):
      '''return data of the state'''
      if state == 'train':
        dataset = self.data_train
      elif state == 'val':
        dataset = self.data_val
      elif state == 'test':
        dataset = self.data_test
      else:
        assert(False), 'error of state'
      return dataset

    def load_data(self, size=(224, 224)):
        """size: (img_h, img_w) """
        self.img_h = size[0] #img_h
        self.img_w = size[1] #img_w

        # print files into input paste
        print(os.listdir(self.input_paste))

        # Define some paths first
        self.input_dir = Path(self.input_paste)
        self.images_dir_train = str(self.input_dir / 'train')
        self.labels_dir_train = str(self.input_dir / 'trainannot')
        self.images_dir_val = str(self.input_dir / 'val')
        self.labels_dir_val = str(self.input_dir / 'valannot')
        self.images_dir_test = str(self.input_dir / 'test')
        self.labels_dir_test = str(self.input_dir / 'testannot')

        print(self.input_dir)
        print(self.images_dir_train)
        print(self.labels_dir_train)
        print(self.images_dir_val)
        print(self.labels_dir_val)
        print(self.images_dir_test)
        print(self.labels_dir_test)

        train_images = sorted(os.listdir(self.images_dir_train))
        train_labels = sorted(os.listdir(self.labels_dir_train))
        val_images = sorted(os.listdir(self.images_dir_val))
        val_labels = sorted(os.listdir(self.labels_dir_val))
        test_images = sorted(os.listdir(self.images_dir_test))
        test_labels = sorted(os.listdir(self.labels_dir_test))

        #self.test_images = sorted(os.listdir(self.images_dir_test))

        if self.num_data_load != None:
            train_images = train_images[0:self.num_data_load]
            train_labels = train_labels[0:self.num_data_load]

        #test set module batch 6 = 228
        #test set module batch 9 = 225
        #test set module batch 7 = 224
        #test_images = test_images[0:224] 
        #test_labels = test_labels[0:224]

        print("len train: ", len(train_images))
        print("len labels: ", len(val_images))
        print("len test: ", len(test_images))

        #for i in range(10):
        #    print(train_images[i])

        merge_image_and_label_train = list(zip(train_images, train_labels))
        random.shuffle(merge_image_and_label_train)
        self.train_images, self.train_labels = zip(*merge_image_and_label_train)

        merge_image_and_label_val = list(zip(val_images, val_labels))
        random.shuffle(merge_image_and_label_val)
        self.val_images, self.val_labels = zip(*merge_image_and_label_val)

        #merge_image_and_label_test = list(zip(test_images, test_labels))
        #random.shuffle(merge_image_and_label_test)
        #self.test_images, self.test_labels = zip(*merge_image_and_label_test)
        self.test_images, self.test_labels = test_images, test_labels

        #print("-----------")
        #for i in range(10):
        #    print(self.train_images[i], " - ", self.train_labels[i] )

        self.num_train_set  = len(train_images)
        self.num_dev_set = len(val_images)
        self.num_test_set = len(test_images)
        self.num_data_set = self.num_train_set + self.num_dev_set + self.num_test_set

        print("num_data_set: ", self.num_data_set, "\nnum_train_set: ", \
            self.num_train_set, "\nnum_dev_set: ", self.num_dev_set, \
            "\nnum_test_set: ", self.num_test_set)
        '''
        self.train_generator = self.batch_generator(self.train_images, self.train_labels,\
            self.images_dir_train, self.labels_dir_train)
        self.valid_generator = self.batch_generator(self.val_images,  self.val_labels,\
            self.images_dir_val, self.labels_dir_val)
        self.test_generator = self.batch_generator(self.test_images, self.test_labels,\
        self.images_dir_test, self.labels_dir_test)
        '''
        '''
        self.data_train = self.load_bull(self.num_train_set, self.train_images, self.train_labels,\
            self.images_dir_train, self.labels_dir_train, True)
        self.data_val = self.load_bull(self.num_dev_set, self.val_images,  self.val_labels,\
            self.images_dir_val, self.labels_dir_val, True)
        self.data_test = self.load_bull(self.num_test_set, self.test_images, self.test_labels,\
            self.images_dir_test, self.labels_dir_test, True)
        '''

        #preprocessing all the dataset by threads
        self.data_train = self.load_bull_thread(self.images_dir_train, self.labels_dir_train, \
                            self.num_train_set, self.train_images, self.train_labels)
        self.data_val = self.load_bull_thread(self.images_dir_val, self.labels_dir_val, \
                            self.num_dev_set, self.val_images,  self.val_labels)
        self.data_test = self.load_bull_thread(self.images_dir_test, self.labels_dir_test, \
                            self.num_test_set, self.test_images, self.test_labels)

        self.train_generator = self.batch_generator_bull(self.data_train[0], self.data_train[1], \
          self.data_train[2], self.data_train[3], self.data_train[4], self.data_train[5])
        self.valid_generator = self.batch_generator_bull(self.data_val[0], self.data_val[1], \
          self.data_val[2], self.data_val[3], self.data_val[4], self.data_val[5])
        self.test_generator = self.batch_generator_bull(self.data_test[0], self.data_test[1], \
          self.data_test[2], self.data_test[3], self.data_test[4], self.data_test[5])
        
    def load_bull2(self, procnum, list_img, list_label, dir_img, dir_label, l_img, l_seg, l_bound, l_bound_class, l_energy, l_name):
      '''load all the dataset on memory using threads'''
      #import time
      for idx in range(len(dir_img)):
        image, semantic_gt_relabel, boundary_gt, contour_class_gt, dist_transf_quantize, \
          im_name = self.preprocessing_data(list_img+"/"+dir_img[idx], list_label+"/"+dir_label[idx])
        l_img.append(image) #[0,255]
        l_seg.append(semantic_gt_relabel) #[0,19]
        l_bound.append(boundary_gt) #[0,1]
        l_bound_class.append(contour_class_gt) #[0,19]
        l_energy.append(dist_transf_quantize) #[0,K]
        l_name.append(im_name)


    def load_bull(self, num_data, files_img, files_label, dir_img, dir_label, show_progress=False):
      '''load all the dataset on memory'''
      #import time
      images = np.zeros((num_data, self.img_h, self.img_w, 3))
      labels_seg = np.zeros((num_data, self.img_h, self.img_w))
      labels_bound = np.zeros((num_data, self.img_h, self.img_w))
      labels_contour_class = np.zeros((num_data, self.img_h, self.img_w))
      labels_dist = np.zeros((num_data, self.img_h, self.img_w))
      img_name = []
      if show_progress:
        pbar = tqdm(total=num_data)
        pbar.set_description('load')
      #start = time.time()
      for idx in range(len(files_img)):
        if show_progress:
          pbar.update(1)
        image, semantic_gt_relabel, boundary_gt, contour_class_gt, dist_transf_quantize,\
        im_name = self.preprocessing_data(dir_img+"/"+files_img[idx], dir_label+"/"+files_label[idx])
        images[idx] = image #[0,255]
        labels_seg[idx] = semantic_gt_relabel #[0,19]
        labels_bound[idx] = boundary_gt #[0,1]
        labels_contour_class[idx] = contour_class_gt #[0,19]
        labels_dist[idx] = dist_transf_quantize #[0,K]
        img_name.append(im_name)
      if show_progress:
        pbar.close()
      #end = time.time()
      #print("time: ", end - start)
      return [np.array(images), np.array(labels_seg), np.array(labels_bound), np.array(labels_contour_class), \
            np.array(labels_dist), img_name]

    def next_batch(self, state, batch_size):
      '''Return the next batch_size examples from this data set'''
      num_data = self.get_size_dataset(state)
      dataset = self.get_dataset(state)

      data_img = dataset[0]
      data_label_seg = dataset[1]
      data_bound = dataset[2]
      data_contour_class = dataset[3]
      data_energy = dataset[4]
      data_name = dataset[5]

      start_bt = self._index_in_epoch
      self._index_in_epoch += batch_size
      end_bt = self._index_in_epoch
      images = data_img[start_bt:end_bt]
      labels_seg = data_label_seg[start_bt:end_bt]
      labels_bound = data_bound[start_bt:end_bt]
      labels_contour_class = data_contour_class[start_bt:end_bt]
      labels_dist = data_energy[start_bt:end_bt]
      img_name = data_name[start_bt:end_bt]
      if self._index_in_epoch >= num_data:
        self._index_in_epoch = 0
      return np.array(images), np.array(labels_seg), np.array(labels_bound), np.array(labels_contour_class), \
            np.array(labels_dist), img_name

    def batch_generator_bull(self, data_img, data_label_seg, data_bound, data_contour_class, data_energy, data_name):
      def gen_batch(batch_size):
        #print("batch_size batch_size: ", batch_size)
        #print("data_img.type: ", type(data_img))
        for offset in range(0, len(data_img), batch_size):
          images = data_img[offset:offset+batch_size]
          labels_seg = data_label_seg[offset:offset+batch_size]
          labels_bound = data_bound[offset:offset+batch_size]
          labels_contour_class = data_contour_class[offset:offset+batch_size]
          labels_dist = data_energy[offset:offset+batch_size]
          img_name = data_name[offset:offset+batch_size]
          yield np.array(images), np.array(labels_seg), np.array(labels_bound), np.array(labels_contour_class), \
                np.array(labels_dist), img_name
      return gen_batch

    def batch_generator_test(self, db_img, dir_img):
        def gen_batch(batch_size):
            for offset in range(0, len(db_img), batch_size):
                files_img = db_img[offset:offset+batch_size]
                images = []
                for idx in range(len(files_img)):
                    image = self.read_image(path_img=dir_img+"/"+files_img[idx],color="RGB")
                    images.append(image.astype(np.uint8))
                yield np.array(images)
        return gen_batch

    def batch_generator(self, db_img, db_label, dir_img, dir_label):
        def gen_batch(batch_size):
            merge_img_and_label = list(zip(db_img, db_label))
            random.shuffle(merge_img_and_label)
            db_img_rand, db_label_rand = zip(*merge_img_and_label)

            print("batch_size batch_size: ", batch_size)
            for offset in range(0, len(db_img), batch_size):
                files_img = db_img_rand[offset:offset+batch_size]
                files_label = db_label_rand[offset:offset+batch_size]

                images = []
                labels_orig = []
                label_edge = []
                label_contour_class = []
                label_dist = []
                list_name = []
                for idx in range(len(files_img)):
                  image, semantic_gt_relabel, boundary_gt, contour_class_gt, dist_transf_quantize, \
                  im_name = self.preprocessing_data(dir_img+"/"+files_img[idx], dir_label+"/"+files_label[idx])
                  images.append(image)
                  labels_orig.append(semantic_gt_relabel)
                  label_edge.append(boundary_gt)
                  label_contour_class.append(contour_class_gt)
                  label_dist.append(dist_transf_quantize)
                  list_name.append(im_name)
                yield np.array(images), np.array(labels_orig), np.array(label_edge), np.array(label_contour_class), \
                      np.array(label_dist), list_name
        return gen_batch


def prove_read_dataset_training():
    data_camvid = CAMVID(input_paste="/datasets/CamVid", num_classes=12, use_threads=10*2, energy_level=5, regression=True, num_data_load=None)
    data_camvid.load_data(size=(360,480))#(int(360/5), int(480/5)))
    epochs = 1
    batch_size=10
    for ep in range(epochs):
        #gen_training = data_camvid.train_generator(batch_size=30)
        #for batch_img, batch_gt_seg, batch_gt_bound, batch_gt_contourclass, batch_gt_dist, name_lb  in gen_training:
        counter = 0
        total_batch = int(data_camvid.num_train_set/batch_size)
        for i in range(total_batch):
            batch_img, batch_gt_seg, batch_gt_bound, batch_gt_contourclass, batch_gt_dist, \
              name_lb = data_camvid.next_batch(state='train', batch_size=batch_size)
            #batch_gt_contourclass, batch_gt_dist, batch_nameimg
            #print( "batch_img: ", len(batch_img), " batch_label: ", batch_label[0] )
            print("batch_img: ", batch_img.shape)
            print("batch_gt_seg: ", batch_gt_seg.shape)
            print("batch_gt_bound: ", batch_gt_bound.shape)
            print("batch_gt_contourclass: ", batch_gt_contourclass.shape)
            print("batch_gt_dist: ", batch_gt_dist.shape)

            #batch_label_bin = np.zeros_like(batch_label)
            #batch_label_bin[batch_label == 8] = 1.0

            for idx in range( len(batch_img) ):
                #bt_img = Image.fromarray(batch_img[idx].astype(np.uint8))
                #bt_img.save("tmp/img_"+name_lb[idx])
                #lb_bin = Image.fromarray(batch_label_bin[idx].astype(np.uint8))
                #lb_bin.save("tmp/lb_bin_"+name_lb[idx])
                bt_img_name = "tmp/img_"+name_lb[idx]
                data_camvid.writeImage_color(image=batch_img[idx].astype(np.uint8),filename=bt_img_name)
                lb_seg_name="tmp/lb_seg_"+name_lb[idx]
                data_camvid.writeImage(image=batch_gt_seg[idx], filename=lb_seg_name)
                #data_camvid.writeFloatImage(image=(batch_gt_seg[idx]==8)*255, filename="tmp/lb_seg_"+name_lb[idx])
                lb_bound_name = "tmp/b_"+name_lb[idx]
                data_camvid.writeFloatImage(image=batch_gt_bound[idx], filename=lb_bound_name)
                lb_cntourclass_name = "tmp/lb_cntclass_"+name_lb[idx]
                data_camvid.writeImage(image=batch_gt_contourclass[idx], filename=lb_cntourclass_name)
                lb_dist_name = "tmp/dist_"+name_lb[idx]
                data_camvid.writeFloatImage(image=batch_gt_dist[idx], filename=lb_dist_name)

                paste_merge = 'tmp/merge/merge_'+name_lb[idx]
                os.system("montage "+bt_img_name+" "+lb_bound_name+" "+lb_cntourclass_name+" "\
                        +lb_seg_name+" "+lb_dist_name+" -geometry +3+2 "+paste_merge)
                os.system("rm "+bt_img_name)
                os.system("rm "+lb_seg_name)
                os.system("rm "+lb_dist_name)
                os.system("rm "+lb_cntourclass_name)
                os.system("rm "+lb_bound_name)

                #cv2.namedWindow('img', cv2.WINDOW_NORMAL) #WINDOW_AUTOSIZE, WINDOW_OPENGL, WINDOW_NORMAL
                #img = batch_xs[0]#.astype(np.uint8)
                #img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
                #cv2.imshow('img',img)
                #cv2.namedWindow('lb', cv2.WINDOW_NORMAL) #WINDOW_AUTOSIZE, WINDOW_OPENGL, WINDOW_NORMAL
                #cv2.imshow('lb',set_class_batch[0])
                #cv2.imshow('lb',batch_ys[0,:,:,0].astype(np.uint8))
                #cv2.waitKey(0)
                #cv2.destroyAllWindows()
                #pos_display = [341,342,343,344,345,346,347,348,349]#, 3410, 3411, 3412]
                """
                fig1 = plt.figure()
                #splt.subplots_adjust(wspace=0)#(wspace=0, hspace=0)
                #fig.suptitle("Classes of the dataset", fontsize=12)
                counter = 1
                for subclass in range(batch_label_classes.shape[3]):
                    plt.subplot(4,4,counter).set_xticks([])
                    plt.subplot(4,4,counter).set_yticks([])
                    if( subclass == batch_label_classes.shape[3] - 1 ):
                        plt.title("background",fontsize=9)
                    else:
                        plt.title(data_camvid.labelmap[subclass][1],fontsize=9)
                    plt.imshow(batch_label_classes[idx,:,:,subclass])
                    counter += 1

                plt.subplot(4,4,counter)#.set_yticks([])

                plt.title("label",fontsize=9)

                #plt.tight_layout()
                #fig1.tight_layout()
                #fig1.subplots_adjust(left=0.0, right=1.0, down=0.0, top=1.0)
                #fig1.subplots_adjust(hspace=0.8, wspace=0.8)
                #fig1.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
                plt.imshow(batch_label[idx])

                #plt.figure()
                fig2 = plt.figure()
                plt.subplot(121)
                plt.title("Image Input")
                plt.imshow(batch_img[idx])
                plt.subplot(122)
                plt.title("Label")
                plt.imshow(batch_label[idx])
                #fig2.tight_layout()
                plt.show()
                counter += 1
                break
                if(counter == 6):
                    break
                """
            #print("------------------------------------------------------")
            break
        print("###########################################################")

def run_weight_classes():
  num_classes =11+1
  energy_level = 5
  camvid = CAMVID(input_paste="/datasets/CamVid", num_classes=num_classes, energy_level=energy_level, num_data_load=100)#None)
  mean, wss, wbc, wel = camvid.compute_class_weights(size=(360, 480))
  np.set_printoptions(formatter={'float': '{: 0.6f}'.format})
  print("mean: ", mean)
  print("wss: ", wss, np.sum(wss))
  print("wbc: ", wbc, np.sum(wbc))
  print("wel: ", wel, np.sum(wel))

def run_data_aug():
  num_classes =11+1
  energy_level = 5
  camvid = CAMVID(input_paste="/datasets/CamVid", num_classes=num_classes, energy_level=energy_level, num_data_load=None)#None)
  #camvid.data_augmentation(paste_out="/datasets/CamVid_aug", size=(360, 480))
  camvid.data_augmentation(paste_out="/work/CamVid_aug", size=(360, 480))

def convert_tfrecord(state='train'):
  data_camvid = CAMVID(input_paste="/datasets/CamVid", num_classes=12, use_threads=10*2, energy_level=5, regression=False, num_data_load=None)
  data_camvid.load_data(size=(360,480))
  data_camvid.convert2tfrecod(record_name='camvid_aug_tfrecord_360x480_'+state, state=state)
  #cityspaces.convert2tfrecod(record_name='/datasets/Cityscapes/camvid_aug_tfrecord_360x450_reg_'+state, state=state)
  

if __name__ == '__main__':
    print("run some function")
    #prove_read_dataset_training()
    #run_data_aug()
    #run_weight_classes()
    convert_tfrecord(state='train')
    
