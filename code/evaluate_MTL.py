import argparse
import datetime
import importlib
import os
import sys
import numpy as np
import tensorflow as tf
import yaml
from PIL import Image
from dataset.helper import *
from models.AdapNet_pp_MTL_multigpu import AdapNet_pp_MTL
from models.SegNet_MTL_multigpu import SegNet_MTL
from models.UNet_MTL_multigpu import UNet_MTL
from models.FCN8_MTL_multigpu import FCN8_MTL
from models.ENet_MTL_multigpu import ENet_MTL
from models.DeconvNet_MTL_multigpu import DeconvNet_MTL
from models.FCDenseNet_MTL_multigpu import FCDenseNet_MTL
from models.FastNet_MTL_multigpu import FastNet_MTL
from models.ParseNet_MTL_multigpu import ParseNet_MTL
from summary.summary import CTensorBoard

from dataset.CamVid_read import CAMVID
from dataset.Cityscape_read_final import CITYSCAPES
from dataset.FreiburgForest_read import FREIBURG_FOREST

PARSER = argparse.ArgumentParser()
PARSER.add_argument('-c', '--config', default='config/cityscapes_test.config')

def test_func(config):
    os.environ['CUDA_VISIBLE_DEVICES'] = config['gpu_id']
    name_model = config['model']

    # placeholders
    batch_size_pl = tf.placeholder(tf.int64, shape=[], name="batch_size")
    images_pl = tf.placeholder(tf.float32, [None, config['height'], config['width'], 3])
    labels_S_pl1 = tf.placeholder(tf.float32, [None, config['height'], config['width'], 1])
    labels_E_pl = tf.placeholder(tf.float32, [None, config['height'], config['width'], 1])
    labels_C_pl1 = tf.placeholder(tf.float32, [None, config['height'], config['width'], 1])
    labels_D_pl1 = tf.placeholder(tf.float32, [None, config['height'], config['width'], 1])
    labels_S_pl = tf.one_hot(tf.cast(tf.squeeze(labels_S_pl1,axis=-1),tf.int64), config['num_classes'])
    labels_C_pl = tf.one_hot(tf.cast(tf.squeeze(labels_C_pl1,axis=-1),tf.int64), config['num_classes'])
    if config['regression']:
      labels_D_pl = labels_D_pl1
    else:
      labels_D_pl = tf.one_hot(tf.cast(tf.squeeze(labels_D_pl1,axis=-1),tf.int64), config['energy_level'])

    if name_model == 'SegNet':
      model_build = SegNet_MTL
    elif name_model == 'UNet':
      model_build = UNet_MTL
    elif name_model == 'DeconvNet':
      model_build = DeconvNet_MTL
    elif name_model == 'ENet':
      model_build = ENet_MTL
    elif name_model == 'FCN8':
      model_build = FCN8_MTL
    elif name_model == 'FCDenseNet67':
      model_build = FCDenseNet_MTL
    elif name_model == 'AdapNet_pp':
      model_build = AdapNet_pp_MTL
    elif name_model == 'FastNet':
      model_build = FastNet_MTL
    elif name_model == 'ParseNet':
      model_build = ParseNet_MTL
    else:
      sys.exit('name model undefined, [SegNet,AdapNet_pp, UNet, DeconvNet, FCN8, FCDenseNet67, FastNet, ParseNet]')
  
    TT_S = 'S' in config['tasks']
    TT_E = 'E' in config['tasks']
    TT_C = 'C' in config['tasks']
    TT_D = 'D' in config['tasks']

    with tf.variable_scope(name_model):
        model = model_build(num_classes=config['num_classes'], energy_level=config['energy_level'], \
                float_type=tf.float32, training=False, ignore_label=True, has_aux_loss=False,\
                S=TT_S, E=TT_E, C=TT_C, D=TT_D,  regression=config['regression'])
        logits_S, aux1_S, aux2_S, logits_E, logits_C, aux1_C, aux2_C, logits_D, aux1_D, aux2_D = \
            model.forward(images_pl, batch_size_pl)

    config1 = tf.ConfigProto()
    config1.gpu_options.allow_growth = True
    sess = tf.Session(config=config1)
    sess.run(tf.global_variables_initializer())
    import_variables = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES)
    print('total_variables_loaded:', len(import_variables))
    from tensorflow.python.tools.inspect_checkpoint import print_tensors_in_checkpoint_file
    saver = tf.train.Saver(import_variables)
    saver.restore(sess, config['checkpoint'])

    if config['dataset'] == 'camvid':
      num_classes = config['num_classes']  # 11+1
      energy_level = config['energy_level'] # 5+1
      #input_paste="/datasets/CamVid/", 360
      #input_paste="/datasets/CamVid_aug/", None 5616
      dataset = CAMVID(input_paste="/datasets/CamVid_aug/", num_classes=num_classes, \
              energy_level=energy_level-1, use_threads=12*4, regression=config['regression'], num_data_load=150)
      dataset.load_data(size=(360, 480)) 
    elif config['dataset'] == 'cityscape_11':
      num_classes = config['num_classes']  # 11+1
      energy_level = config['energy_level'] # 5+1 
      # /work/Cityscapes_aug3/train   17850 # 17850 - (17850 % config['batch_size']*len(devices))
      # /work/Cityscapes_aug3/val     500
      # /work/Cityscapes_aug3/test    1525
      dataset = CITYSCAPES(input_paste="/drw/Cityscapes_aug3", \
                    num_classes=num_classes, energy_level=energy_level-1, use_threads=12*4, num_data_load=\
                    #17850 - (17850 % (config['batch_size']*len(devices)) ) )
                    150)
      dataset.load_data(size=(384,768))
      #mean_channel = dataset.get_mean_channel()
    elif  config['dataset'] == 'freiburgforest':
      # /datasets/FreiburgForest/Aug/train   1840
      num_classes = config['num_classes'] # 5+1
      energy_level = config['energy_level'] # 5+1 
      dataset = FREIBURG_FOREST(input_paste="/datasets/FreiburgForest/Aug", num_classes=num_classes, \
          use_threads=12*4, energy_level=5, regression=config['regression'], num_data_load=\
          150 )
      dataset.load_data(size=(384,768))
    elif config['dataset'] == 'synthia':
      num_classes = config['num_classes']  # 11+1
      energy_level = config['energy_level'] # 5+1
      #input_paste="/datasets/synthia/", 9400 (7k train) (2.4k test)
      #input_paste="/datasets/synthia/Aug/", None 56000
      dataset = SYNTHIA(input_paste="/datasets/SYNTHIA/Aug", num_classes=num_classes, \
              energy_level=energy_level-1, use_threads=12*4, regression=config['regression'], num_data_load=\
              150 )
      dataset.load_data(size=(384,768)) 
    else:
      sys.exit('dataset undefined, [camvid, cityscape_11, freiburgforest]')
  
    tasks = 'S'
    if model.T_E: tasks += 'E'
    if model.T_C: tasks += 'C'
    if model.T_D: tasks += 'D'
    if model.regression: tasks += 'R'

    step = 0
    total_num = 0
    output_matrix_S = np.zeros([config['num_classes'], 3])
    output_matrix_E = np.zeros([2, 3])
    output_matrix_C = np.zeros([config['num_classes'], 3])
    output_matrix_D = np.zeros([config['energy_level'], 3])
    
    print(config['model'] + " --- " + config['dataset'] + " --- " + config['tasks'] )

    bt_sz = config['batch_size']
    testing_set = None
    if config['dataset'] == 'camvid':
      testing_set = 'test'
    elif config['dataset'] == 'cityscape_11':
      testing_set = 'val'
    elif config['dataset'] == 'freiburgforest':
      testing_set = 'test'
    else:
      sys.exit('dataset undefined, [camvid, cityscape_11, freiburgforest]')

    total_batch = int(dataset.get_size_dataset(testing_set)/bt_sz)
    for i in range(total_batch):
      batch_img, batch_gt_S, batch_gt_E, batch_gt_C, batch_gt_D, \
          batch_nameimg = dataset.next_batch(state=testing_set, batch_size=bt_sz)
      if len(batch_img) == 0:
        continue
      for j in range(len(batch_nameimg)):
        batch_nameimg[j] = batch_nameimg[j].replace('.jpg','.png')
      batch_gt_S_ = np.expand_dims(batch_gt_S, axis=-1)
      batch_gt_E_ = np.expand_dims(batch_gt_E, axis=-1)
      batch_gt_C_ = np.expand_dims(batch_gt_C, axis=-1)
      batch_gt_D_ = np.expand_dims(batch_gt_D, axis=-1)

      feed_dict = {images_pl:batch_img, labels_S_pl1:batch_gt_S_, labels_E_pl:batch_gt_E_, \
          labels_C_pl1:batch_gt_C_, labels_D_pl1:batch_gt_D_, batch_size_pl:bt_sz}
      fetches = [ logits_S ] 
      if model.T_E: fetches += [ logits_E ]
      if model.T_C: fetches += [ logits_C ]
      if model.T_D: fetches += [ logits_D ]

      res = sess.run(fetches, feed_dict)
      prob_S = res[0]
      cnt_res = 1
      if model.T_E: 
        prob_E = res[cnt_res]
        cnt_res += 1
      if model.T_C: 
        prob_C = res[cnt_res]
        cnt_res += 1
      if model.T_D: 
        prob_D = res[cnt_res]
        cnt_res += 1

      prediction_S = np.argmax(prob_S, 3)
      gt_S = batch_gt_S
      prediction_S[gt_S == config['unlabel']] = config['unlabel']
      output_matrix_S = compute_output_matrix(gt_S, prediction_S, output_matrix_S)
      if model.T_E: 
        prediction_E = (np.squeeze(prob_E,axis=-1) > 0.5)
        gt_E = batch_gt_E
        output_matrix_E = compute_output_matrix(gt_E, prediction_E, output_matrix_E)
      if model.T_C: 
        prediction_C = np.argmax(prob_C, 3)
        gt_C = batch_gt_C
        prediction_C[gt_C == config['unlabel']] = config['unlabel']
        output_matrix_C = compute_output_matrix(gt_C, prediction_C, output_matrix_C)
      if model.T_D: 
        prediction_D = np.argmax(prob_D, 3)
        gt_D = batch_gt_D
        prediction_D[gt_D == 0] = 0
        output_matrix_D = compute_output_matrix(gt_D, prediction_D, output_matrix_D)
      '''
      for k in range(len(batch_img)):
        #paste_out_pred = config['saved_results']+'/'+tasks+'/seg_pred/'+batch_nameimg[k]
        #dataset.writeImage(prediction_ss[k].astype(np.uint8), paste_out_pred) #save predicted segmentation
        #"""
        # probability prediction for roc curve
        paste_out_prob = config['saved_results']+'/'+tasks+'/seg_pred_prob/'+batch_nameimg[k]
        paste_out_prob = paste_out_prob.replace('.png', '.npy')
        np.save(paste_out_prob, prob_ss[k])
        paste_gt_prob = config['saved_results']+'/'+tasks+'/seg_gt_prob/'+batch_nameimg[k]
        paste_gt_prob = paste_gt_prob.replace('.png', '.npy')
        np.save(paste_gt_prob, gt_ss[k])
        #"""
        #paste_img = name_cam = config['saved_results']+'/'+tasks+'/img/'+batch_nameimg[k]
        #bt_img = Image.fromarray(batch_img[k].astype(np.uint8))
        #bt_img.save(paste_img)

        #paste_gt = name_cam = config['saved_results']+'/'+tasks+'/seg_gt/'+batch_nameimg[k]
        #dataset.writeImage(batch_gt_seg[k].astype(np.uint8), paste_gt)
      '''

      step += 1
    sess.close()
    print("------------------------------------------")
    print('SmIoU: ', compute_iou(output_matrix_S, config['unlabel'], True), 'EmIoU: ', compute_iou(output_matrix_E, 0), \
        'CmIoU: ', compute_iou(output_matrix_C, config['unlabel']), 'DmIoU: ', compute_iou(output_matrix_D, 0))

def main():
    args = PARSER.parse_args()
    if args.config:
        file_address = open(args.config)
        config = yaml.load(file_address)
    else:
        print('--config config_file_address missing')
    test_func(config)

if __name__ == '__main__':
    main()
