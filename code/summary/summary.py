
import os
import numpy as np
import tensorflow as tf

class CTensorBoard():
  def __init__(self): #, S=True, B=False, C=False, E=False):
    self.acc_S_ph = tf.placeholder(tf.float32, shape=None, name='acc_S_ph')
    self.iou_S_ph = tf.placeholder(tf.float32, shape=None, name='iou_S_ph')
    self.iou_E_ph = tf.placeholder(tf.float32, shape=None, name='iou_E_ph')
    self.iou_C_ph = tf.placeholder(tf.float32, shape=None, name='iou_C_ph')
    self.iou_D_ph = tf.placeholder(tf.float32, shape=None, name='iou_D_ph')
    
    self.loss_S_cross_ph = tf.placeholder(tf.float32, shape=None, name='loss_S_cross_ph')
    self.loss_S_iou_ph = tf.placeholder(tf.float32, shape=None, name='loss_S_iou_ph')
    self.loss_E_cross_ph = tf.placeholder(tf.float32, shape=None, name='loss_E_cross_ph')
    self.loss_E_iou_ph = tf.placeholder(tf.float32, shape=None, name='loss_E_iou_ph')
    self.loss_C_cross_ph = tf.placeholder(tf.float32, shape=None, name='loss_C_cross_ph')
    self.loss_C_iou_ph = tf.placeholder(tf.float32, shape=None, name='loss_C_iou_ph')
    self.loss_D_cross_ph = tf.placeholder(tf.float32, shape=None, name='loss_D_cross_ph')
    self.loss_D_iou_ph = tf.placeholder(tf.float32, shape=None, name='loss_D_iou_ph')
    self.loss_ph = tf.placeholder(tf.float32, shape=None, name='loss_ph')

    with tf.device('/cpu:0'):
      with tf.name_scope('Loss'):
        self.loss_sc = tf.summary.scalar("general", self.loss_ph)
        self.loss_S_cross_sc = tf.summary.scalar("S_cross", self.loss_S_cross_ph)
        self.loss_S_iou_sc = tf.summary.scalar("S_iou", self.loss_S_iou_ph)
        self.loss_E_cross_sc = tf.summary.scalar("E_cross", self.loss_E_cross_ph)
        self.loss_E_iou_sc = tf.summary.scalar("E_iou", self.loss_E_iou_ph)
        self.loss_C_cross_sc = tf.summary.scalar("C_cross", self.loss_C_cross_ph)
        self.loss_C_iou_sc = tf.summary.scalar("C_iou", self.loss_C_iou_ph)
        self.loss_D_cross_sc = tf.summary.scalar("D_cross", self.loss_D_cross_ph)
        self.loss_D_iou_sc = tf.summary.scalar("D_iou", self.loss_D_iou_ph)

      with tf.name_scope('Metrics'):
        self.acc_S = tf.summary.scalar("S_acc", self.acc_S_ph)
        self.iou_S = tf.summary.scalar("S_iou", self.iou_S_ph)
        self.iou_E = tf.summary.scalar("E_iou", self.iou_E_ph)
        self.iou_C = tf.summary.scalar("C_iou", self.iou_C_ph)
        self.iou_D = tf.summary.scalar("D_iou", self.iou_D_ph)


def main():
    TB = CTensorBoard()

if __name__ == '__main__':
    main()  
