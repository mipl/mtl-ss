import tensorflow as tf
from models import network_base
from loss.losses import LossFunc

class ENet_MTL(network_base.Network):
    def __init__(self, num_classes=12, energy_level=6, learning_rate=0.001, float_type=tf.float32, weight_decay=0.0005,
                 decay_steps=30000, power=0.9, training=True, ignore_label=True, global_step=0,
                 has_aux_loss=True, S=True, E=False, C=False, D=False, regression=False):
        
        super(ENet_MTL, self).__init__()
        self.num_classes = num_classes
        self.learning_rate = learning_rate
        self.weight_decay = weight_decay
        self.initializer = 'he'
        self.has_aux_loss = has_aux_loss
        self.float_type = float_type
        self.power = power
        self.decay_steps = decay_steps
        self.training = training
        self.bn_decay_ = 0.99
        self.global_step = global_step
        self.energy_level = energy_level
        self.regression = regression

        self.T_S = S
        self.T_E = E
        self.T_C = C
        self.T_D = D
      
        self.loss_func = LossFunc()

        if self.training:
            self.keep_prob = 0.3
        else:
            self.keep_prob = 1.0
        if ignore_label:
            self.weights = tf.ones(self.num_classes-1)
            self.weights = tf.concat((tf.zeros(1), self.weights), 0)
            self.weights_el = tf.ones(self.energy_level-1)
            self.weights_el = tf.concat((tf.zeros(1), self.weights_el), 0)
        else:
            self.weights = tf.ones(self.num_classes)
            self.weights_el = tf.ones(self.energy_level)

    def PReLU(self, x, scope):
        alpha = tf.get_variable(scope + "/alpha", shape=[1],
                    initializer=tf.constant_initializer(0), dtype=tf.float32)

        output = tf.nn.relu(x) + alpha*(x - abs(x))*0.5

        return output

    def initial_block(self, x, scope):        
        # convolution branch:
        conv_branch = self.conv_bias(x, 3, 2, 13, name='W')
        
        # max pooling branch:
        pool_branch = self.pool(x, 2, 2)

        # concatenate the branches:
        concat = tf.concat([conv_branch, pool_branch], axis=3)

        with tf.variable_scope(scope):
            # apply batch normalization and PReLU:
            output = self.batch_norm(concat)
            output = self.PReLU(output, scope=scope)

            return output


    def encoder_bottleneck_regular(self, x, output_depth, scope, proj_ratio=4, downsampling=False):
        with tf.variable_scope(scope):
            input_shape = x.get_shape().as_list()
            input_depth = input_shape[3]

            internal_depth = int(output_depth/proj_ratio)

            # convolution branch:
            conv_branch = x

            # # 1x1 projection:
            if downsampling:
                conv_branch = self.conv2d(conv_branch, 2, 2, internal_depth, name='W_proj', padding='VALID')
            else:
                conv_branch = self.conv2d(conv_branch, 1, 1, internal_depth, name='W_proj', padding='VALID')
            with tf.variable_scope('W_proj'):
                conv_branch = self.batch_norm(conv_branch)
                conv_branch = self.PReLU(conv_branch, scope=scope + "/proj")

            # # conv
            conv_branch = self.conv_bias(conv_branch, 3, 1, internal_depth, 'W_conv')
            with tf.variable_scope('W_conv'):
                conv_branch = self.batch_norm(conv_branch)
                conv_branch = self.PReLU(conv_branch, scope=scope + "/conv")
            
            # # 1x1 expansion:
            conv_branch = self.conv2d(conv_branch, 1, 1, output_depth, name='W_exp', padding='VALID')
            with tf.variable_scope('W_exp'):
                conv_branch = self.batch_norm(conv_branch)
            # NOTE! no PReLU here

            # main branch:
            main_branch = x

            if downsampling:
                # max pooling with argmax
                main_branch, pooling_indices, shape_main_branch = self.pool_index(main_branch, 2, 2)
                
                # pad with zeros so that the feature block depth matches:
                depth_to_pad = output_depth - input_depth
                paddings = tf.convert_to_tensor([[0, 0], [0, 0], [0, 0], [0, depth_to_pad]])
                main_branch = tf.pad(main_branch, paddings=paddings, mode="CONSTANT")

            # add the branches:
            merged = conv_branch + main_branch

            # apply PReLU:
            output = self.PReLU(merged, scope=scope + "/output")
        
            if downsampling:
                return output, pooling_indices
            else:
                return output

    def encoder_bottleneck_dilated(self, x, output_depth, scope, dilation_rate, proj_ratio=4):
        with tf.variable_scope(scope):
            input_shape = x.get_shape().as_list()
            input_depth = input_shape[3]

            internal_depth = int(output_depth/proj_ratio)

            # convolution branch:
            conv_branch = x

            # # 1x1 projection:
            conv_branch = self.conv2d(conv_branch, 1, 1, internal_depth, name='W_proj', padding='VALID')
            with tf.variable_scope('W_proj'):
                conv_branch = self.batch_norm(conv_branch)
                conv_branch = self.PReLU(conv_branch, scope=scope + "/proj")

            # # dilated conv:
            conv_branch = self.atrous(conv_branch, 3, dilation_rate, internal_depth, name='W_conv')
            with tf.variable_scope('W_conv'):
                conv_branch = self.batch_norm(conv_branch)
                conv_branch = self.PReLU(conv_branch, scope=scope + "/conv")

            # # 1x1 expansion:
            conv_branch = self.conv2d(conv_branch, 1, 1, output_depth, name='W_exp', padding='VALID')
            with tf.variable_scope('W_exp'):
                conv_branch = self.batch_norm(conv_branch)
            # NOTE! no PReLU here

            # main branch:
            main_branch = x


            # add the branches:
            merged = conv_branch + main_branch

            # apply PReLU:
            output = self.PReLU(merged, scope=scope + "/output")

            return output

    def encoder_bottleneck_asymmetric(self, x, output_depth, scope, proj_ratio=4):
        with tf.variable_scope(scope):
            input_shape = x.get_shape().as_list()
            input_depth = input_shape[3]

            internal_depth = int(output_depth/proj_ratio)

            # convolution branch:
            conv_branch = x

            # # 1x1 projection:
            conv_branch = self.conv2d(conv_branch, 1, 1, internal_depth, name='W_proj', padding='VALID')
            with tf.variable_scope('W_proj'):
                conv_branch = self.batch_norm(conv_branch)
                conv_branch = self.PReLU(conv_branch, scope=scope + "/proj")

            # # asymmetric conv:
            # # # asymmetric conv 1:
            W_conv1 = tf.get_variable(scope + "/W_conv1", shape=[5, 1, internal_depth, internal_depth], \
                dtype=tf.float32, initializer=tf.contrib.layers.xavier_initializer())
            conv_branch = tf.nn.conv2d(conv_branch, W_conv1, strides=[1, 1, 1, 1], padding="SAME") # NOTE! no bias terms
            # # # asymmetric conv 2:
            W_conv2 = tf.get_variable(scope + "/W_conv2", shape=[1, 5, internal_depth, internal_depth], \
                dtype=tf.float32, initializer=tf.contrib.layers.xavier_initializer())
            b_conv2 = tf.get_variable(scope + "/b_conv2", shape=[internal_depth], \
                dtype=tf.float32, initializer=tf.constant_initializer(0))
            conv_branch = tf.nn.conv2d(conv_branch, W_conv2, strides=[1, 1, 1, 1], padding="SAME") + b_conv2

            # # # batch norm and PReLU:
            with tf.variable_scope('conv'):
                conv_branch = self.batch_norm(conv_branch)
                conv_branch = self.PReLU(conv_branch, scope=scope + "/conv")

            # # 1x1 expansion:
            conv_branch = self.conv2d(conv_branch, 1, 1, output_depth, name='W_exp', padding='VALID')
            with tf.variable_scope('W_exp'):
                conv_branch = self.batch_norm(conv_branch)
            # NOTE! no PReLU here

            # main branch:
            main_branch = x

            # add the branches:
            merged = conv_branch + main_branch

            # apply PReLU:
            output = self.PReLU(merged, scope=scope + "/output")

            return output

    def decoder_bottleneck(self, x, output_depth, batch_size, scope, proj_ratio=4, upsampling=False, pooling_indices=None, output_shape=None):
        # NOTE! decoder uses ReLU instead of PReLU
        with tf.variable_scope(scope):
            input_shape = x.get_shape().as_list()
            input_depth = input_shape[3]

            internal_depth = int(output_depth/proj_ratio)

            # main branch:
            main_branch = x

            if upsampling:
                # # 1x1 projection (to decrease depth to the same value as before downsampling):
                main_branch = self.conv2d(main_branch, 1, 1, output_depth, name='W_upsample', padding='VALID')
                with tf.variable_scope('W_upsample'):
                    main_branch = self.batch_norm(main_branch)
                # NOTE! no ReLU here
                
                # # max unpooling: 
                main_branch = self.up_sampling(main_branch, pooling_indices, output_shape, batch_size, name="unpool")
            
            main_branch = tf.cast(main_branch, tf.float32)

            # convolution branch:
            conv_branch = x

            # # 1x1 projection:
            conv_branch = self.conv2d(conv_branch, 1, 1, internal_depth, name='W_proj', padding='VALID')
            with tf.variable_scope('W_proj'):
                conv_branch = self.batch_norm(conv_branch)
                conv_branch = tf.nn.relu(conv_branch)

            # # conv:
            if upsampling:
                # deconvolution:
                main_branch_shape = main_branch.get_shape().as_list()
                conv_branch = self.tconv2d(conv_branch, 3, internal_depth, 2, main_branch_shape[1], main_branch_shape[2], name='W_conv')
            else:
                conv_branch = self.conv_bias(conv_branch, 3, 1, internal_depth, 'W_conv')

            # # # batch norm and ReLU:
            with tf.variable_scope('W_conv'):
                conv_branch = self.batch_norm(conv_branch)
                conv_branch = self.PReLU(conv_branch, scope=scope + "/conv")

            # # 1x1 expansion:
            conv_branch = self.conv2d(conv_branch, 1, 1, output_depth, name='W_exp', padding='VALID')
            with tf.variable_scope('W_exp'):
                conv_branch = self.batch_norm(conv_branch)
            # NOTE! no ReLU here

            # add the branches:
            merged = conv_branch + main_branch

            # apply ReLU:
            output = tf.nn.relu(merged)

            return output
     
    def forward(self, data, batch_size):   
        input_shape = data.get_shape()
        with tf.variable_scope('conv0'):
            data_after_bn = self.batch_norm(data)

        network = self.initial_block(x=data_after_bn, scope="inital")

        # # layer 1:
        inputs_shape_1 = network.get_shape().as_list()
        network, pooling_indices_1 = self.encoder_bottleneck_regular(x=network, output_depth=64, scope="bottleneck_1_0", downsampling=True)
        network = self.encoder_bottleneck_regular(x=network, output_depth=64, scope="bottleneck_1_1")
        network = self.encoder_bottleneck_regular(x=network, output_depth=64, scope="bottleneck_1_2")
        network = self.encoder_bottleneck_regular(x=network, output_depth=64, scope="bottleneck_1_3")
        network = self.encoder_bottleneck_regular(x=network, output_depth=64, scope="bottleneck_1_4")

        # # layer 2:
        inputs_shape_2 = network.get_shape().as_list()
        network, pooling_indices_2 = self.encoder_bottleneck_regular(x=network, output_depth=128, scope="bottleneck_2_0", downsampling=True)
        network = self.encoder_bottleneck_regular(x=network, output_depth=128, scope="bottleneck_2_1")
        network = self.encoder_bottleneck_dilated(x=network, output_depth=128, scope="bottleneck_2_2", dilation_rate=2)
        network = self.encoder_bottleneck_asymmetric(x=network, output_depth=128, scope="bottleneck_2_3")
        network = self.encoder_bottleneck_dilated(x=network, output_depth=128, scope="bottleneck_2_4", dilation_rate=4)
        network = self.encoder_bottleneck_regular(x=network, output_depth=128, scope="bottleneck_2_5")
        network = self.encoder_bottleneck_dilated(x=network, output_depth=128, scope="bottleneck_2_6", dilation_rate=8)
        network = self.encoder_bottleneck_asymmetric(x=network, output_depth=128, scope="bottleneck_2_7")
        network = self.encoder_bottleneck_dilated(x=network, output_depth=128, scope="bottleneck_2_8", dilation_rate=16)

        # layer 3:
        network = self.encoder_bottleneck_regular(x=network, output_depth=128, scope="bottleneck_3_1")
        network = self.encoder_bottleneck_dilated(x=network, output_depth=128, scope="bottleneck_3_2", dilation_rate=2)
        network = self.encoder_bottleneck_asymmetric(x=network, output_depth=128, scope="bottleneck_3_3")
        network = self.encoder_bottleneck_dilated(x=network, output_depth=128, scope="bottleneck_3_4", dilation_rate=4)
        network = self.encoder_bottleneck_regular(x=network, output_depth=128, scope="bottleneck_3_5")
        network = self.encoder_bottleneck_dilated(x=network, output_depth=128, scope="bottleneck_3_6", dilation_rate=8)
        network = self.encoder_bottleneck_asymmetric(x=network, output_depth=128, scope="bottleneck_3_7")
        network = self.encoder_bottleneck_dilated(x=network, output_depth=128, scope="bottleneck_3_8", dilation_rate=16)

        # decoder:
        # # layer 4:
        network1 = self.decoder_bottleneck(x=network, output_depth=64, batch_size=batch_size, scope="bottleneck_4_0", \
            upsampling=True, pooling_indices=pooling_indices_2, output_shape=inputs_shape_2)
        network2 = self.decoder_bottleneck(x=network1, output_depth=64, batch_size=batch_size, scope="bottleneck_4_1")
        network3 = self.decoder_bottleneck(x=network2, output_depth=64, batch_size=batch_size, scope="bottleneck_4_2")

        # # layer 5:
        network4 = self.decoder_bottleneck(x=network3, output_depth=16, batch_size=batch_size, scope="bottleneck_5_0", \
            upsampling=True, pooling_indices=pooling_indices_1, output_shape=inputs_shape_1)

        network5 = self.decoder_bottleneck(x=network4, output_depth=16, batch_size=batch_size, scope="bottleneck_5_1")
        network = self.tconv2d(network5, 2, self.num_classes, 2, input_shape[1], input_shape[2], name='fullconv')
       
        deconv1_2 =  network

        softmax_ss , aux1_ss, aux2_ss, softmax_bd, softmax_cc, aux1_cc, aux2_cc, \
          aux1_el, aux2_el, softmax_el = [None for i in range(10)]

        if self.T_S: # Semantic Segmentation
            if not self.T_E  and not self.T_C and not self.T_D:
              conv_seg = deconv1_2
            else:
              pre_ss = self.conv_batchN_relu(deconv1_2, 1, 1, self.num_classes, name='pre_ss')
              with tf.variable_scope('sem_segmentation'):
                conv_seg = self.conv2d(pre_ss, 8, 1, self.num_classes)
            softmax_ss = conv_seg

            ## Auxilary SS
            if self.has_aux_loss:
              aux1_ss = tf.nn.softmax(tf.image.resize_images(self.conv_batchN_relu(network2, 1, 1, self.num_classes, name='conv_aux1_ss', relu=False), \
                                        [input_shape[1], input_shape[2]]))
              aux2_ss = tf.nn.softmax(tf.image.resize_images(self.conv_batchN_relu(network4, 1, 1, self.num_classes, name='conv_aux2_ss', relu=False), \
                                        [input_shape[1], input_shape[2]]))

        if self.T_E: # Edge Detection
            pre_bd = self.conv_batchN_relu(deconv1_2, 1, 1, 1, name='pre_bd')
            with tf.variable_scope('contour'):
              conv_contour = self.conv2d(pre_bd, 8, 1, 1)
            softmax_bd = tf.nn.sigmoid(conv_contour)

        if self.T_C: # Semantic Contour Detection
            pre_cc = self.conv_batchN_relu(deconv1_2, 1, 1, self.num_classes, name='pre_cc')
            with tf.variable_scope('sem_contour'):
              conv_sem_contour = self.conv2d(pre_cc, 8, 1, self.num_classes)

            ## Auxilary Semantic Contours
            if self.has_aux_loss:
              aux1_cc = tf.nn.softmax(tf.image.resize_images(self.conv_batchN_relu(network2, 1, 1, self.num_classes, name='conv_aux1_cc', relu=False), \
                                        [input_shape[1], input_shape[2]]))
              aux2_cc = tf.nn.softmax(tf.image.resize_images(self.conv_batchN_relu(network4, 1, 1, self.num_classes, name='conv_aux2_cc', relu=False), \
                                        [input_shape[1], input_shape[2]]))

            softmax_cc = conv_sem_contour #tf.nn.softmax(conv_sem_contour)

        if self.T_D: #Distance Transform
            pre_el = self.conv_batchN_relu(deconv1_2, 1, 1, self.energy_level, name='pre_el')
            with tf.variable_scope('dist_transf'):
                if self.regression:    
                  conv_dist_transf = self.conv2d(pre_el, 8, 1, 1)
                  conv_dist_transf = tf.sigmoid(conv_dist_transf )
                else:
                  conv_dist_transf = self.conv2d(pre_el, 8, 1, self.energy_level)

            ## Auxilary Distance Transform    
            if self.has_aux_loss:
                if self.regression:
                  aux1_el = tf.nn.softmax(tf.image.resize_images(self.conv_batchN_relu(network2, 1, 1, 1, name='conv_aux1_el', relu=False), \
                                            [input_shape[1], input_shape[2]]))
                  aux2_el = tf.nn.softmax(tf.image.resize_images(self.conv_batchN_relu(network4, 1, 1, 1, name='conv_aux2_el', relu=False), \
                                            [input_shape[1], input_shape[2]]))
                else:
                  aux1_el = tf.nn.softmax(tf.image.resize_images(self.conv_batchN_relu(network2, 1, 1,  self.energy_level, name='conv_aux1_el', relu=False), \
                                            [input_shape[1], input_shape[2]]))
                  aux2_el = tf.nn.softmax(tf.image.resize_images(self.conv_batchN_relu(network4, 1, 1,  self.energy_level, name='conv_aux2_el', relu=False), \
                                            [input_shape[1], input_shape[2]]))

            softmax_el = conv_dist_transf
        
        return softmax_ss, aux1_ss, aux2_ss, softmax_bd, softmax_cc,  aux1_cc, aux2_cc, softmax_el, aux1_el, aux2_el
        
    def loss(self, logits_ss, label_ss, aux1_ss=None, aux2_ss=None, softmax_bd=None, \
                    softmax_cc=None, aux1_cc=None, aux2_cc=None,  softmax_el=None, aux1_el=None, aux2_el=None, \
                    label_bd=None, label_cc=None, label_el=None):
        
        loss = tf.Variable(0.0, trainable=False, name='loss')
        loss_ss = tf.Variable(0.0, trainable=False, name='loss_ss')
        loss_ss_iou = tf.Variable(0.0, trainable=False, name='loss_ss_iou')
        loss_bd = tf.Variable(0.0, trainable=False, name='loss_bd')
        loss_bd_iou = tf.Variable(0.0, trainable=False, name='loss_bd_iou')
        loss_cc = tf.Variable(0.0, trainable=False, name='loss_cc')
        loss_cc_iou = tf.Variable(0.0, trainable=False, name='loss_cc_iou')
        loss_el = tf.Variable(0.0, trainable=False, name='loss_el')
        loss_el_iou = tf.Variable(0.0, trainable=False, name='loss_el_iou')

        if self.T_S:
          loss_ss = self.loss_func.loss_cross(softmax=tf.nn.softmax(logits_ss), label=label_ss, weights=self.weights)
          loss_ss_iou = self.loss_func.IoU_loss_multilabel(logits=tf.nn.sigmoid(logits_ss), labels=label_ss, \
                          num_classes=self.num_classes)
          loss += 1.1*(loss_ss + loss_ss_iou)/2.0
          if self.has_aux_loss:
              aux_loss1_ss = tf.reduce_mean(-tf.reduce_sum(tf.multiply(label_ss*tf.log(aux1_ss+1e-10), self.weights), axis=[3]))
              aux_loss2_ss = tf.reduce_mean(-tf.reduce_sum(tf.multiply(label_ss*tf.log(aux2_ss+1e-10), self.weights), axis=[3]))
              loss = loss + (0.6*aux_loss1_ss + 0.5*aux_loss2_ss)/2.0

        if self.T_E: 
          loss_bd = self.loss_func.HED_loss(logits=softmax_bd, labels=label_bd)
          loss_bd_iou = self.loss_func.IoU_loss(logits=softmax_bd, labels=label_bd, multilabel=False)
          loss += (loss_bd + loss_bd_iou)/2.0

        if self.T_C:
          loss_cc = self.loss_func.loss_cross(softmax=tf.nn.softmax(softmax_cc), label=label_cc, weights=self.weights)
          loss_cc_iou = self.loss_func.IoU_loss_multilabel(logits=tf.nn.sigmoid(softmax_cc), labels=label_cc, \
                          num_classes=self.num_classes)
          loss += (loss_cc + loss_cc_iou)/2.0
          if self.has_aux_loss:
              aux_loss1_cc = tf.reduce_mean(-tf.reduce_sum(tf.multiply(label_cc*tf.log(aux1_cc+1e-10), self.weights), axis=[3]))
              aux_loss2_cc = tf.reduce_mean(-tf.reduce_sum(tf.multiply(label_cc*tf.log(aux2_cc+1e-10), self.weights), axis=[3]))
              loss = loss + (0.6*aux_loss1_cc + 0.5*aux_loss2_cc)/2.0
        
        if self.T_D:
          if self.regression:
              loss_el = tf.losses.mean_squared_error(labels=label_el, predictions=softmax_el)
              loss += loss_el
          else:
              loss_el = self.loss_func.loss_cross(softmax=tf.nn.softmax(softmax_el), label=label_el, weights=self.weights_el)
              loss_el_iou = self.loss_func.IoU_loss_multilabel(logits=tf.nn.sigmoid(softmax_el), labels=label_el, \
                              num_classes=self.energy_level)
              loss += (loss_el + loss_el_iou)/2.0

          if self.has_aux_loss:
              aux_loss1_el = tf.reduce_mean(-tf.reduce_sum(tf.multiply(label_el*tf.log(aux1_el+1e-10), self.weights_el), axis=[3]))
              aux_loss2_el = tf.reduce_mean(-tf.reduce_sum(tf.multiply(label_el*tf.log(aux2_el+1e-10), self.weights_el), axis=[3]))
              loss = loss + (0.6*aux_loss1_el + 0.5*aux_loss2_el)/2.0

        tf.add_to_collection('losses', loss)
        
        return loss, loss_ss, loss_ss_iou, loss_bd, loss_bd_iou, loss_cc, loss_cc_iou, loss_el, loss_el_iou


    #def create_optimizer(self):
    #    self.lr = tf.train.polynomial_decay(self.learning_rate, self.global_step, self.decay_steps, power=self.power)
    #    self.train_op = tf.train.AdamOptimizer(self.lr).minimize(self.loss, global_step=self.global_step)
    #    #self.train_op = tf.train.AdamOptimizer(self.learning_rate).minimize(self.loss, global_step=self.global_step)

    def _create_summaries(self):
        with tf.name_scope("summaries"):
            tf.summary.scalar("loss", self.loss)
            tf.summary.histogram("histogram_loss", self.loss)
            self.summary_op = tf.summary.merge_all()

    def build_graph(self, data, batch_size, label_ss=None, label_bd=None, label_cc=None, label_el=None):
        self.forward(data, batch_size)

def main():
    print('Do Nothing')
   
if __name__ == '__main__':
    main()

