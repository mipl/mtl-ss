import tensorflow as tf
from models import network_base
from loss.losses import LossFunc

class AdapNet_pp_MTL(network_base.Network):
    def __init__(self, num_classes=12, energy_level=6, learning_rate=0.001, float_type=tf.float32, weight_decay=0.0005,
                 decay_steps=30000, power=0.9, training=True, ignore_label=True, global_step=0,
                 has_aux_loss=True, S=True, E=False, C=False, D=False, regression=False):
        
        super(AdapNet_pp_MTL, self).__init__()
        self.num_classes = num_classes
        self.learning_rate = learning_rate
        self.weight_decay = weight_decay
        self.initializer = 'he'
        self.has_aux_loss = has_aux_loss
        self.float_type = float_type
        self.power = power
        self.decay_steps = decay_steps
        self.training = training
        self.bn_decay_ = 0.99
        self.eAspp_rate = [3, 6, 12]
        self.residual_units = [3, 4, 6, 3]
        self.filters = [256, 512, 1024, 2048]
        self.strides = [1, 2, 2, 1]
        self.global_step = global_step
        self.energy_level = energy_level
        self.regression = regression

        self.T_S = S
        self.T_E = E
        self.T_C = C
        self.T_D = D
      
        self.loss_func = LossFunc()

        if self.training:
            self.keep_prob = 0.3
        else:
            self.keep_prob = 1.0
        if ignore_label:
            self.weights = tf.ones(self.num_classes-1)
            self.weights = tf.concat((tf.zeros(1), self.weights), 0)
            self.weights_el = tf.ones(self.energy_level-1)
            self.weights_el = tf.concat((tf.zeros(1), self.weights_el), 0)
        else:
            self.weights = tf.ones(self.num_classes)
            self.weights_el = tf.ones(self.energy_level)
     
    def forward(self, data, batch_size):   
        input_shape = data.get_shape()
        with tf.variable_scope('conv0'):
            data_after_bn = self.batch_norm(data)
        conv_7x7_out = self.conv_batchN_relu(data_after_bn, 7, 2, 64, name='conv1')
        max_pool_out = self.pool(conv_7x7_out, 3, 2)

        ##block1
        m_b1_out = self.unit_0(max_pool_out, self.filters[0], 1, 1)
        for unit_index in range(1, self.residual_units[0]):
            m_b1_out = self.unit_1(m_b1_out, self.filters[0], 1, 1, unit_index+1)
        with tf.variable_scope('block1/unit_%d/bottleneck_v1/conv3'%self.residual_units[0]):
            b1_out = tf.nn.relu(self.batch_norm(m_b1_out))
        
        ##block2
        m_b2_out = self.unit_1(b1_out, self.filters[1], self.strides[1], 2, 1, shortcut=True)
        for unit_index in range(1, self.residual_units[1]-1):
            m_b2_out = self.unit_1(m_b2_out, self.filters[1], 1, 2, unit_index+1)
        m_b2_out = self.unit_3(m_b2_out, self.filters[1], 2, self.residual_units[1])
        with tf.variable_scope('block2/unit_%d/bottleneck_v1/conv3'%self.residual_units[1]):
            b2_out = tf.nn.relu(self.batch_norm(m_b2_out))

        ##block3
        m_b3_out = self.unit_1(b2_out, self.filters[2], self.strides[2], 3, 1, shortcut=True)
        m_b3_out = self.unit_1(m_b3_out, self.filters[2], 1, 3, 2)
        for unit_index in range(2, self.residual_units[2]):
            m_b3_out = self.unit_3(m_b3_out, self.filters[2], 3, unit_index+1)

        ##block4
        m_b4_out = self.unit_4(m_b3_out, self.filters[3], 4, 1, shortcut=True)
        for unit_index in range(1, self.residual_units[3]):
            dropout = False
            if unit_index == 2:
                dropout = True
            m_b4_out = self.unit_4(m_b4_out, self.filters[3], 4, unit_index+1, dropout=dropout)
        with tf.variable_scope('block4/unit_%d/bottleneck_v1/conv3'%self.residual_units[3]):
            b4_out = tf.nn.relu(self.batch_norm(m_b4_out))

        ##skip
        skip1 = self.conv_batchN_relu(b1_out, 1, 1, 24, name='conv32', relu=False)
        skip2 = self.conv_batchN_relu(b2_out, 1, 1, 24, name='conv174', relu=False)

        ##eAspp
        IA = self.conv_batchN_relu(b4_out, 1, 1, 256, name='conv256')

        IB = self.conv_batchN_relu(b4_out, 1, 1, 64, name='conv70')
        IB = self.aconv_batchN_relu(IB, 3, self.eAspp_rate[0], 64, name='conv7')
        IB = self.aconv_batchN_relu(IB, 3, self.eAspp_rate[0], 64, name='conv247')
        IB = self.conv_batchN_relu(IB, 1, 1, 256, name='conv71')

        IC = self.conv_batchN_relu(b4_out, 1, 1, 64, name='conv80')
        IC = self.aconv_batchN_relu(IC, 3, self.eAspp_rate[1], 64, name='conv8')
        IC = self.aconv_batchN_relu(IC, 3, self.eAspp_rate[1], 64, name='conv248')
        IC = self.conv_batchN_relu(IC, 1, 1, 256, name='conv81')

        ID = self.conv_batchN_relu(b4_out, 1, 1, 64, name='conv90')
        ID = self.aconv_batchN_relu(ID, 3, self.eAspp_rate[2], 64, name='conv9')
        ID = self.aconv_batchN_relu(ID, 3, self.eAspp_rate[2], 64, name='conv249')
        ID = self.conv_batchN_relu(ID, 1, 1, 256, name='conv91')

        IE = tf.expand_dims(tf.expand_dims(tf.reduce_mean(b4_out, [1, 2]), 1), 2)
        IE = self.conv_batchN_relu(IE, 1, 1, 256, name='conv57')
        IE_shape = b4_out.get_shape()
        IE = tf.image.resize_images(IE, [IE_shape[1], IE_shape[2]])

        eAspp_out = self.conv_batchN_relu(tf.concat((IA, IB, IC, ID, IE), 3), 1, 1, 256, name='conv10', relu=False)
        
        ### Upsample/Decoder
        with tf.variable_scope('conv41'):
            deconv_up1 = self.tconv2d(eAspp_out, 4, 256, 2, skip2.get_shape()[1], skip2.get_shape()[2])
            deconv_up1 = self.batch_norm(deconv_up1)

        up1 = self.conv_batchN_relu(tf.concat((deconv_up1, skip2), 3), 3, 1, 256, name='conv89') 
        up1 = self.conv_batchN_relu(up1, 3, 1, 256, name='conv96')
        with tf.variable_scope('conv16'):
            deconv_up2 = self.tconv2d(up1, 4, 256, 2, skip1.get_shape()[1], skip1.get_shape()[2])
            deconv_up2 = self.batch_norm(deconv_up2)
        up2 = self.conv_batchN_relu(tf.concat((deconv_up2, skip1), 3), 3, 1, 256, name='conv88') 
        up2 = self.conv_batchN_relu(up2, 3, 1, 256, name='conv95')

        deconv1_2 = up2

        softmax_ss , aux1_ss, aux2_ss, softmax_bd, softmax_cc, aux1_cc, aux2_cc, \
          aux1_el, aux2_el, softmax_el = [None for i in range(10)]

        if self.T_S: # Semantic Segmentation
            pre_ss = self.conv_batchN_relu(deconv1_2, 1, 1, self.num_classes, name='pre_ss')
            with tf.variable_scope('sem_segmentation'):
              conv_seg = self.tconv2d(pre_ss, 8, self.num_classes, 4, input_shape[1], input_shape[2])
            softmax_ss = conv_seg

            ## Auxilary SS
            if self.has_aux_loss:
              aux1_ss = tf.nn.softmax(tf.image.resize_images(self.conv_batchN_relu(deconv_up1, 1, 1, self.num_classes, name='conv_aux1_ss', relu=False), \
                                        [input_shape[1], input_shape[2]]))
              aux2_ss = tf.nn.softmax(tf.image.resize_images(self.conv_batchN_relu(deconv_up2, 1, 1, self.num_classes, name='conv_aux2_ss', relu=False), \
                                        [input_shape[1], input_shape[2]]))

        if self.T_E: # Edge Detection
            pre_bd = self.conv_batchN_relu(deconv1_2, 1, 1, 1, name='pre_bd')
            with tf.variable_scope('contour'):
              conv_contour = self.tconv2d(pre_bd, 8, 1, 4, input_shape[1], input_shape[2])
            softmax_bd = tf.nn.sigmoid(conv_contour)

        if self.T_C: # Semantic Contour Detection
            pre_cc = self.conv_batchN_relu(deconv1_2, 1, 1, self.num_classes, name='pre_cc')
            with tf.variable_scope('sem_contour'):
              conv_sem_contour = self.tconv2d(pre_cc, 8, self.num_classes, 4, input_shape[1], input_shape[2])

            ## Auxilary Semantic Contours
            if self.has_aux_loss:
              aux1_cc = tf.nn.softmax(tf.image.resize_images(self.conv_batchN_relu(deconv_up1, 1, 1, self.num_classes, name='conv_aux1_cc', relu=False), \
                                        [input_shape[1], input_shape[2]]))
              aux2_cc = tf.nn.softmax(tf.image.resize_images(self.conv_batchN_relu(deconv_up2, 1, 1, self.num_classes, name='conv_aux2_cc', relu=False), \
                                        [input_shape[1], input_shape[2]]))

            softmax_cc = conv_sem_contour

        if self.T_D: #Distance Transform
            pre_el = self.conv_batchN_relu(deconv1_2, 1, 1, self.energy_level, name='pre_el')
            with tf.variable_scope('dist_transf'):
                if self.regression:    
                  conv_dist_transf = self.tconv2d(pre_el, 8, 1, 4, input_shape[1], input_shape[2])
                  conv_dist_transf = tf.sigmoid(conv_dist_transf )
                else:
                  conv_dist_transf = self.tconv2d(pre_el, 8, self.energy_level, 4, input_shape[1], input_shape[2])

            ## Auxilary Distance Transform    
            if self.has_aux_loss:
                if self.regression:
                  aux1_el = tf.nn.softmax(tf.image.resize_images(self.conv_batchN_relu(deconv_up1, 1, 1, 1, name='conv_aux1_el', relu=False), \
                                            [input_shape[1], input_shape[2]]))
                  aux2_el = tf.nn.softmax(tf.image.resize_images(self.conv_batchN_relu(deconv_up2, 1, 1, 1, name='conv_aux2_el', relu=False), \
                                            [input_shape[1], input_shape[2]]))
                else:
                  aux1_el = tf.nn.softmax(tf.image.resize_images(self.conv_batchN_relu(deconv_up1, 1, 1,  self.energy_level, name='conv_aux1_el', relu=False), \
                                            [input_shape[1], input_shape[2]]))
                  aux2_el = tf.nn.softmax(tf.image.resize_images(self.conv_batchN_relu(deconv_up2, 1, 1,  self.energy_level, name='conv_aux2_el', relu=False), \
                                            [input_shape[1], input_shape[2]]))

            softmax_el = conv_dist_transf
        
        return softmax_ss, aux1_ss, aux2_ss, softmax_bd, softmax_cc,  aux1_cc, aux2_cc, softmax_el, aux1_el, aux2_el
        
    def loss(self, logits_ss, label_ss, aux1_ss=None, aux2_ss=None, softmax_bd=None, \
                    softmax_cc=None, aux1_cc=None, aux2_cc=None,  softmax_el=None, aux1_el=None, aux2_el=None, \
                    label_bd=None, label_cc=None, label_el=None, h_ss=1.1, h_bd=0.9, h_cc=0.9, h_el=0.8):
        
        loss = tf.Variable(0.0, trainable=False, name='loss')
        loss_ss = tf.Variable(0.0, trainable=False, name='loss_ss')
        loss_ss_iou = tf.Variable(0.0, trainable=False, name='loss_ss_iou')
        loss_bd = tf.Variable(0.0, trainable=False, name='loss_bd')
        loss_bd_iou = tf.Variable(0.0, trainable=False, name='loss_bd_iou')
        loss_cc = tf.Variable(0.0, trainable=False, name='loss_cc')
        loss_cc_iou = tf.Variable(0.0, trainable=False, name='loss_cc_iou')
        loss_el = tf.Variable(0.0, trainable=False, name='loss_el')
        loss_el_iou = tf.Variable(0.0, trainable=False, name='loss_el_iou')

        if self.T_S:
          loss_ss = self.loss_func.loss_cross(softmax=tf.nn.softmax(logits_ss), label=label_ss, weights=self.weights)
          loss_ss_iou = self.loss_func.IoU_loss_multilabel(logits=tf.nn.sigmoid(logits_ss), labels=label_ss, \
                          num_classes=self.num_classes)
          loss += h_ss*(loss_ss + loss_ss_iou)/2.0
          if self.has_aux_loss:
              aux_loss1_ss = tf.reduce_mean(-tf.reduce_sum(tf.multiply(label_ss*tf.log(aux1_ss+1e-10), self.weights), axis=[3]))
              aux_loss2_ss = tf.reduce_mean(-tf.reduce_sum(tf.multiply(label_ss*tf.log(aux2_ss+1e-10), self.weights), axis=[3]))
              loss = loss + h_ss*(0.6*aux_loss1_ss + 0.5*aux_loss2_ss)/2.0

        if self.T_E: 
          loss_bd = self.loss_func.HED_loss(logits=softmax_bd, labels=label_bd)
          loss_bd_iou = self.loss_func.IoU_loss(logits=softmax_bd, labels=label_bd, multilabel=False)
          loss += h_bd*(loss_bd + loss_bd_iou)/2.0
          
          if self.has_aux_loss:
              aux1_bd =  tf.nn.sigmoid( tf.expand_dims(tf.reduce_sum(aux1_cc, axis=3), axis=-1) )
              aux2_bd =  tf.nn.sigmoid( tf.expand_dims(tf.reduce_sum(aux2_cc, axis=3), axis=-1) )
              aux_loss1_bd = tf.reduce_mean(-tf.reduce_sum(tf.multiply(label_bd*tf.log(aux1_bd+1e-10), self.weights), axis=[3]))
              aux_loss2_bd = tf.reduce_mean(-tf.reduce_sum(tf.multiply(label_bd*tf.log(aux2_bd+1e-10), self.weights), axis=[3]))
              loss = loss + h_bd*(0.6*aux_loss1_bd + 0.5*aux_loss2_bd)/2.0
          

        if self.T_C:
          loss_cc = self.loss_func.loss_cross(softmax=tf.nn.softmax(softmax_cc), label=label_cc, weights=self.weights)
          loss_cc_iou = self.loss_func.IoU_loss_multilabel(logits=tf.nn.sigmoid(softmax_cc), labels=label_cc, \
                          num_classes=self.num_classes)
          loss += h_cc*(loss_cc + loss_cc_iou)/2.0
          if self.has_aux_loss:
              aux_loss1_cc = tf.reduce_mean(-tf.reduce_sum(tf.multiply(label_cc*tf.log(aux1_cc+1e-10), self.weights), axis=[3]))
              aux_loss2_cc = tf.reduce_mean(-tf.reduce_sum(tf.multiply(label_cc*tf.log(aux2_cc+1e-10), self.weights), axis=[3]))
              loss = loss + h_cc*(0.6*aux_loss1_cc + 0.5*aux_loss2_cc)/2.0
        
        if self.T_D:
          if self.regression:
              loss_el = tf.losses.mean_squared_error(labels=label_el, predictions=softmax_el)
              loss += h_el*loss_el
          else:
              loss_el = self.loss_func.loss_cross(softmax=tf.nn.softmax(softmax_el), label=label_el, weights=self.weights_el)
              loss_el_iou = self.loss_func.IoU_loss_multilabel(logits=tf.nn.sigmoid(softmax_el), labels=label_el, \
                              num_classes=self.energy_level)
              loss += h_el*(loss_el + loss_el_iou)/2.0

          if self.has_aux_loss:
              aux_loss1_el = tf.reduce_mean(-tf.reduce_sum(tf.multiply(label_el*tf.log(aux1_el+1e-10), self.weights_el), axis=[3]))
              aux_loss2_el = tf.reduce_mean(-tf.reduce_sum(tf.multiply(label_el*tf.log(aux2_el+1e-10), self.weights_el), axis=[3]))
              loss = loss + h_el*(0.6*aux_loss1_el + 0.5*aux_loss2_el)/2.0

        tf.add_to_collection('losses', loss)
        
        return loss, loss_ss, loss_ss_iou, loss_bd, loss_bd_iou, loss_cc, loss_cc_iou, loss_el, loss_el_iou


    #def create_optimizer(self):
    #    self.lr = tf.train.polynomial_decay(self.learning_rate, self.global_step, self.decay_steps, power=self.power)
    #    self.train_op = tf.train.AdamOptimizer(self.lr).minimize(self.loss, global_step=self.global_step)
    #    #self.train_op = tf.train.AdamOptimizer(self.learning_rate).minimize(self.loss, global_step=self.global_step)

    def _create_summaries(self):
        with tf.name_scope("summaries"):
            tf.summary.scalar("loss", self.loss)
            tf.summary.histogram("histogram_loss", self.loss)
            self.summary_op = tf.summary.merge_all()

def main():
    print('Do Nothing')
   
if __name__ == '__main__':
    main()

