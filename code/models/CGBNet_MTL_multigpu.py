import tensorflow as tf
from models import network_base
from loss.losses import LossFunc

def low_pass_gaussian(size, mean, std): #size: int, mean: float, std: float
    """Makes 2D gaussian Kernel for convolution.
    https://stackoverflow.com/questions/52012657/how-to-make-a-2d-gaussian-filter-in-tensorflow
    """
    # Make Gaussian Kernel with desired specs
    d = tf.distributions.Normal(mean, std)
    vals = d.prob(tf.range(start = -size, limit = size + 1, dtype = tf.float32))
    gauss_kernel = tf.einsum('i,j->ij', vals, vals)
    gauss_kernel / tf.reduce_sum(gauss_kernel)

    # Expand dimensions of 'gauss_kernel' for 'tf.nn.conv2d' signature.
    gauss_kernel = gauss_kernel[:, :, tf.newaxis, tf.newaxis]
    low_pass_gauss_kernel = tf.Variable(gauss_kernel, trainable=False, name='low_pass_gauss')

    return low_pass_gauss_kernel


class CGBNet_MTL(network_base.Network):
    def __init__(self, num_classes=12, k=12, energy_level=6, learning_rate=0.001, float_type=tf.float32, weight_decay=0.0005,
                 decay_steps=30000, power=0.9, training=True, ignore_label=True, global_step=0,
                 has_aux_loss=True, S=True, E=False, C=False, D=False, regression=False):
      
      super(CGBNet_MTL, self).__init__()
      self.num_classes = num_classes
      self.learning_rate = learning_rate
      self.weight_decay = weight_decay
      self.initializer = 'he'
      self.has_aux_loss = has_aux_loss
      self.float_type = float_type
      self.power = power
      self.decay_steps = decay_steps
      self.training = training
      self.bn_decay_ = 0.99
      self.residual_units = [3, 4, 23, 3]
      self.global_step = global_step
      self.energy_level = energy_level
      self.regression = regression

      self.T_S = S
      self.T_E = E
      self.T_C = C
      self.T_D = D
      
      self.loss_func = LossFunc()

      if self.training:
        self.keep_prob = 0.3
      else:
          self.keep_prob = 1.0
      if ignore_label:
        self.weights = tf.ones(self.num_classes-1)
        self.weights = tf.concat((tf.zeros(1), self.weights), 0)
        self.weights_el = tf.ones(self.energy_level-1)
        self.weights_el = tf.concat((tf.zeros(1), self.weights_el), 0)
      else:
        self.weights = tf.ones(self.num_classes)
        self.weights_el = tf.ones(self.energy_level)
    
    def rnn_cell(self, rnn_input, state, num_units, name='rnn_cell'):
      num_input = rnn_input.get_shape()[-1]
      with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
        W = tf.get_variable('Wab', [num_input + num_units, num_units])
      return tf.tanh(tf.matmul(tf.concat([rnn_input, state], 1), W))

    def CCL(self, x, num_layer):
      list_CLL = []
      for i in range(num_layer):
        with tf.variable_scope('CCL_'+str(i+1)):
          local = self.conv2d(x, 3, 1, 512, 'CCL_local')
          context = self.atrous(x, 3, 5, 512, 'CCL_context')
          ccl = local-context
          list_CLL.append(ccl)
      return list_CLL
    
    def GatedSum(self, x, num_classes, name, size_out=1):
      with tf.variable_scope(name):
        def RNN(x, out_dim):
          x_sz = x.get_shape().as_list()
          cell_rnn = tf.contrib.cudnn_rnn.CudnnRNNTanh(num_layers=x_sz[1], num_units=out_dim, name='RNN')
          cell_rnn.build(x_sz)
          out_rnn, output_states = cell_rnn(x, training=self.training)
          return out_rnn
          
        info_S = []
        clf_S = []
        num_layer = len(x)
        x_sz = x[0].get_shape().as_list()
        for i in range(num_layer):
          with tf.variable_scope('GatedSum_'+str(i+1)):
            sz_t = x[i].get_shape().as_list()
            conv_info_S = self.conv2d(x[i], 3, 1, num_classes, 'info_S')
            conv_clf_S = self.conv2d(x[i], 3, 1, num_classes, 'clf_S')
            info_S.append(tf.reshape(conv_info_S, [-1, sz_t[1]*sz_t[2]*self.num_classes]))
            clf_S.append(conv_clf_S)
        in_rnn = info_S

        # ----------- RNN ---------------
        rnn_outputs = []
        num_units = x_sz[1]*x_sz[2]
        state = tf.zeros([tf.shape(x[0])[0], num_units])
        for rnn_input in in_rnn:
          state = self.rnn_cell(rnn_input, state, num_units=num_units)
          rnn_outputs.append(state)
        out_rnn = tf.stack(rnn_outputs, axis=-1)

        # global refine
        Hp = tf.reshape(out_rnn, [-1, x_sz[1], x_sz[2], num_layer])
        with tf.variable_scope('global_refine'):
          Hp_conv = self.conv2d(Hp, 3, 1, num_layer, 'glrf')
        Hp_hat = Hp_conv + Hp

        Gp = []
        for j in range(num_layer):
          Gp.append(tf.nn.softmax(Hp_hat[:,:,:,j]))
        
        S_hat = []
        for c in range(self.num_classes):
          S_c_n = 0.0
          for l in range(num_layer):
            S_c_n += Gp[l] * clf_S[l][:,:,:,c]
          S_hat.append(S_c_n)
        S_hat = tf.stack(S_hat, axis=-1)
        if size_out == 1:
          return S_hat
        else:
          return S_hat, clf_S
    
    def BRD(self, x, skip_l, skip_f, m, out_dim):
      with tf.variable_scope('BRD'):
        bound = self.conv2d(x, 1, 1, 1, '')
        S_hat, S_c = self.GatedSum([x, skip_l, skip_f], out_dim, 'Gs1', 2)
        list_B_S = tf.zeros_like(S_c[0])
        for i in range(len(m)):
          gauss_kernel_m = low_pass_gaussian(8, 1.0, m[i])
          B_m = tf.nn.conv2d(bound, gauss_kernel_m, strides=[1, 1, 1, 1], padding="SAME")
          for c in range(len(S_c)):
            list_B_S += S_c[c]*B_m
        out = list_B_S + S_hat
        return out
     
    def forward(self, data, batch_size):   
        input_shape = data.get_shape()
        # ----------------------------- ResNet 101 ------------------------------
        # Encoder
        with tf.variable_scope('conv0'):
          data_after_bn = self.batch_norm(data)
        conv_7x7_out = self.conv_batchN_relu(data_after_bn, 7, 2, 64, name='conv1')
        max_pool_out = self.pool(conv_7x7_out, 2, 2)

        ## enc_block1
        m_b1_out = self.residual_block(max_pool_out, [64,64,256], 1,  True, 1, 1)
        shape_b1 = m_b1_out.get_shape()
        enc_b1_f = m_b1_out
        for unit_index in range(1, self.residual_units[0]):
          m_b1_out = self.residual_block(m_b1_out, [64,64,256], 1, False, 1, unit_index+1)
        enc_b1_l = m_b1_out
        with tf.variable_scope('block1/unit_%d/bottleneck_v2/conv3'%(self.residual_units[0]+1)):
          b1_out = tf.nn.relu(self.batch_norm(m_b1_out))
        
        ## enc_block2
        m_b2_out = self.residual_block(b1_out, [128, 128, 512], 2, True, 2, 1)
        shape_b2 = m_b2_out.get_shape()
        enc_b2_f = m_b2_out
        for unit_index in range(1, self.residual_units[1]):
          m_b2_out = self.residual_block(m_b2_out, [128, 128, 512], 1, False, 2, unit_index+1)
        enc_b2_l = m_b2_out
        with tf.variable_scope('block2/unit_%d/bottleneck_v2/conv3'%(self.residual_units[1]+1)):
          b2_out = tf.nn.relu(self.batch_norm(m_b2_out))

        ## enc_block3
        m_b3_out = self.residual_block(b2_out, [256, 256, 1024], 2, True, 3, 1, 2)
        shape_b3 = m_b3_out.get_shape()
        enc_b3_f = m_b3_out
        for unit_index in range(1, self.residual_units[2]):
          m_b3_out = self.residual_block(m_b3_out, [256, 256, 1024], 1, False, 3, unit_index+1, 2)
        enc_b3_l = m_b3_out
        with tf.variable_scope('block3/unit_%d/bottleneck_v2/conv3'%(self.residual_units[2]+1)):
          b3_out = tf.nn.relu(self.batch_norm(m_b3_out))

        ## enc_block4
        m_b4_out = self.residual_block(b3_out, [512, 512, 2048], 2, True, 4, 1, 4)
        shape_b4 = m_b4_out.get_shape()
        enc_b4_f = m_b4_out
        for unit_index in range(1, self.residual_units[3]):
          m_b4_out = self.residual_block(m_b4_out, [512, 512, 2048], 1, False, 4, unit_index+1, 4)
        enc_b4_l = m_b4_out
        with tf.variable_scope('block4/unit_%d/bottleneck_v2/conv3'%(self.residual_units[3]+1)):
          b4_out = tf.nn.relu(self.batch_norm(m_b4_out))

        conv1_1_CCL = self.conv_batchN_relu(b4_out, 3, 1, 512, 'conv1_1_CCL')
        ccl = self.CCL(x=conv1_1_CCL, num_layer=6)
        ccl.append(enc_b4_f)
        ccl.append(enc_b4_l)
        Gs4 = self.GatedSum(ccl, self.num_classes, 'Gs4')

        ## dec_block4
        depth = [self.num_classes]*3
        d_m_b4_out = self.residual_block(Gs4, depth, 1, True, 4, 1, 4, name='dec_block')
        for unit_index in range(1, self.residual_units[3]):
          d_m_b4_out = self.residual_block(d_m_b4_out, depth, 1, False, 4, unit_index+1, 4, name='dec_block')
        with tf.variable_scope('dec_block4/unit_%d/bottleneck_v2/conv3'%(self.residual_units[3]+1)):
          d_m_b4_out = tf.nn.relu(self.batch_norm(d_m_b4_out))

        d_m_b4_out_up = tf.image.resize_nearest_neighbor(d_m_b4_out, (shape_b3[1], shape_b3[2]))
        Gs3 = self.GatedSum([d_m_b4_out_up, enc_b3_f, enc_b3_l], self.num_classes, 'Gs3')

        ## dec_block3
        depth = [self.num_classes]*3
        d_m_b3_out = self.residual_block(Gs3, depth, 1, True, 3, 1, 2, name='dec_block')
        for unit_index in range(1, self.residual_units[2]):
          d_m_b3_out = self.residual_block(d_m_b3_out, depth, 1, False, 3, unit_index+1, 2, name='dec_block')
        with tf.variable_scope('dec_block3/unit_%d/bottleneck_v2/conv3'%(self.residual_units[2]+1)):
          d_m_b3_out = tf.nn.relu(self.batch_norm(d_m_b3_out))

        d_m_b3_out = tf.image.resize_nearest_neighbor(d_m_b3_out, (shape_b2[1], shape_b2[2]))
        Gs2 = self.GatedSum([d_m_b3_out, enc_b2_f, enc_b2_l], self.num_classes, 'Gs2')

        ## dec_block2
        depth = [self.num_classes]*3
        d_m_b2_out = self.residual_block(Gs2, depth, 1, True, 2, 1, name='dec_block')
        for unit_index in range(1, self.residual_units[1]):
          d_m_b2_out = self.residual_block(d_m_b2_out, depth, 1, False, 2, unit_index+1, name='dec_block')
        with tf.variable_scope('dec_block2/unit_%d/bottleneck_v2/conv3'%(self.residual_units[1]+1)):
          d_m_b2_out = tf.nn.relu(self.batch_norm(d_m_b2_out))
        
        d_m_b2_out = tf.image.resize_nearest_neighbor(d_m_b2_out, (shape_b1[1], shape_b1[2]))

        ## dec_block1
        depth = [self.num_classes]*3
        d_m_b1_out = self.residual_block(d_m_b2_out, depth, 1, True, 1, 1, name='dec_block')
        for unit_index in range(1, self.residual_units[0]):
          d_m_b1_out = self.residual_block(d_m_b1_out, depth, 1, False, 1, unit_index+1, name='dec_block')
        with tf.variable_scope('dec_block1/unit_%d/bottleneck_v2/conv3'%(self.residual_units[0]+1)):
          d_m_b1_out = tf.nn.relu(self.batch_norm(d_m_b1_out))

        brd = self.BRD(x=d_m_b1_out, skip_l=enc_b1_l, skip_f=enc_b1_f, m=[1.0,3.0,5.0], out_dim= self.num_classes)
        output = brd

        softmax_ss , aux1_ss, aux2_ss, softmax_bd, softmax_cc, aux1_cc, aux2_cc, \
          aux1_el, aux2_el, softmax_el = [None for i in range(10)]

        if self.T_S: # Semantic Segmentation
            if not self.T_E  and not self.T_C and not self.T_D:
              with tf.variable_scope('sem_segmentation'):
                conv_seg = output
            else:
              pre_ss = self.conv_batchN_relu(output, 1, 1, self.num_classes, name='pre_ss')
              with tf.variable_scope('sem_segmentation'):
                conv_seg = self.conv2d(pre_ss, 8, 1, self.num_classes)
            softmax_ss = tf.image.resize_images(conv_seg, (input_shape[1], input_shape[2]))
            softmax_ss = tf.nn.softmax(softmax_ss)

            ## Auxilary SS
            if self.has_aux_loss:
              aux1_ss = tf.nn.softmax(tf.image.resize_images(self.conv_batchN_relu(b3_out, 1, 1, self.num_classes, name='conv_aux1_ss', relu=False), \
                                        [input_shape[1], input_shape[2]]))
              aux2_ss = tf.nn.softmax(tf.image.resize_images(self.conv_batchN_relu(b2_out, 1, 1, self.num_classes, name='conv_aux2_ss', relu=False), \
                                        [input_shape[1], input_shape[2]]))

        if self.T_E: # Edge Detection
            pre_bd = self.conv_batchN_relu(output, 1, 1, 1, name='pre_bd')
            with tf.variable_scope('contour'):
              conv_contour = self.conv2d(pre_bd, 3, 1, 1)
            softmax_bd = tf.nn.sigmoid(tf.image.resize_images(conv_contour, (input_shape[1], input_shape[2])))

        if self.T_C: # Semantic Contours Detection
            pre_cc = self.conv_batchN_relu(output, 1, 1, self.num_classes, name='pre_cc')
            with tf.variable_scope('sem_contour'):
              conv_sem_contour = self.conv2d(pre_cc, 3, 1, self.num_classes)

            ## Auxilary Semantic Contours
            if self.has_aux_loss:
              aux1_cc = tf.nn.softmax(tf.image.resize_images(self.conv_batchN_relu(b3_out, 1, 1, self.num_classes, name='conv_aux1_cc', relu=False), \
                                        [input_shape[1], input_shape[2]]))
              aux2_cc = tf.nn.softmax(tf.image.resize_images(self.conv_batchN_relu(b2_out, 1, 1, self.num_classes, name='conv_aux2_cc', relu=False), \
                                        [input_shape[1], input_shape[2]]))

            softmax_cc = tf.image.resize_images(conv_sem_contour, (input_shape[1], input_shape[2])) 
            softmax_cc = tf.nn.softmax(softmax_cc)

        if self.T_D: #Distance Transform
            pre_el = self.conv_batchN_relu(output, 1, 1, self.energy_level, name='pre_el')
            with tf.variable_scope('dist_transf'):
                if self.regression:    
                  conv_dist_transf = self.conv2d(pre_el, 3, 1, 1)
                  conv_dist_transf = tf.sigmoid(conv_dist_transf )
                else:
                  conv_dist_transf = self.conv2d(pre_el, 3, 1, self.energy_level)

            ## Auxilary Distance Transform    
            if self.has_aux_loss:
                if self.regression:
                  aux1_el = tf.nn.softmax(tf.image.resize_images(self.conv_batchN_relu(b3_out, 1, 1, 1, name='conv_aux1_el', relu=False), \
                                            [input_shape[1], input_shape[2]]))
                  aux2_el = tf.nn.softmax(tf.image.resize_images(self.conv_batchN_relu(b2_out, 1, 1, 1, name='conv_aux2_el', relu=False), \
                                            [input_shape[1], input_shape[2]]))
                else:
                  aux1_el = tf.nn.softmax(tf.image.resize_images(self.conv_batchN_relu(b3_out, 1, 1,  self.energy_level, name='conv_aux1_el', relu=False), \
                                            [input_shape[1], input_shape[2]]))
                  aux2_el = tf.nn.softmax(tf.image.resize_images(self.conv_batchN_relu(b2_out, 1, 1,  self.energy_level, name='conv_aux2_el', relu=False), \
                                            [input_shape[1], input_shape[2]]))

            softmax_el = tf.image.resize_images(conv_dist_transf, (input_shape[1], input_shape[2]))
            softmax_el = tf.nn.softmax(softmax_el)
        
        return softmax_ss, aux1_ss, aux2_ss, softmax_bd, softmax_cc,  aux1_cc, aux2_cc, softmax_el, aux1_el, aux2_el
        
    def loss(self, logits_ss, label_ss, aux1_ss=None, aux2_ss=None, softmax_bd=None, \
                    softmax_cc=None, aux1_cc=None, aux2_cc=None,  softmax_el=None, aux1_el=None, aux2_el=None, \
                    label_bd=None, label_cc=None, label_el=None):
        
        loss = tf.Variable(0.0, trainable=False, name='loss')
        loss_ss = tf.Variable(0.0, trainable=False, name='loss_ss')
        loss_ss_iou = tf.Variable(0.0, trainable=False, name='loss_ss_iou')
        loss_bd = tf.Variable(0.0, trainable=False, name='loss_bd')
        loss_bd_iou = tf.Variable(0.0, trainable=False, name='loss_bd_iou')
        loss_cc = tf.Variable(0.0, trainable=False, name='loss_cc')
        loss_cc_iou = tf.Variable(0.0, trainable=False, name='loss_cc_iou')
        loss_el = tf.Variable(0.0, trainable=False, name='loss_el')
        loss_el_iou = tf.Variable(0.0, trainable=False, name='loss_el_iou')

        if self.T_S:
          loss_ss = self.loss_func.loss_cross(softmax=tf.nn.softmax(logits_ss), label=label_ss, weights=self.weights)
          loss_ss_iou = self.loss_func.IoU_loss_multilabel(logits=tf.nn.sigmoid(logits_ss), labels=label_ss, \
                          num_classes=self.num_classes)
          loss += 1.1*(loss_ss + loss_ss_iou)/2.0
          if self.has_aux_loss:
              aux_loss1_ss = tf.reduce_mean(-tf.reduce_sum(tf.multiply(label_ss*tf.log(aux1_ss+1e-10), self.weights), axis=[3]))
              aux_loss2_ss = tf.reduce_mean(-tf.reduce_sum(tf.multiply(label_ss*tf.log(aux2_ss+1e-10), self.weights), axis=[3]))
              loss = loss + (0.6*aux_loss1_ss + 0.5*aux_loss2_ss)/2.0

        if self.T_E: 
          loss_bd = self.loss_func.HED_loss(logits=softmax_bd, labels=label_bd)
          loss_bd_iou = self.loss_func.IoU_loss(logits=softmax_bd, labels=label_bd, multilabel=False)
          loss += (loss_bd + loss_bd_iou)/2.0

        if self.T_C:
          loss_cc = self.loss_func.loss_cross(softmax=tf.nn.softmax(softmax_cc), label=label_cc, weights=self.weights)
          loss_cc_iou = self.loss_func.IoU_loss_multilabel(logits=tf.nn.sigmoid(softmax_cc), labels=label_cc, \
                          num_classes=self.num_classes)
          loss += (loss_cc + loss_cc_iou)/2.0
          if self.has_aux_loss:
              aux_loss1_cc = tf.reduce_mean(-tf.reduce_sum(tf.multiply(label_cc*tf.log(aux1_cc+1e-10), self.weights), axis=[3]))
              aux_loss2_cc = tf.reduce_mean(-tf.reduce_sum(tf.multiply(label_cc*tf.log(aux2_cc+1e-10), self.weights), axis=[3]))
              loss = loss + (0.6*aux_loss1_cc + 0.5*aux_loss2_cc)/2.0
        
        if self.T_D:
          if self.regression:
              loss_el = tf.losses.mean_squared_error(labels=label_el, predictions=softmax_el)
              loss += loss_el
          else:
              loss_el = self.loss_func.loss_cross(softmax=tf.nn.softmax(softmax_el), label=label_el, weights=self.weights_el)
              loss_el_iou = self.loss_func.IoU_loss_multilabel(logits=tf.nn.sigmoid(softmax_el), labels=label_el, \
                              num_classes=self.energy_level)
              loss += (loss_el + loss_el_iou)/2.0

          if self.has_aux_loss:
              aux_loss1_el = tf.reduce_mean(-tf.reduce_sum(tf.multiply(label_el*tf.log(aux1_el+1e-10), self.weights_el), axis=[3]))
              aux_loss2_el = tf.reduce_mean(-tf.reduce_sum(tf.multiply(label_el*tf.log(aux2_el+1e-10), self.weights_el), axis=[3]))
              loss = loss + (0.6*aux_loss1_el + 0.5*aux_loss2_el)/2.0

        tf.add_to_collection('losses', loss)
        
        return loss, loss_ss, loss_ss_iou, loss_bd, loss_bd_iou, loss_cc, loss_cc_iou, loss_el, loss_el_iou


    #def create_optimizer(self):
    #    self.lr = tf.train.polynomial_decay(self.learning_rate, self.global_step, self.decay_steps, power=self.power)
    #    self.train_op = tf.train.AdamOptimizer(self.lr).minimize(self.loss, global_step=self.global_step)
    #    #self.train_op = tf.train.AdamOptimizer(self.learning_rate).minimize(self.loss, global_step=self.global_step)

    def _create_summaries(self):
        with tf.name_scope("summaries"):
            tf.summary.scalar("loss", self.loss)
            tf.summary.histogram("histogram_loss", self.loss)
            self.summary_op = tf.summary.merge_all()

def main():
    print('Do Nothing')
   
if __name__ == '__main__':
    main()

