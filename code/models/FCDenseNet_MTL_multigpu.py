import tensorflow as tf
from models import network_base
from loss.losses import LossFunc

class FCDenseNet_MTL(network_base.Network):
    def __init__(self, num_classes=12, energy_level=6, learning_rate=0.001, float_type=tf.float32, weight_decay=0.0005,
                 decay_steps=30000, power=0.9, training=True, ignore_label=True, global_step=0,
                 has_aux_loss=True, S=True, E=False, C=False, D=False, regression=False):
        
        super(FCDenseNet_MTL, self).__init__()
        self.num_classes = num_classes
        self.learning_rate = learning_rate
        self.weight_decay = weight_decay
        self.initializer = 'he'
        self.has_aux_loss = has_aux_loss
        self.float_type = float_type
        self.power = power
        self.decay_steps = decay_steps
        self.training = training
        self.bn_decay_ = 0.99
        self.global_step = global_step
        self.energy_level = energy_level
        self.regression = regression

        preset_model = 'FCDenseNet67'
        if preset_model == 'FCDenseNet56':
          self.n_pool = 5
          self.growth_rate = 12
          self.n_layers_per_block = [4]*(2*self.n_pool+1)
          self.first_conv = 48
        elif preset_model == 'FCDenseNet67':
          self.n_pool = 5
          self.growth_rate = 16
          self.n_layers_per_block = [5]*(2*self.n_pool+1)
          self.first_conv = 48
        elif preset_model == 'FCDenseNet103':
          self.n_pool = 5
          self.growth_rate = 16
          self.n_layers_per_block = [4, 5, 7, 10, 12, 15, 12, 10, 7, 5, 4]
          self.first_conv = 48
        else:
          raise ValueError("Unsupported FC-DenseNet model '%s'. This function only supports FC-DenseNet56, FC-DenseNet67, and FC-DenseNet103" % (preset_model)) 

        self.T_S = S
        self.T_E = E
        self.T_C = C
        self.T_D = D
      
        self.loss_func = LossFunc()

        if self.training:
            self.keep_prob = 0.3
        else:
            self.keep_prob = 1.0
        if ignore_label:
            self.weights = tf.ones(self.num_classes-1)
            self.weights = tf.concat((tf.zeros(1), self.weights), 0)
            self.weights_el = tf.ones(self.energy_level-1)
            self.weights_el = tf.concat((tf.zeros(1), self.weights_el), 0)
        else:
            self.weights = tf.ones(self.num_classes)
            self.weights_el = tf.ones(self.energy_level)

    def preact_conv(self, inputs, n_filters, kernel_size=3, scope='preact_conv'):
        '''
        Basic pre-activation layer for DenseNets
        Apply successivly BatchNormalization, ReLU nonlinearity, Convolution
        '''
        with tf.variable_scope(scope):
          preact = tf.nn.relu(self.batch_norm(inputs))
          conv = self.conv2d(preact, kernel_size, 1, n_filters, name='first_conv')
        return conv

    def TransitionDown(self, inputs, n_filters, scope):
        '''
        Transition Down (TD) for FC-DenseNet
        Apply 1x1 BN + ReLU + conv then 2x2 max pooling
        '''
        #with tf.name_scope(scope) as sc:
        with tf.variable_scope(scope):
          l = self.preact_conv(inputs, n_filters, kernel_size=1)
          l = self.pool(l, 2, 2)
          return l

    def TransitionUp(self, block_to_upsample, skip_connection, n_filters_keep, h, w, scope):
        '''
        Transition Up for FC-DenseNet
        Performs upsampling on block_to_upsample by a factor 2 and concatenates it with the skip_connection
        '''
        with tf.variable_scope(scope):
          # Upsample
          l = self.tconv2d(block_to_upsample, 3, n_filters_keep, 2, h, w)
          # Concatenate with skip connection
          l = tf.concat([l, skip_connection], axis=-1)
          return l

    def DenseBlock(self, stack, n_layers, growth_rate, scope):
        '''
        DenseBlock for DenseNet and FC-DenseNet
        Arguments:
          stack: input 4D tensor
          n_layers: number of internal layers
          growth_rate: number of feature maps per internal layer
        Returns:
          stack: current stack of feature maps (4D tensor)
          new_features: 4D tensor containing only the new feature maps generated
            in this block
        '''
        with tf.variable_scope(scope):
          new_features = []
          for j in range(n_layers):
            # Compute new feature maps
            layer = self.preact_conv(stack, self.growth_rate, scope='preact_conv%d' % (j+1))
            new_features.append(layer)
            # Stack new layer
            stack = tf.concat([stack, layer], axis=-1)
          new_features = tf.concat(new_features, axis=-1)
          return stack, new_features
     
    def forward(self, data, batch_size):   
        input_shape = data.get_shape()
        with tf.variable_scope('conv0'):
            data_after_bn = self.batch_norm(data)

        # First Convolution
        stack = self.conv2d(data_after_bn, 3, 1, self.first_conv, name='first_conv')
        n_filters = self.first_conv

        # Downsampling path
        skip_connection_list = []
        for i in range(self.n_pool):
          # Dense Block
          stack, _ = self.DenseBlock(stack, self.n_layers_per_block[i], self.growth_rate, scope='denseblock%d' % (i+1))
          n_filters += self.growth_rate * self.n_layers_per_block[i]
          # At the end of the dense block, the current stack is stored in the skip_connections list
          skip_connection_list.append(stack)

          # Transition Down
          stack = self.TransitionDown(stack, n_filters, scope='transitiondown%d'%(i+1))
        
        skip_connection_list = skip_connection_list[::-1]

        # Bottleneck
        # We will only upsample the new feature maps
        stack, block_to_upsample = self.DenseBlock(stack, self.n_layers_per_block[self.n_pool], self.growth_rate, scope='denseblock%d' % (self.n_pool + 1))

        # Upsampling path
        for i in range(self.n_pool):
          # Transition Up ( Upsampling + concatenation with the skip connection)
          n_filters_keep = self.growth_rate * self.n_layers_per_block[self.n_pool + i]
          size_skip = skip_connection_list[i].get_shape().as_list()
                                #TransitionUp(block_to_upsample, skip_connection, n_filters_keep, h, w, scope)
          stack = self.TransitionUp(block_to_upsample, skip_connection_list[i], n_filters_keep, h=size_skip[1], w=size_skip[2], scope='transitionup%d' % (self.n_pool + i + 1))
          
          # We will only upsample the new feature maps
          stack, block_to_upsample = self.DenseBlock(stack, self.n_layers_per_block[self.n_pool + i + 1], self.growth_rate, scope='denseblock%d' % (self.n_pool + i + 2))
          
        deconv1_2 = stack

        softmax_ss , aux1_ss, aux2_ss, softmax_bd, softmax_cc, aux1_cc, aux2_cc, \
          aux1_el, aux2_el, softmax_el = [None for i in range(10)]

        if self.T_S: # Semantic Segmentation
            if not self.T_E  and not self.T_C and not self.T_D:
              with tf.variable_scope('sem_segmentation'):
                conv_seg = self.conv2d(deconv1_2, 1, 1, self.num_classes)
            else:
              pre_ss = self.conv_batchN_relu(deconv1_2, 1, 1, self.num_classes, name='pre_ss')
              with tf.variable_scope('sem_segmentation'):
                conv_seg = self.conv2d(pre_ss, 8, 1, self.num_classes)
            softmax_ss = conv_seg

        if self.T_E: # Edge Detection
            pre_bd = self.conv_batchN_relu(deconv1_2, 1, 1, 1, name='pre_bd')
            with tf.variable_scope('contour'):
              conv_contour = self.conv2d(pre_bd, 8, 1, 1)
            softmax_bd = tf.nn.sigmoid(conv_contour)

        if self.T_C: # Semantic Contour Detection
            pre_cc = self.conv_batchN_relu(deconv1_2, 1, 1, self.num_classes, name='pre_cc')
            with tf.variable_scope('sem_contour'):
              conv_sem_contour = self.conv2d(pre_cc, 8, 1, self.num_classes)
            softmax_cc = conv_sem_contour

        if self.T_D: #Distance Transform
            pre_el = self.conv_batchN_relu(deconv1_2, 1, 1, self.energy_level, name='pre_el')
            with tf.variable_scope('dist_transf'):
                if self.regression:    
                  conv_dist_transf = self.conv2d(pre_el, 8, 1, 1)
                  conv_dist_transf = tf.sigmoid(conv_dist_transf )
                else:
                  conv_dist_transf = self.conv2d(pre_el, 8, 1, self.energy_level)
            softmax_el = conv_dist_transf
        
        return softmax_ss, aux1_ss, aux2_ss, softmax_bd, softmax_cc,  aux1_cc, aux2_cc, softmax_el, aux1_el, aux2_el
        
    def loss(self, logits_ss, label_ss, aux1_ss=None, aux2_ss=None, softmax_bd=None, \
                    softmax_cc=None, aux1_cc=None, aux2_cc=None,  softmax_el=None, aux1_el=None, aux2_el=None, \
                    label_bd=None, label_cc=None, label_el=None):
        #self.loss = tf.reduce_mean(-tf.reduce_sum(tf.multiply(label*tf.log(self.softmax+1e-10), self.weights), axis=[3]))
        
        loss = tf.Variable(0.0, trainable=False, name='loss')
        loss_ss = tf.Variable(0.0, trainable=False, name='loss_ss')
        loss_ss_iou = tf.Variable(0.0, trainable=False, name='loss_ss_iou')
        loss_bd = tf.Variable(0.0, trainable=False, name='loss_bd')
        loss_bd_iou = tf.Variable(0.0, trainable=False, name='loss_bd_iou')
        loss_cc = tf.Variable(0.0, trainable=False, name='loss_cc')
        loss_cc_iou = tf.Variable(0.0, trainable=False, name='loss_cc_iou')
        loss_el = tf.Variable(0.0, trainable=False, name='loss_el')
        loss_el_iou = tf.Variable(0.0, trainable=False, name='loss_el_iou')

        if self.T_S:
          loss_ss = self.loss_func.loss_cross(softmax=tf.nn.softmax(logits_ss), label=label_ss, weights=self.weights)
          loss_ss_iou = self.loss_func.IoU_loss_multilabel(logits=tf.nn.sigmoid(logits_ss), labels=label_ss, \
                          num_classes=self.num_classes)
          loss += 1.1*(loss_ss + loss_ss_iou)/2.0

        if self.T_E: 
          loss_bd = self.loss_func.HED_loss(logits=softmax_bd, labels=label_bd)
          loss_bd_iou = self.loss_func.IoU_loss(logits=softmax_bd, labels=label_bd, multilabel=False)
          loss += (loss_bd + loss_bd_iou)/2.0

        if self.T_C:
          loss_cc = self.loss_func.loss_cross(softmax=tf.nn.softmax(softmax_cc), label=label_cc, weights=self.weights)
          loss_cc_iou = self.loss_func.IoU_loss_multilabel(logits=tf.nn.sigmoid(softmax_cc), labels=label_cc, \
                          num_classes=self.num_classes)
          loss += (loss_cc + loss_cc_iou)/2.0
        
        if self.T_D:
          if self.regression:
              loss_el = tf.losses.mean_squared_error(labels=label_el, predictions=softmax_el)
              loss += loss_el
          else:
              loss_el = self.loss_func.loss_cross(softmax=tf.nn.softmax(softmax_el), label=label_el, weights=self.weights_el)
              loss_el_iou = self.loss_func.IoU_loss_multilabel(logits=tf.nn.sigmoid(softmax_el), labels=label_el, \
                              num_classes=self.energy_level)
              loss += (loss_el + loss_el_iou)/2.0

        tf.add_to_collection('losses', loss)
        
        return loss, loss_ss, loss_ss_iou, loss_bd, loss_bd_iou, loss_cc, loss_cc_iou, loss_el, loss_el_iou


    #def create_optimizer(self):
    #    self.lr = tf.train.polynomial_decay(self.learning_rate, self.global_step, self.decay_steps, power=self.power)
    #    self.train_op = tf.train.AdamOptimizer(self.lr).minimize(self.loss, global_step=self.global_step)
    #    #self.train_op = tf.train.AdamOptimizer(self.learning_rate).minimize(self.loss, global_step=self.global_step)

    def _create_summaries(self):
        with tf.name_scope("summaries"):
            tf.summary.scalar("loss", self.loss)
            tf.summary.histogram("histogram_loss", self.loss)
            self.summary_op = tf.summary.merge_all()

    def build_graph(self, data, batch_size, label_ss=None, label_bd=None, label_cc=None, label_el=None):
        self.forward(data, batch_size)

def main():
    print('Do Nothing')
   
if __name__ == '__main__':
    main()

