import tensorflow as tf
from models import network_base
from loss.losses import LossFunc

class ParseNet_MTL(network_base.Network):
    def __init__(self, num_classes=12, energy_level=6, learning_rate=0.001, float_type=tf.float32, weight_decay=0.0005,
                 decay_steps=30000, power=0.9, training=True, ignore_label=True, global_step=0,
                 has_aux_loss=True, S=True, E=False, C=False, D=False, regression=False):
        
        super(ParseNet_MTL, self).__init__()
        self.num_classes = num_classes
        self.learning_rate = learning_rate
        self.weight_decay = weight_decay
        self.initializer = 'he'
        self.has_aux_loss = has_aux_loss
        self.float_type = float_type
        self.power = power
        self.decay_steps = decay_steps
        self.training = training
        self.bn_decay_ = 0.99
        self.global_step = global_step
        self.energy_level = energy_level
        self.regression = regression

        self.T_S = S
        self.T_E = E
        self.T_C = C
        self.T_D = D
      
        self.loss_func = LossFunc()

        if self.training:
            self.keep_prob = 0.3
        else:
            self.keep_prob = 1.0
        if ignore_label:
            self.weights = tf.ones(self.num_classes-1)
            self.weights = tf.concat((tf.zeros(1), self.weights), 0)
            self.weights_el = tf.ones(self.energy_level-1)
            self.weights_el = tf.concat((tf.zeros(1), self.weights_el), 0)
        else:
            self.weights = tf.ones(self.num_classes)
            self.weights_el = tf.ones(self.energy_level)
     
    def forward(self, data, batch_size):   
        input_shape = data.get_shape()
        with tf.variable_scope('conv0'):
            data_after_bn = self.batch_norm(data)
       
        with tf.variable_scope('enc_box1'):
            conv1_1 = self.conv_batchN_relu(data_after_bn, 3, 1, 64, name='conv1_1') #kernel_size, stride, out_channels
            conv1_2 = self.conv_batchN_relu(conv1_1, 3, 1, 64, name='conv1_2')
            pool1, pool1_index, shape_1 = self.pool_index(conv1_2, 2, 2)

        with tf.variable_scope('enc_box2'):
            conv2_1 = self.conv_batchN_relu(pool1, 3, 1, 128, name='conv2_1')
            conv2_2 = self.conv_batchN_relu(conv2_1, 3, 1, 128, name='conv2_2')
            pool2, pool2_index, shape_2 = self.pool_index(conv2_2, 2, 2)

        with tf.variable_scope('enc_box3'):
            conv3_1 = self.conv_batchN_relu(pool2, 3, 1, 256, name='conv3_1')
            conv3_2 = self.conv_batchN_relu(conv3_1, 3, 1, 256, name='conv3_2')
            conv3_3 = self.conv_batchN_relu(conv3_2, 3, 1, 256, name='conv3_3')
            pool3, pool3_index, shape_3 = self.pool_index(conv3_3, 2, 2)

        with tf.variable_scope('enc_box4'):
            conv4_1 = self.conv_batchN_relu(pool3, 3, 1, 512, name='conv4_1')
            conv4_2 = self.conv_batchN_relu(conv4_1, 3, 1, 512, name='conv4_2')
            conv4_3 = self.conv_batchN_relu(conv4_2, 3, 1, 512, name='conv4_3')
            pool4, pool4_index, shape_4 = self.pool_index(conv4_3, 2, 2)

        with tf.variable_scope('enc_box5'):
            conv5_1 = self.conv_batchN_relu(pool4, 3, 1, 512, name='conv5_1')
            conv5_2 = self.conv_batchN_relu(conv5_1, 3, 1, 512, name='conv5_2')
            conv5_3 = self.conv_batchN_relu(conv5_2, 3, 1, 512, name='conv5_3')
            pool5, pool5_index, shape_5 = self.pool_index(conv5_3, 2, 2)

        with tf.variable_scope('enc_box6'):
            fc_6 = self.conv_batchN_relu(pool5, 7, 1, 4096, name='fc_6')
            shape_fc6 = fc_6.get_shape().as_list()

            #global pooling
            global_feature = tf.reduce_mean(fc_6, [1, 2], keepdims=True)
            global_feature = tf.nn.l2_normalize(global_feature)

            fc_6_norm = tf.nn.l2_normalize(fc_6)
            unpool_fc6 = tf.image.resize_bilinear(global_feature, (shape_fc6[1], shape_fc6[2]), name="unpool_fc6")
            combined_feature = tf.concat([fc_6_norm, unpool_fc6] , -1)

            fc_7 = self.conv_batchN_relu(combined_feature, 1, 1, 4096, name='fc_7')

        with tf.variable_scope('dec'):
            # Apply 1x1 convolution to predict classes of layer fc_7 at stride 32
            conv_classes = self.conv_batchN_relu(fc_7, 1, 1, self.num_classes, name='conv_classes')
            # 32x bilinear interpolation
            fcn32_out = tf.image.resize_bilinear(conv_classes, (input_shape[1], input_shape[2]), name="fcn32_out")

            # Apply 1x1 convolution to predict classes of layer 4 at stride 16
            pool4_classes = self.conv_batchN_relu(pool4, 1, 1, self.num_classes, name='pool4_classes')
            # Up-sample (2x) conv7 class predictions to match the size of layer 4
            fcn32_upsampled = self.tconv_batchN_relu(conv_classes, 4, 2, self.num_classes, shape_5[1], shape_5[2], name='fcn32_upsampled') 
            # Add a skip connection between class predictions of layer 4 and up-sampled class predictions of layer 7
            skip_1 = tf.add(pool4_classes, fcn32_upsampled, name="skip_cnx_1")
            # 16x bilinear interpolation
            fcn16_out = tf.image.resize_bilinear(skip_1, (input_shape[1], input_shape[2]), name="fcn16_out")

            # Apply 1x1 convolution to predict classes of layer 3 at stride 8
            pool3_classes = self.conv_batchN_relu(pool3, 1, 1, self.num_classes, name='pool3_classes')
            # Up-sample (2x) skip_1 class predictions to match the size of layer 3
            fcn16_upsampled = self.tconv_batchN_relu(skip_1, 4, 2, self.num_classes, shape_4[1], shape_4[2], name='fcn16_upsampled') 
            # Add a skip connection between class predictions of layer 4 and up-sampled class predictions of layer 7
            skip_2 = tf.add(pool3_classes, fcn16_upsampled, name="skip_cnx_2")
            # 8x bilinear interpolation
            fcn8_out = tf.image.resize_bilinear(skip_2, (input_shape[1], input_shape[2]), name="fcn8_out")

        softmax_ss , aux1_ss, aux2_ss, softmax_bd, softmax_cc, aux1_cc, aux2_cc, \
          aux1_el, aux2_el, softmax_el = [None for i in range(10)]

        if self.T_S: # Semantic Segmentation
            if not self.T_E  and not self.T_C and not self.T_D:
              conv_seg = fcn8_out
            else:
              pre_ss = self.conv_batchN_relu(fcn8_out, 1, 1, self.num_classes, name='pre_ss')
              with tf.variable_scope('sem_segmentation'):
                conv_seg = self.conv2d(pre_ss, 8, 1, self.num_classes)
            softmax_ss = conv_seg

        if self.T_E: # Edge Detection
            pre_bd = self.conv_batchN_relu(fcn8_out, 1, 1, 1, name='pre_bd')
            with tf.variable_scope('contour'):
              conv_contour = self.conv2d(pre_bd, 8, 1, 1)
            softmax_bd = tf.nn.sigmoid(conv_contour)

        if self.T_C: # Semantic Contour Detection
            pre_cc = self.conv_batchN_relu(fcn8_out, 1, 1, self.num_classes, name='pre_cc')
            with tf.variable_scope('sem_contour'):
              conv_sem_contour = self.conv2d(pre_cc, 8, 1, self.num_classes)
            softmax_cc = conv_sem_contour 

        if self.T_E: #Distance Transform
            pre_el = self.conv_batchN_relu(fcn8_out, 1, 1, self.energy_level, name='pre_el')
            with tf.variable_scope('dist_transf'):
                if self.regression:    
                  conv_dist_transf = self.conv2d(pre_el, 8, 1, 1)
                  conv_dist_transf = tf.sigmoid(conv_dist_transf )
                else:
                  conv_dist_transf = self.conv2d(pre_el, 8, 1, self.energy_level)
            softmax_el = conv_dist_transf
        
        return softmax_ss, aux1_ss, aux2_ss, softmax_bd, softmax_cc,  aux1_cc, aux2_cc, softmax_el, aux1_el, aux2_el
        
    def loss(self, logits_ss, label_ss, aux1_ss=None, aux2_ss=None, softmax_bd=None, \
                    softmax_cc=None, aux1_cc=None, aux2_cc=None,  softmax_el=None, aux1_el=None, aux2_el=None, \
                    label_bd=None, label_cc=None, label_el=None, h_ss=1.1, h_bd=0.9, h_cc=0.9, h_el=0.8):
        
        loss = tf.Variable(0.0, trainable=False, name='loss')
        loss_ss = tf.Variable(0.0, trainable=False, name='loss_ss')
        loss_ss_iou = tf.Variable(0.0, trainable=False, name='loss_ss_iou')
        loss_bd = tf.Variable(0.0, trainable=False, name='loss_bd')
        loss_bd_iou = tf.Variable(0.0, trainable=False, name='loss_bd_iou')
        loss_cc = tf.Variable(0.0, trainable=False, name='loss_cc')
        loss_cc_iou = tf.Variable(0.0, trainable=False, name='loss_cc_iou')
        loss_el = tf.Variable(0.0, trainable=False, name='loss_el')
        loss_el_iou = tf.Variable(0.0, trainable=False, name='loss_el_iou')

        if self.T_S:
          loss_ss = self.loss_func.loss_cross(softmax=tf.nn.softmax(logits_ss), label=label_ss, weights=self.weights)
          loss_ss_iou = self.loss_func.IoU_loss_multilabel(logits=tf.nn.sigmoid(logits_ss), labels=label_ss, \
                          num_classes=self.num_classes)
          loss += h_ss*(loss_ss + loss_ss_iou)/2.0

        if self.T_E: 
          loss_bd = self.loss_func.HED_loss(logits=softmax_bd, labels=label_bd)
          loss_bd_iou = self.loss_func.IoU_loss(logits=softmax_bd, labels=label_bd, multilabel=False)
          loss += h_bd*(loss_bd + loss_bd_iou)/2.0

        if self.T_C:
          loss_cc = self.loss_func.loss_cross(softmax=tf.nn.softmax(softmax_cc), label=label_cc, weights=self.weights)
          loss_cc_iou = self.loss_func.IoU_loss_multilabel(logits=tf.nn.sigmoid(softmax_cc), labels=label_cc, \
                          num_classes=self.num_classes)
          loss += h_cc*(loss_cc + loss_cc_iou)/2.0
        
        if self.T_D:
          if self.regression:
              loss_el = tf.losses.mean_squared_error(labels=label_el, predictions=softmax_el)
              loss += h_el*loss_el
          else:
              loss_el = self.loss_func.loss_cross(softmax=tf.nn.softmax(softmax_el), label=label_el, weights=self.weights_el)
              loss_el_iou = self.loss_func.IoU_loss_multilabel(logits=tf.nn.sigmoid(softmax_el), labels=label_el, \
                              num_classes=self.energy_level)
              loss += h_el*(loss_el + loss_el_iou)/2.0

        tf.add_to_collection('losses', loss)
        
        return loss, loss_ss, loss_ss_iou, loss_bd, loss_bd_iou, loss_cc, loss_cc_iou, loss_el, loss_el_iou


    #def create_optimizer(self):
    #    self.lr = tf.train.polynomial_decay(self.learning_rate, self.global_step, self.decay_steps, power=self.power)
    #    self.train_op = tf.train.AdamOptimizer(self.lr).minimize(self.loss, global_step=self.global_step)
    #    #self.train_op = tf.train.AdamOptimizer(self.learning_rate).minimize(self.loss, global_step=self.global_step)

    def _create_summaries(self):
        with tf.name_scope("summaries"):
            tf.summary.scalar("loss", self.loss)
            tf.summary.histogram("histogram_loss", self.loss)
            self.summary_op = tf.summary.merge_all()

    def build_graph(self, data, batch_size, label_ss=None, label_bd=None, label_cc=None, label_el=None):
        self.forward(data, batch_size)

def main():
    print('Do Nothing')
   
if __name__ == '__main__':
    main()

