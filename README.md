# Empirical Study of Multi-Task Hourglass Model for Semantic Segmentation Task
Created by Darwin Saire and Adín Ramírez Rivera

### Model

<div align="center"><img src="img/model.png" alt="non-trivial image" width="70%" align="center"></div> <br>

The semantic segmentation (SS) task aims to create a dense classification by labeling at the pixel level each object present on images. Convolutional neural network (CNN) approaches have been widely used, and exhibited the best results in this task. However, the loss of spatial precision on the results is a main drawback that has not been solved. In this work, we propose to use a multi-task approach by complementing the semantic segmentation task with edge detection (E), semantic contour (C), and distance transform (D) tasks. We propose that by sharing a common latent space, the complementary tasks can produce more robust representations that can enhance the semantic labels. We explore the influence of contour-based tasks on latent space, as well as their impact on the final results of SS. We demonstrate the effectiveness of learning in a multi-task setting for hourglass models in the Cityscapes, CamVid, and Freiburg Forest datasets by improving the state-of-the-art without any refinement post-processing.

<div align="center"><img src="img/contour-based-tasks.png" alt="non-trivial image" width="80%" align="center"></div>

We capture two ideas:

- The first idea is to visualize the behavior of the internal representation of the features (latent space) in encoder-decoder models (hourglass) when influenced by auxiliary information provided by the multi-task approach.

  <div align="center"><img src="img/latent-space-tasks.png" alt="non-trivial image" width="80%" align="center"></div> <br>

- The second idea is to use this new latent space influenced complementary contour-based tasks, to improve semantic segmentation in the different hourglass models, and to address the problem of loss of spatial precision (loss of segmentation at the edge of segmented objects).

  <div align="center"><img src="img/quali-results.png" alt="non-trivial image" width="90%" align="center"></div> <br>

### Citations

If you find the code useful for your research, please consider citing our paper:

```
@Article{Saire2021,
  author  = {Saire, D. and Ram\'irez Rivera, A.},
  title   = {Empirical Study of Multi-Task Hourglass Model for Semantic Segmentation Task},
  journal = {{IEEE} Access},
  year    = {2021},
  Pages   = {80654--80670},
  Volume  = {9},
  code    = {https://gitlab.com/mipl/mtl-ss},
  doi     = {10.1109/ACCESS.2021.3085218},
}
```


### Setting up

- Get this repo, git clone https://gitlab.com/mipl/mtl-ss.git
- Get a docker env with tensorflow 1.9, sklearn, numpy, opencv3, python3, for example, [the tensorflow dockerfile](https://gitlab.com/mipl/mtl-ss/-/blob/master/docker/tf.Dockerfile) or use the [autobuild image](https://hub.docker.com/repository/docker/mipl/mtl-ss).  The container we are making available contains the needed dependencies, and exposes a jupyter installation (default from tensorflow), exposed on ports 8888.  Similarly, the port 6006 is exposed for tensorboard.

##### Training Params
```
    gpu_id: id of gpu to be used
    model: name of the model
    num_classes: number of classes (including void, label id:0)
    energy_level: number of quantized distance transform
    tasks: number of task using in the training
    regression: (True/False) if distance transform is quantized or not
    dataset: name of the dataset used
    checkpoint: path to save model
    train_data: path to dataset .tfrecords
    batch_size: training batch size
    skip_step: how many steps to print loss 
    height: height of input image
    width: width of input image
    epochs: number of epochs
    max_iteration: how many iterations to train
    learning_rate: initial learning rate
    save_step: how many steps to save the model
    power: parameter for poly learning rate
    unlabel: id of background on the dataset
    is_board: (True/False) if tensorboard is used
    tb_logs: path to save the tensorboard metrics
    load_param: (True/False) if load the parameters of the model
    path_param: path to load model
    
```

##### Evaluation Params
```
    gpu_id: id of gpu to be used
    model: name of the model
    num_classes: number of classes (including void, label id:0)
    energy_level: number of quantized distance transform
    tasks: number of task using in the testing (default 'S')
    regression: (true, false) if distance transform is quantized or not
    dataset: name of the dataset used
    checkpoint: path to load the model
    test_data: path to dataset .tfrecords
    batch_size: evaluation batch size
    skip_step: how many steps to print mIoU
    height: height of input image
    width: width of input image
    unlabel: id of background on the dataset
    saved_results: path to saved the results
```

### Training and Evaluation

`dataset: 'camvid', 'cityscape_11','freiburgforest'` 

##### Training Procedure

Edit the config file for training in config folder. Run:

```
python3 train_MTL_multigpu.py --config config/cityscapes_train.config
```

##### Evaluation Procedure

Select a checkpoint to test/validate your model in terms of the mean IoU metric.
Edit the config file for evaluation in config folder. Run:

```
python3 evaluate_MTL.py --config config/cityscapes_test.config
```

